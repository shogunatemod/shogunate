# Shogunate
Shogunate is a total conversion mod for Crusader Kings III. You can find more information in the [Wiki](https://gitlab.com/shogunatemod/shogunate/-/wikis/home).

![Sample Image 1](https://i.imgur.com/6NgF9ep.jpg)

## About This Mod
Shogunate focuses on the medieval age in Japan, especially the Sengoku period around the 16th century. It includes a lot of historical feudal lords, aka Sengoku Daimyo, with their family trees.

![Sample Image 2](https://i.imgur.com/OBXkWiw.jpg)
![Sample Image 3](https://i.imgur.com/Ww4YCxv.jpg)

## Installation
Please read the [Wiki](https://gitlab.com/shogunatemod/shogunate/-/wikis/Installation).

## Contact
We develop Shogunate on [our Discord server](https://discord.gg/QqfQveq). We welcome your questions, suggestions, and also participation as co-developers.

## Credit
Shogunate uses [Nova Monumenta Iaponiae Historica](https://steamcommunity.com/sharedfiles/filedetails/?id=333442855) data with the author chatnoir17's permission.

Some genes and skin textures come from [Ethnicities & Portraits Expanded](https://steamcommunity.com/sharedfiles/filedetails/?id=2507209632) with the lead Celticus's permission.

Japanese unit models come from [Asia Expansion Project](https://steamcommunity.com/sharedfiles/filedetails/?id=2970440958) with the lead Longus Cattus's permission.

Chinese translation comes from [幕府汉化](https://steamcommunity.com/sharedfiles/filedetails/?id=3200306825) with the lead 無壹's permission.

Japanese translation comes from [CK3_JPM_PROJECT](https://paratranz.cn/projects/1518).

The music comes from [PeriTune](https://peritune.com/).

The thumbnail image comes from the [Geospatial Information Authority of Japan](https://kochizu.gsi.go.jp/items/185).
