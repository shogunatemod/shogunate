﻿namespace = shogunate_betrayal

# Ask for Betrayal
shogunate_betrayal.0001 = {
	type = letter_event

	opening = {
		desc = char_interaction.0010.opening
	}
	desc = char_interaction.0001.desc

	sender = scope:recipient

	option = {
		name = EXCELLENT
	}
}

# Offer Betrayal
shogunate_betrayal.0002 = {
	type = letter_event

	opening = {
		desc = char_interaction.0010.opening
	}
	desc = char_interaction.0010.desc

	sender = scope:recipient

	option = {
		name = char_interaction.0010.a
	}
}

# My Vassal Betrayed!
shogunate_betrayal.0003 = {
	type = character_event

	title = shogunate_betrayal_interaction.0003.t
	desc = shogunate_betrayal_interaction.0003.desc

	theme = intrigue
	left_portrait = {
		character = scope:recipient
		animation = personality_cynical
	}

	option = {
		trigger = {
			is_liege_or_above_of = scope:recipient
		}
		name = shogunate_betrayal_interaction.0003.a
	}

	option = {
		trigger = {
			NOT = { is_liege_or_above_of = scope:recipient }
		}
		name = shogunate_betrayal_interaction.0003.b
	}
}

# My Vassal Betrayed!
shogunate_betrayal.0004 = {
	type = character_event

	title = shogunate_betrayal_interaction.0004.t
	desc = shogunate_betrayal_interaction.0004.desc

	theme = intrigue
	left_portrait = {
		character = scope:actor
		animation = personality_cynical
	}

	option = {
		trigger = {
			is_liege_or_above_of = scope:actor
		}
		name = shogunate_betrayal_interaction.0003.a
	}

	option = {
		trigger = {
			NOT = { is_liege_or_above_of = scope:actor }
		}
		name = shogunate_betrayal_interaction.0003.b
	}
}
