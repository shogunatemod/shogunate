﻿###CE1 Music

###### CUE Tracks

apocalyptic_plague= {
    music = "event:/DLC/FP4/MUSIC/Cues/mx_cue_apocalyptic_plague"
    group = group_fp4_cuetrack
}


black_death = {
    music = "event:/DLC/FP4/MUSIC/Cues/mx_cue_black_death"
    group = group_fp4_cuetrack
}

legend_begins = {
    music = "event:/DLC/FP4/MUSIC/Cues/mx_cue_legend_begins"
    group = group_fp4_cuetrack
}

######### GROUPS

group_fp4_cuetrack = {
    pause_factor = 30
}

group_fp4_moodtrack = {
    pause_factor = 15
 	subsequent_playback_chance = { 0.65 0.35 0.1 }
 	mood = yes
	can_be_interrupted = yes
 	trigger_prio_override = yes
	
	is_valid = {
		has_ce1_dlc_trigger = yes
	}
}
