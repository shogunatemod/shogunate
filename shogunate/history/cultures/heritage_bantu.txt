# East Bantu (for Yasuke)

1000.1.1 = {
	discover_innovation = innovation_shogunate_tachi
	discover_innovation = innovation_shogunate_wakyu
	discover_innovation = innovation_catapult
	discover_innovation = innovation_shogunate_bushi
	discover_innovation = innovation_shogunate_goso
	discover_innovation = innovation_horseshoes

	discover_innovation = innovation_shogunate_shogun
	discover_innovation = innovation_shogunate_inzen
	discover_innovation = innovation_gavelkind
	discover_innovation = innovation_shogunate_song_trade
	discover_innovation = innovation_shogunate_yusoku_kojitsu
	discover_innovation = innovation_shogunate_shinden_zukuri
	discover_innovation = innovation_shogunate_mappo_shiso
	discover_innovation = innovation_shogunate_sarugaku

	join_era = culture_era_tribal
}

1200.1.1 = {
	discover_innovation = innovation_motte
	discover_innovation = innovation_quilted_armor
	discover_innovation = innovation_bannus
	discover_innovation = innovation_mustering_grounds
	discover_innovation = innovation_barracks
	discover_innovation = innovation_shogunate_shikimoku

	discover_innovation = innovation_plenary_assemblies
	discover_innovation = innovation_hereditary_rule
	discover_innovation = innovation_currency_01
	discover_innovation = innovation_development_01
	discover_innovation = innovation_casus_belli
	discover_innovation = innovation_city_planning
	discover_innovation = innovation_crop_rotation
	discover_innovation = innovation_ledger

	join_era = culture_era_pre_medieval
}

1400.1.1 = {
	discover_innovation = innovation_shogunate_naginata
	discover_innovation = innovation_shogunate_domaru
	discover_innovation = innovation_mangonel
	discover_innovation = innovation_shogunate_hokoshu
	discover_innovation = innovation_shogunate_toritsugi
	discover_innovation = innovation_shogunate_bashaku

	discover_innovation = innovation_shogunate_shugo_daimyo
	discover_innovation = innovation_primogeniture
	discover_innovation = innovation_shogunate_doso_sakaya
	discover_innovation = innovation_shogunate_soson
	discover_innovation = innovation_shogunate_gonaisho
	discover_innovation = innovation_shogunate_shoin_zukuri
	discover_innovation = innovation_shogunate_suibokuga
	discover_innovation = innovation_shogunate_noh_kyogen

	join_era = culture_era_early_medieval
}

1600.1.1 = {
	discover_innovation = innovation_arched_saddle
	discover_innovation = innovation_shogunate_haramaki
	discover_innovation = innovation_battlements
	discover_innovation = innovation_house_soldiers
	discover_innovation = innovation_burhs
	discover_innovation = innovation_shogunate_jizamurai

	discover_innovation = innovation_royal_prerogative
	discover_innovation = innovation_heraldry
	discover_innovation = innovation_chronicle_writing
	discover_innovation = innovation_baliffs
	discover_innovation = innovation_manorialism
	discover_innovation = innovation_currency_02
	discover_innovation = innovation_development_02
	discover_innovation = innovation_armilary_sphere

	join_era = culture_era_high_medieval
}
