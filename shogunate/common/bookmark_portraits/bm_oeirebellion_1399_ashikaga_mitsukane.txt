# Auto generated file, do not edit manually. Created using console command dump_bookmark_portraits
# History database id:10040064
bm_oeirebellion_1399_ashikaga_mitsukane={
	type=male
	id=18130
	age=0.210000
	genes={ 		hair_color={ 18 244 14 244 }
 		skin_color={ 157 42 157 42 }
 		eye_color={ 5 253 2 232 }
 		gene_chin_forward={ "chin_forward_pos" 127 "chin_forward_pos" 101 }
 		gene_chin_height={ "chin_height_neg" 129 "chin_height_neg" 99 }
 		gene_chin_width={ "chin_width_neg" 131 "chin_width_neg" 132 }
 		gene_eye_angle={ "eye_angle_neg" 147 "eye_angle_neg" 150 }
 		gene_eye_depth={ "eye_depth_pos" 166 "eye_depth_pos" 175 }
 		gene_eye_height={ "eye_height_pos" 147 "eye_height_pos" 146 }
 		gene_eye_distance={ "eye_distance_pos" 147 "eye_distance_pos" 81 }
 		gene_eye_shut={ "eye_shut_pos" 135 "eye_shut_neg" 168 }
 		gene_forehead_angle={ "forehead_angle_neg" 148 "forehead_angle_neg" 128 }
 		gene_forehead_brow_height={ "forehead_brow_height_neg" 153 "forehead_brow_height_neg" 175 }
 		gene_forehead_roundness={ "forehead_roundness_neg" 204 "forehead_roundness_neg" 215 }
 		gene_forehead_width={ "forehead_width_neg" 112 "forehead_width_neg" 154 }
 		gene_forehead_height={ "forehead_height_neg" 119 "forehead_height_neg" 154 }
 		gene_head_height={ "head_height_pos" 164 "head_height_pos" 130 }
 		gene_head_width={ "head_width_pos" 141 "head_width_pos" 102 }
 		gene_head_profile={ "head_profile_pos" 47 "head_profile_pos" 44 }
 		gene_head_top_height={ "head_top_height_pos" 149 "head_top_height_pos" 126 }
 		gene_head_top_width={ "head_top_width_neg" 145 "head_top_width_neg" 115 }
 		gene_jaw_angle={ "jaw_angle_pos" 176 "jaw_angle_pos" 103 }
 		gene_jaw_forward={ "jaw_forward_pos" 99 "jaw_forward_pos" 97 }
 		gene_jaw_height={ "jaw_height_pos" 109 "jaw_height_pos" 96 }
 		gene_jaw_width={ "jaw_width_neg" 137 "jaw_width_neg" 160 }
 		gene_mouth_corner_depth={ "mouth_corner_depth_neg" 174 "mouth_corner_depth_neg" 177 }
 		gene_mouth_corner_height={ "mouth_corner_height_pos" 133 "mouth_corner_height_pos" 113 }
 		gene_mouth_forward={ "mouth_forward_neg" 136 "mouth_forward_neg" 143 }
 		gene_mouth_height={ "mouth_height_neg" 157 "mouth_height_neg" 134 }
 		gene_mouth_width={ "mouth_width_neg" 110 "mouth_width_neg" 65 }
 		gene_mouth_upper_lip_size={ "mouth_upper_lip_size_pos" 125 "mouth_upper_lip_size_neg" 51 }
 		gene_mouth_lower_lip_size={ "mouth_lower_lip_size_pos" 118 "mouth_lower_lip_size_pos" 119 }
 		gene_mouth_open={ "mouth_open_neg" 22 "mouth_open_neg" 127 }
 		gene_neck_length={ "neck_length_pos" 138 "neck_length_pos" 136 }
 		gene_neck_width={ "neck_width_pos" 77 "neck_width_pos" 92 }
 		gene_bs_cheek_forward={ "cheek_forward_neg" 120 "cheek_forward_neg" 122 }
 		gene_bs_cheek_height={ "cheek_height_neg" 9 "cheek_height_pos" 113 }
 		gene_bs_cheek_width={ "cheek_width_pos" 79 "cheek_width_pos" 140 }
 		gene_bs_ear_angle={ "ear_angle_neg" 56 "ear_angle_pos" 56 }
 		gene_bs_ear_inner_shape={ "ear_inner_shape_pos" 31 "ear_inner_shape_pos" 178 }
 		gene_bs_ear_bend={ "ear_both_bend_pos" 34 "ear_both_bend_pos" 47 }
 		gene_bs_ear_outward={ "ear_outward_pos" 56 "ear_outward_pos" 47 }
 		gene_bs_ear_size={ "ear_size_pos" 39 "ear_size_pos" 117 }
 		gene_bs_eye_corner_depth={ "eye_corner_depth_pos" 19 "eye_corner_depth_neg" 124 }
 		gene_bs_eye_fold_shape={ "eye_fold_shape_neg" 0 "eye_fold_shape_neg" 39 }
 		gene_bs_eye_size={ "eye_size_neg" 148 "eye_size_neg" 86 }
 		gene_bs_eye_upper_lid_size={ "eye_upper_lid_size_neg" 28 "eye_upper_lid_size_neg" 69 }
 		gene_bs_forehead_brow_curve={ "forehead_brow_curve_neg" 242 "forehead_brow_curve_neg" 95 }
 		gene_bs_forehead_brow_forward={ "forehead_brow_forward_neg" 170 "forehead_brow_forward_pos" 76 }
 		gene_bs_forehead_brow_inner_height={ "forehead_brow_inner_height_neg" 8 "forehead_brow_inner_height_pos" 32 }
 		gene_bs_forehead_brow_outer_height={ "forehead_brow_outer_height_neg" 6 "forehead_brow_outer_height_pos" 57 }
 		gene_bs_forehead_brow_width={ "forehead_brow_width_pos" 75 "forehead_brow_width_pos" 114 }
 		gene_bs_jaw_def={ "jaw_def_pos" 50 "jaw_def_pos" 74 }
 		gene_bs_mouth_lower_lip_def={ "mouth_lower_lip_def_pos" 250 "mouth_lower_lip_def_pos" 35 }
 		gene_bs_mouth_lower_lip_full={ "mouth_lower_lip_full_pos" 15 "mouth_lower_lip_full_pos" 75 }
 		gene_bs_mouth_lower_lip_pad={ "mouth_lower_lip_pad_pos" 12 "mouth_lower_lip_pad_pos" 16 }
 		gene_bs_mouth_lower_lip_width={ "mouth_lower_lip_width_neg" 19 "mouth_lower_lip_width_neg" 3 }
 		gene_bs_mouth_philtrum_def={ "mouth_philtrum_def_pos" 79 "mouth_philtrum_def_pos" 82 }
 		gene_bs_mouth_philtrum_shape={ "mouth_philtrum_shape_neg" 50 "mouth_philtrum_shape_neg" 9 }
 		gene_bs_mouth_philtrum_width={ "mouth_philtrum_width_pos" 8 "mouth_philtrum_width_pos" 17 }
 		gene_bs_mouth_upper_lip_def={ "mouth_upper_lip_def_pos" 193 "mouth_upper_lip_def_pos" 10 }
 		gene_bs_mouth_upper_lip_full={ "mouth_upper_lip_full_pos" 63 "mouth_upper_lip_full_pos" 2 }
 		gene_bs_mouth_upper_lip_profile={ "mouth_upper_lip_profile_neg" 6 "mouth_upper_lip_profile_neg" 26 }
 		gene_bs_mouth_upper_lip_width={ "mouth_upper_lip_width_pos" 46 "mouth_upper_lip_width_pos" 41 }
 		gene_bs_nose_forward={ "nose_forward_pos" 30 "nose_forward_pos" 23 }
 		gene_bs_nose_height={ "nose_height_pos" 9 "nose_height_neg" 50 }
 		gene_bs_nose_length={ "nose_length_pos" 50 "nose_length_pos" 11 }
 		gene_bs_nose_nostril_height={ "nose_nostril_height_neg" 99 "nose_nostril_height_neg" 152 }
 		gene_bs_nose_nostril_width={ "nose_nostril_width_pos" 13 "nose_nostril_width_pos" 43 }
 		gene_bs_nose_profile={ "nose_profile_neg" 129 "nose_profile_neg" 10 }
 		gene_bs_nose_ridge_angle={ "nose_ridge_angle_pos" 115 "nose_ridge_angle_pos" 58 }
 		gene_bs_nose_ridge_width={ "nose_ridge_width_neg" 139 "nose_ridge_width_neg" 6 }
 		gene_bs_nose_size={ "nose_size_pos" 62 "nose_size_neg" 6 }
 		gene_bs_nose_tip_angle={ "nose_tip_angle_neg" 25 "nose_tip_angle_neg" 23 }
 		gene_bs_nose_tip_forward={ "nose_tip_forward_neg" 125 "nose_tip_forward_neg" 214 }
 		gene_bs_nose_tip_width={ "nose_tip_width_neg" 215 "nose_tip_width_neg" 134 }
 		face_detail_cheek_def={ "cheek_def_02" 57 "cheek_def_02" 50 }
 		face_detail_cheek_fat={ "cheek_fat_01_pos" 54 "cheek_fat_02_pos" 82 }
 		face_detail_chin_cleft={ "chin_cleft" 9 "chin_dimple" 10 }
 		face_detail_chin_def={ "chin_def" 79 "chin_def" 15 }
 		face_detail_eye_lower_lid_def={ "eye_lower_lid_def" 151 "eye_lower_lid_def" 148 }
 		face_detail_eye_socket={ "eye_socket_01" 129 "eye_socket_02" 75 }
 		face_detail_nasolabial={ "nasolabial_02" 45 "nasolabial_03" 104 }
 		face_detail_nose_ridge_def={ "nose_ridge_def_pos" 49 "nose_ridge_def_neg" 184 }
 		face_detail_nose_tip_def={ "nose_tip_def" 37 "nose_tip_def" 44 }
 		face_detail_temple_def={ "temple_def" 52 "temple_def" 156 }
 		expression_brow_wrinkles={ "brow_wrinkles_01" 16 "brow_wrinkles_02" 56 }
 		expression_eye_wrinkles={ "eye_wrinkles_01" 84 "eye_wrinkles_02" 70 }
 		expression_forehead_wrinkles={ "forehead_wrinkles_01" 133 "forehead_wrinkles_02" 138 }
 		expression_other={ "cheek_wrinkles_both_01" 60 "cheek_wrinkles_both_01" 46 }
 		complexion={ "complexion_1" 16 "complexion_1" 32 }
 		gene_height={ "normal_height" 152 "normal_height" 126 }
 		gene_bs_body_type={ "body_fat_head_fat_full" 152 "body_fat_head_fat_full" 139 }
 		gene_bs_body_shape={ "body_shape_pear_half" 153 "body_shape_rectangle_full" 193 }
 		gene_bs_bust={ "bust_clothes" 68 "bust_shape_4_full" 68 }
 		gene_age={ "old_1" 122 "old_4" 201 }
 		gene_eyebrows_shape={ "far_spacing_avg_thickness" 158 "avg_spacing_low_thickness" 191 }
 		gene_eyebrows_fullness={ "layer_2_low_thickness" 250 "layer_2_low_thickness" 199 }
 		gene_body_hair={ "body_hair_sparse_low_stubble" 123 "body_hair_sparse_low_stubble" 131 }
 		gene_hair_type={ "hair_straight_thin_beard" 134 "hair_straight_thin_beard" 121 }
 		gene_baldness={ "male_pattern_baldness" 166 "no_baldness" 127 }
 		eye_accessory={ "normal_eyes" 255 "normal_eyes" 255 }
 		teeth_accessory={ "normal_teeth" 0 "normal_teeth" 0 }
 		eyelashes_accessory={ "asian_eyelashes" 255 "asian_eyelashes" 255 }
 		face_detail_eye_upper_lid_def={ "default_upper_lid_def" 127 "default_upper_lid_def" 127 }
 		face_detail_monolid={ "monolid" 246 "monolid" 191 }
 		gene_eye_size={ "eye_size" 125 "eye_size" 124 }
 		gene_eye_shut_top={ "eye_shut_top" 148 "eye_shut_top" 132 }
 		gene_eye_shut_bottom={ "eye_shut_bottom" 126 "eye_shut_bottom" 117 }
 		gene_bs_eye_height_inside={ "eye_height_inside_neg" 72 "eye_height_inside_pos" 4 }
 		gene_bs_eye_height_outisde={ "eye_height_outisde_neg" 108 "eye_height_outisde_neg" 53 }
 		gene_bs_ear_lobe={ "ear_lobe_fused" 234 "ear_lobe_fused" 227 }
 		gene_bs_nose_central_width={ "nose_central_width_pos" 105 "nose_central_width_pos" 96 }
 		gene_bs_nose_septum_width={ "nose_septum_width_pos" 114 "nose_septum_width_pos" 121 }
 		gene_forehead_inner_brow_width={ "vanilla_inner_brow_width" 127 "vanilla_inner_brow_width" 127 }
 		gene_bs_mouth_lower_lip_profile={ "vanilla_lower_lip_profile" 127 "lower_lip_profile" 113 }
 		gene_bs_eye_outer_width={ "vanilla_eye_outer_width" 127 "vanilla_eye_outer_width" 127 }
 		gene_bs_head_asymmetry_1={ "vanilla_head_asymmetry_1" 127 "vanilla_head_asymmetry_1" 127 }
 		gene_bs_eye_fold_2={ "vanilla_eye_fold_2" 127 "vanilla_eye_fold_2" 127 }
 		gene_bs_mouth_center_curve={ "vanilla_mouth_center_curve" 127 "vanilla_mouth_center_curve" 127 }
 		gene_bs_eyebrow_straight={ "vanilla_eyebrow_straight" 127 "vanilla_eyebrow_straight" 127 }
 		gene_bs_head_round_shape={ "vanilla_head_round_shape" 127 "vanilla_head_round_shape" 127 }
 		gene_bs_nose_septum_height={ "vanilla_nose_septum_height" 127 "vanilla_nose_septum_height" 127 }
 		gene_bs_head_lower_height={ "vanilla_head_lower_height" 127 "vanilla_head_lower_height" 127 }
 		gene_bs_nose_flared_nostril={ "vanilla_nose_flared_nostril" 127 "vanilla_nose_flared_nostril" 127 }
 		gene_bs_mouth_upper_lip_forward={ "vanilla_upper_lip_forward" 127 "vanilla_upper_lip_forward" 127 }
 		gene_bs_mouth_lower_lip_forward={ "vanilla_lower_lip_forward" 127 "vanilla_lower_lip_forward" 127 }
 		gene_bs_nose_swollen={ "vanilla_nose_swollen" 127 "vanilla_nose_swollen" 127 }
 		gene_bs_ears_fantasy={ "vanilla_ears_fantasy" 127 "vanilla_ears_fantasy" 127 }
 		gene_bs_mouth_glamour_lips={ "vanilla_mouth_glamour_lips" 127 "vanilla_mouth_glamour_lips" 127 }
 		pose={ "" 255 "" 0 }
 		beards={ "japanese_beards" 174 "no_beard" 0 }
 		hairstyles={ "japanese_hairstyles_fp2" 174 "all_hairstyles" 0 }
 		legwear={ "japanese_tabi" 174 "all_legwear" 0 }
 		gene_shrink_body={ "shrink_all" 255 "" 0 }
 		gene_bs_additive_headgears={ "additive_headgears" 255 "" 0 }
 		gene_bs_cloak_offset={ "cloak_offset" 255 "" 0 }
 		gene_bs_long_beard={ "long_beard" 255 "" 0 }
 		clothes={ "japanese_high_nobility_clothes" 81 "most_clothes" 0 }
 		shogunate_special_belt_accessory={ "katana_01" 105 "katana_01" 0 }
 		headgear={ "shogunate_suieikan" 174 "no_headgear" 0 }
 		gene_balding_hair_effect={ "no_baldness" 255 "no_baldness" 0 }
 }
	entity={ 1663114784 1663114784 }
	tags={ {
			hash=1989892411
			invert=no
		}
 {
			hash=2125534279
			invert=no
		}
 {
			hash=3941715396
			invert=no
		}
 {
			hash=681013727
			invert=no
		}
 {
			hash=3672171019
			invert=no
		}
 {
			hash=2670398845
			invert=no
		}
 {
			hash=1500963499
			invert=no
		}
 {
			hash=1563190100
			invert=no
		}
 }
}

