# Auto generated file, do not edit manually. Created using console command dump_bookmark_portraits
# History database id:10056244
bm_shikokucampaign1585_kikkawa_motonaga={
	type=male
	id=25427
	age=0.370000
	genes={ 		hair_color={ 12 242 12 243 }
 		skin_color={ 109 49 110 50 }
 		eye_color={ 9 244 29 244 }
 		gene_chin_forward={ "chin_forward_pos" 139 "chin_forward_pos" 103 }
 		gene_chin_height={ "chin_height_neg" 138 "chin_height_neg" 117 }
 		gene_chin_width={ "chin_width_neg" 144 "chin_width_neg" 130 }
 		gene_eye_angle={ "eye_angle_neg" 150 "eye_angle_neg" 105 }
 		gene_eye_depth={ "eye_depth_pos" 180 "eye_depth_pos" 175 }
 		gene_eye_height={ "eye_height_pos" 141 "eye_height_pos" 146 }
 		gene_eye_distance={ "eye_distance_pos" 85 "eye_distance_pos" 94 }
 		gene_eye_shut={ "eye_shut_pos" 128 "eye_shut_pos" 160 }
 		gene_forehead_angle={ "forehead_angle_neg" 132 "forehead_angle_neg" 140 }
 		gene_forehead_brow_height={ "forehead_brow_height_neg" 149 "forehead_brow_height_neg" 163 }
 		gene_forehead_roundness={ "forehead_roundness_neg" 123 "forehead_roundness_neg" 137 }
 		gene_forehead_width={ "forehead_width_neg" 102 "forehead_width_neg" 136 }
 		gene_forehead_height={ "forehead_height_neg" 127 "forehead_height_neg" 165 }
 		gene_head_height={ "head_height_pos" 127 "head_height_pos" 149 }
 		gene_head_width={ "head_width_pos" 91 "head_width_pos" 90 }
 		gene_head_profile={ "head_profile_pos" 56 "head_profile_pos" 32 }
 		gene_head_top_height={ "head_top_height_pos" 131 "head_top_height_pos" 152 }
 		gene_head_top_width={ "head_top_width_neg" 137 "head_top_width_neg" 160 }
 		gene_jaw_angle={ "jaw_angle_pos" 127 "jaw_angle_pos" 145 }
 		gene_jaw_forward={ "jaw_forward_pos" 95 "jaw_forward_neg" 103 }
 		gene_jaw_height={ "jaw_height_pos" 118 "jaw_height_pos" 121 }
 		gene_jaw_width={ "jaw_width_neg" 130 "jaw_width_neg" 124 }
 		gene_mouth_corner_depth={ "mouth_corner_depth_neg" 181 "mouth_corner_depth_neg" 181 }
 		gene_mouth_corner_height={ "mouth_corner_height_pos" 113 "mouth_corner_height_pos" 124 }
 		gene_mouth_forward={ "mouth_forward_neg" 152 "mouth_forward_neg" 134 }
 		gene_mouth_height={ "mouth_height_neg" 153 "mouth_height_neg" 159 }
 		gene_mouth_width={ "mouth_width_neg" 50 "mouth_width_neg" 16 }
 		gene_mouth_upper_lip_size={ "mouth_upper_lip_size_neg" 83 "mouth_upper_lip_size_neg" 119 }
 		gene_mouth_lower_lip_size={ "mouth_lower_lip_size_pos" 124 "mouth_lower_lip_size_pos" 111 }
 		gene_mouth_open={ "mouth_open_neg" 46 "mouth_open_neg" 132 }
 		gene_neck_length={ "neck_length_pos" 138 "neck_length_pos" 130 }
 		gene_neck_width={ "neck_width_pos" 94 "neck_width_pos" 84 }
 		gene_bs_cheek_forward={ "cheek_forward_neg" 119 "cheek_forward_neg" 117 }
 		gene_bs_cheek_height={ "cheek_height_pos" 179 "cheek_height_neg" 8 }
 		gene_bs_cheek_width={ "cheek_width_pos" 76 "cheek_width_pos" 96 }
 		gene_bs_ear_angle={ "ear_angle_pos" 46 "ear_angle_pos" 5 }
 		gene_bs_ear_inner_shape={ "ear_inner_shape_pos" 11 "ear_inner_shape_pos" 3 }
 		gene_bs_ear_bend={ "ear_both_bend_pos" 56 "ear_both_bend_pos" 51 }
 		gene_bs_ear_outward={ "ear_outward_pos" 13 "ear_outward_pos" 17 }
 		gene_bs_ear_size={ "ear_size_pos" 54 "ear_size_pos" 11 }
 		gene_bs_eye_corner_depth={ "eye_corner_depth_neg" 60 "eye_corner_depth_pos" 29 }
 		gene_bs_eye_fold_shape={ "eye_fold_shape_neg" 0 "eye_fold_shape_neg" 0 }
 		gene_bs_eye_size={ "eye_size_neg" 81 "eye_size_neg" 93 }
 		gene_bs_eye_upper_lid_size={ "eye_upper_lid_size_neg" 33 "eye_upper_lid_size_neg" 59 }
 		gene_bs_forehead_brow_curve={ "forehead_brow_curve_neg" 91 "forehead_brow_curve_neg" 79 }
 		gene_bs_forehead_brow_forward={ "forehead_brow_forward_neg" 73 "forehead_brow_forward_neg" 12 }
 		gene_bs_forehead_brow_inner_height={ "forehead_brow_inner_height_pos" 102 "forehead_brow_inner_height_pos" 28 }
 		gene_bs_forehead_brow_outer_height={ "forehead_brow_outer_height_pos" 85 "forehead_brow_outer_height_pos" 34 }
 		gene_bs_forehead_brow_width={ "forehead_brow_width_neg" 115 "forehead_brow_width_pos" 106 }
 		gene_bs_jaw_def={ "jaw_def_neg" 51 "jaw_def_neg" 45 }
 		gene_bs_mouth_lower_lip_def={ "mouth_lower_lip_def_pos" 38 "mouth_lower_lip_def_pos" 36 }
 		gene_bs_mouth_lower_lip_full={ "mouth_lower_lip_full_pos" 87 "mouth_lower_lip_full_pos" 76 }
 		gene_bs_mouth_lower_lip_pad={ "mouth_lower_lip_pad_pos" 13 "mouth_lower_lip_pad_pos" 8 }
 		gene_bs_mouth_lower_lip_width={ "mouth_lower_lip_width_pos" 60 "mouth_lower_lip_width_neg" 11 }
 		gene_bs_mouth_philtrum_def={ "mouth_philtrum_def_pos" 87 "mouth_philtrum_def_pos" 68 }
 		gene_bs_mouth_philtrum_shape={ "mouth_philtrum_shape_neg" 6 "mouth_philtrum_shape_neg" 3 }
 		gene_bs_mouth_philtrum_width={ "mouth_philtrum_width_pos" 46 "mouth_philtrum_width_pos" 15 }
 		gene_bs_mouth_upper_lip_def={ "mouth_upper_lip_def_pos" 122 "mouth_upper_lip_def_pos" 124 }
 		gene_bs_mouth_upper_lip_full={ "mouth_upper_lip_full_pos" 4 "mouth_upper_lip_full_pos" 159 }
 		gene_bs_mouth_upper_lip_profile={ "mouth_upper_lip_profile_neg" 1 "mouth_upper_lip_profile_neg" 33 }
 		gene_bs_mouth_upper_lip_width={ "mouth_upper_lip_width_pos" 56 "mouth_upper_lip_width_pos" 50 }
 		gene_bs_nose_forward={ "nose_forward_pos" 55 "nose_forward_pos" 23 }
 		gene_bs_nose_height={ "nose_height_neg" 54 "nose_height_neg" 4 }
 		gene_bs_nose_length={ "nose_length_neg" 77 "nose_length_neg" 0 }
 		gene_bs_nose_nostril_height={ "nose_nostril_height_neg" 77 "nose_nostril_height_pos" 41 }
 		gene_bs_nose_nostril_width={ "nose_nostril_width_pos" 44 "nose_nostril_width_pos" 20 }
 		gene_bs_nose_profile={ "nose_profile_pos" 18 "nose_profile_neg" 18 }
 		gene_bs_nose_ridge_angle={ "nose_ridge_angle_pos" 68 "nose_ridge_angle_pos" 94 }
 		gene_bs_nose_ridge_width={ "nose_ridge_width_neg" 24 "nose_ridge_width_neg" 27 }
 		gene_bs_nose_size={ "nose_size_neg" 239 "nose_size_neg" 71 }
 		gene_bs_nose_tip_angle={ "nose_tip_angle_neg" 5 "nose_tip_angle_neg" 6 }
 		gene_bs_nose_tip_forward={ "nose_tip_forward_neg" 127 "nose_tip_forward_neg" 117 }
 		gene_bs_nose_tip_width={ "nose_tip_width_neg" 133 "nose_tip_width_neg" 181 }
 		face_detail_cheek_def={ "cheek_def_02" 43 "cheek_def_01" 99 }
 		face_detail_cheek_fat={ "cheek_fat_04_pos" 41 "cheek_fat_04_pos" 114 }
 		face_detail_chin_cleft={ "chin_dimple" 6 "chin_cleft" 18 }
 		face_detail_chin_def={ "chin_def" 94 "chin_def" 30 }
 		face_detail_eye_lower_lid_def={ "eye_lower_lid_def" 176 "eye_lower_lid_def" 194 }
 		face_detail_eye_socket={ "eye_socket_01" 173 "eye_socket_01" 112 }
 		face_detail_nasolabial={ "nasolabial_01" 60 "nasolabial_03" 101 }
 		face_detail_nose_ridge_def={ "nose_ridge_def_neg" 203 "nose_ridge_def_neg" 227 }
 		face_detail_nose_tip_def={ "nose_tip_def" 62 "nose_tip_def" 135 }
 		face_detail_temple_def={ "temple_def" 55 "temple_def" 85 }
 		expression_brow_wrinkles={ "brow_wrinkles_01" 172 "brow_wrinkles_02" 255 }
 		expression_eye_wrinkles={ "eye_wrinkles_01" 255 "eye_wrinkles_01" 255 }
 		expression_forehead_wrinkles={ "forehead_wrinkles_01" 136 "forehead_wrinkles_03" 50 }
 		expression_other={ "cheek_wrinkles_both_01" 188 "cheek_wrinkles_both_01" 54 }
 		complexion={ "complexion_1" 55 "complexion_1" 57 }
 		gene_height={ "normal_height" 113 "normal_height" 123 }
 		gene_bs_body_type={ "body_fat_head_fat_low" 134 "body_fat_head_fat_low" 121 }
 		gene_bs_body_shape={ "body_shape_average" 0 "body_shape_hourglass_half" 0 }
 		gene_bs_bust={ "bust_clothes" 125 "bust_default" 103 }
 		gene_age={ "old_1" 139 "old_1" 90 }
 		gene_eyebrows_shape={ "far_spacing_avg_thickness" 246 "far_spacing_avg_thickness" 232 }
 		gene_eyebrows_fullness={ "layer_2_high_thickness" 194 "layer_2_low_thickness" 190 }
 		gene_body_hair={ "body_hair_sparse_low_stubble" 144 "body_hair_sparse_low_stubble" 54 }
 		gene_hair_type={ "hair_straight_thin_beard" 138 "hair_straight_thin_beard" 176 }
 		gene_baldness={ "male_pattern_baldness" 113 "no_baldness" 127 }
 		eye_accessory={ "normal_eyes" 255 "normal_eyes" 255 }
 		teeth_accessory={ "normal_teeth" 0 "normal_teeth" 0 }
 		eyelashes_accessory={ "asian_eyelashes" 255 "asian_eyelashes" 255 }
 		face_detail_eye_upper_lid_def={ "default_upper_lid_def" 127 "default_upper_lid_def" 127 }
 		face_detail_monolid={ "monolid" 241 "monolid" 255 }
 		gene_eye_size={ "eye_size" 129 "eye_size" 139 }
 		gene_eye_shut_top={ "eye_shut_top" 138 "eye_shut_top" 104 }
 		gene_eye_shut_bottom={ "eye_shut_bottom" 105 "eye_shut_bottom" 118 }
 		gene_bs_eye_height_inside={ "eye_height_inside_pos" 13 "eye_height_inside_pos" 4 }
 		gene_bs_eye_height_outisde={ "eye_height_outisde_neg" 100 "eye_height_outisde_pos" 48 }
 		gene_bs_ear_lobe={ "ear_lobe_fused" 216 "ear_lobe_fused" 209 }
 		gene_bs_nose_central_width={ "nose_central_width_pos" 6 "nose_central_width_pos" 119 }
 		gene_bs_nose_septum_width={ "nose_septum_width_pos" 5 "nose_septum_width_pos" 54 }
 		gene_forehead_inner_brow_width={ "vanilla_inner_brow_width" 127 "vanilla_inner_brow_width" 127 }
 		gene_bs_mouth_lower_lip_profile={ "lower_lip_profile" 126 "vanilla_lower_lip_profile" 127 }
 		gene_bs_eye_outer_width={ "vanilla_eye_outer_width" 127 "vanilla_eye_outer_width" 127 }
 		gene_bs_head_asymmetry_1={ "vanilla_head_asymmetry_1" 127 "vanilla_head_asymmetry_1" 127 }
 		gene_bs_eye_fold_2={ "vanilla_eye_fold_2" 127 "vanilla_eye_fold_2" 127 }
 		gene_bs_mouth_center_curve={ "vanilla_mouth_center_curve" 127 "vanilla_mouth_center_curve" 127 }
 		gene_bs_eyebrow_straight={ "vanilla_eyebrow_straight" 127 "vanilla_eyebrow_straight" 127 }
 		gene_bs_head_round_shape={ "vanilla_head_round_shape" 127 "vanilla_head_round_shape" 127 }
 		gene_bs_nose_septum_height={ "vanilla_nose_septum_height" 127 "vanilla_nose_septum_height" 127 }
 		gene_bs_head_lower_height={ "vanilla_head_lower_height" 127 "vanilla_head_lower_height" 127 }
 		gene_bs_nose_flared_nostril={ "vanilla_nose_flared_nostril" 127 "vanilla_nose_flared_nostril" 127 }
 		gene_bs_mouth_upper_lip_forward={ "vanilla_upper_lip_forward" 127 "vanilla_upper_lip_forward" 127 }
 		gene_bs_mouth_lower_lip_forward={ "vanilla_lower_lip_forward" 127 "vanilla_lower_lip_forward" 127 }
 		gene_bs_nose_swollen={ "vanilla_nose_swollen" 127 "vanilla_nose_swollen" 127 }
 		gene_bs_ears_fantasy={ "vanilla_ears_fantasy" 127 "vanilla_ears_fantasy" 127 }
 		gene_bs_mouth_glamour_lips={ "vanilla_mouth_glamour_lips" 127 "vanilla_mouth_glamour_lips" 127 }
 		pose={ "" 255 "" 0 }
 		beards={ "japanese_beards" 142 "no_beard" 0 }
 		hairstyles={ "japanese_hairstyles_fp2" 2 "all_hairstyles" 0 }
 		legwear={ "japanese_war_legwear" 255 "all_legwear" 0 }
 		gene_shrink_body={ "shrink_all" 255 "" 0 }
 		gene_bs_additive_headgears={ "additive_headgears" 255 "" 0 }
 		gene_bs_cloak_offset={ "cloak_offset" 255 "" 0 }
 		gene_bs_long_beard={ "long_beard" 255 "" 0 }
 		clothes={ "japanese_war_nobility_clothes" 255 "most_clothes" 0 }
 		headgear={ "no_headgear" 51 "no_headgear" 0 }
 		shogunate_special_belt_accessory={ "katana_01" 255 "katana_01" 0 }
 		gene_balding_hair_effect={ "baldness_stage_2" 255 "no_baldness" 0 }
 }
	entity={ 979141817 979141817 }
	tags={ {
			hash=4196965543
			invert=no
		}
 {
			hash=2125534279
			invert=no
		}
 {
			hash=2670398845
			invert=no
		}
 {
			hash=2951898311
			invert=no
		}
 {
			hash=681013727
			invert=no
		}
 {
			hash=1563190100
			invert=no
		}
 }
}

