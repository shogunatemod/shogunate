# Auto generated file, do not edit manually. Created using console command dump_bookmark_portraits
# History database id:10029415
bm_osaka1614_toyotomi_hideyori={
	type=male
	id=27536
	age=0.210000
	genes={ 		hair_color={ 19 244 15 244 }
 		skin_color={ 190 54 181 51 }
 		eye_color={ 5 253 38 242 }
 		gene_chin_forward={ "chin_forward_pos" 228 "chin_forward_pos" 114 }
 		gene_chin_height={ "chin_height_neg" 119 "chin_height_neg" 129 }
 		gene_chin_width={ "chin_width_neg" 132 "chin_width_neg" 129 }
 		gene_eye_angle={ "eye_angle_neg" 110 "eye_angle_neg" 108 }
 		gene_eye_depth={ "eye_depth_pos" 175 "eye_depth_pos" 103 }
 		gene_eye_height={ "eye_height_pos" 146 "eye_height_pos" 143 }
 		gene_eye_distance={ "eye_distance_pos" 104 "eye_distance_pos" 147 }
 		gene_eye_shut={ "eye_shut_pos" 70 "eye_shut_pos" 137 }
 		gene_forehead_angle={ "forehead_angle_neg" 133 "forehead_angle_neg" 148 }
 		gene_forehead_brow_height={ "forehead_brow_height_neg" 149 "forehead_brow_height_neg" 152 }
 		gene_forehead_roundness={ "forehead_roundness_neg" 186 "forehead_roundness_neg" 140 }
 		gene_forehead_width={ "forehead_width_neg" 143 "forehead_width_neg" 150 }
 		gene_forehead_height={ "forehead_height_neg" 0 "forehead_height_neg" 145 }
 		gene_head_height={ "head_height_pos" 195 "head_height_pos" 130 }
 		gene_head_width={ "head_width_pos" 101 "head_width_pos" 114 }
 		gene_head_profile={ "head_profile_pos" 47 "head_profile_pos" 40 }
 		gene_head_top_height={ "head_top_height_pos" 124 "head_top_height_pos" 149 }
 		gene_head_top_width={ "head_top_width_neg" 135 "head_top_width_neg" 145 }
 		gene_jaw_angle={ "jaw_angle_pos" 144 "jaw_angle_pos" 103 }
 		gene_jaw_forward={ "jaw_forward_pos" 188 "jaw_forward_pos" 97 }
 		gene_jaw_height={ "jaw_height_pos" 241 "jaw_height_pos" 108 }
 		gene_jaw_width={ "jaw_width_neg" 162 "jaw_width_neg" 114 }
 		gene_mouth_corner_depth={ "mouth_corner_depth_neg" 140 "mouth_corner_depth_neg" 174 }
 		gene_mouth_corner_height={ "mouth_corner_height_pos" 113 "mouth_corner_height_pos" 122 }
 		gene_mouth_forward={ "mouth_forward_neg" 143 "mouth_forward_neg" 140 }
 		gene_mouth_height={ "mouth_height_neg" 150 "mouth_height_neg" 157 }
 		gene_mouth_width={ "mouth_width_neg" 114 "mouth_width_neg" 94 }
 		gene_mouth_upper_lip_size={ "mouth_upper_lip_size_neg" 51 "mouth_upper_lip_size_pos" 123 }
 		gene_mouth_lower_lip_size={ "mouth_lower_lip_size_pos" 119 "mouth_lower_lip_size_pos" 112 }
 		gene_mouth_open={ "mouth_open_neg" 127 "mouth_open_neg" 114 }
 		gene_neck_length={ "neck_length_pos" 138 "neck_length_pos" 134 }
 		gene_neck_width={ "neck_width_pos" 82 "neck_width_pos" 87 }
 		gene_bs_cheek_forward={ "cheek_forward_neg" 118 "cheek_forward_neg" 122 }
 		gene_bs_cheek_height={ "cheek_height_neg" 167 "cheek_height_pos" 113 }
 		gene_bs_cheek_width={ "cheek_width_pos" 236 "cheek_width_pos" 127 }
 		gene_bs_ear_angle={ "ear_angle_pos" 56 "ear_angle_pos" 37 }
 		gene_bs_ear_inner_shape={ "ear_inner_shape_pos" 14 "ear_inner_shape_pos" 31 }
 		gene_bs_ear_bend={ "ear_both_bend_pos" 34 "ear_both_bend_pos" 47 }
 		gene_bs_ear_outward={ "ear_outward_pos" 61 "ear_outward_pos" 47 }
 		gene_bs_ear_size={ "ear_size_pos" 41 "ear_size_pos" 117 }
 		gene_bs_eye_corner_depth={ "eye_corner_depth_neg" 124 "eye_corner_depth_neg" 102 }
 		gene_bs_eye_fold_shape={ "eye_fold_shape_neg" 0 "eye_fold_shape_neg" 0 }
 		gene_bs_eye_size={ "eye_size_neg" 103 "eye_size_neg" 110 }
 		gene_bs_eye_upper_lid_size={ "eye_upper_lid_size_neg" 44 "eye_upper_lid_size_neg" 69 }
 		gene_bs_forehead_brow_curve={ "forehead_brow_curve_neg" 89 "forehead_brow_curve_neg" 189 }
 		gene_bs_forehead_brow_forward={ "forehead_brow_forward_pos" 238 "forehead_brow_forward_neg" 84 }
 		gene_bs_forehead_brow_inner_height={ "forehead_brow_inner_height_pos" 127 "forehead_brow_inner_height_pos" 32 }
 		gene_bs_forehead_brow_outer_height={ "forehead_brow_outer_height_neg" 25 "forehead_brow_outer_height_pos" 57 }
 		gene_bs_forehead_brow_width={ "forehead_brow_width_pos" 117 "forehead_brow_width_pos" 75 }
 		gene_bs_jaw_def={ "jaw_def_pos" 74 "jaw_def_neg" 34 }
 		gene_bs_mouth_lower_lip_def={ "mouth_lower_lip_def_pos" 35 "mouth_lower_lip_def_pos" 35 }
 		gene_bs_mouth_lower_lip_full={ "mouth_lower_lip_full_pos" 75 "mouth_lower_lip_full_pos" 73 }
 		gene_bs_mouth_lower_lip_pad={ "mouth_lower_lip_pad_pos" 9 "mouth_lower_lip_pad_pos" 16 }
 		gene_bs_mouth_lower_lip_width={ "mouth_lower_lip_width_pos" 64 "mouth_lower_lip_width_neg" 15 }
 		gene_bs_mouth_philtrum_def={ "mouth_philtrum_def_pos" 79 "mouth_philtrum_def_pos" 63 }
 		gene_bs_mouth_philtrum_shape={ "mouth_philtrum_shape_neg" 17 "mouth_philtrum_shape_neg" 11 }
 		gene_bs_mouth_philtrum_width={ "mouth_philtrum_width_pos" 84 "mouth_philtrum_width_pos" 40 }
 		gene_bs_mouth_upper_lip_def={ "mouth_upper_lip_def_pos" 112 "mouth_upper_lip_def_pos" 123 }
 		gene_bs_mouth_upper_lip_full={ "mouth_upper_lip_full_pos" 70 "mouth_upper_lip_full_pos" 80 }
 		gene_bs_mouth_upper_lip_profile={ "mouth_upper_lip_profile_neg" 6 "mouth_upper_lip_profile_neg" 31 }
 		gene_bs_mouth_upper_lip_width={ "mouth_upper_lip_width_pos" 40 "mouth_upper_lip_width_pos" 41 }
 		gene_bs_nose_forward={ "nose_forward_pos" 30 "nose_forward_pos" 78 }
 		gene_bs_nose_height={ "nose_height_neg" 59 "nose_height_neg" 55 }
 		gene_bs_nose_length={ "nose_length_neg" 79 "nose_length_pos" 0 }
 		gene_bs_nose_nostril_height={ "nose_nostril_height_pos" 44 "nose_nostril_height_pos" 99 }
 		gene_bs_nose_nostril_width={ "nose_nostril_width_pos" 17 "nose_nostril_width_pos" 13 }
 		gene_bs_nose_profile={ "nose_profile_neg" 25 "nose_profile_neg" 23 }
 		gene_bs_nose_ridge_angle={ "nose_ridge_angle_pos" 115 "nose_ridge_angle_neg" 17 }
 		gene_bs_nose_ridge_width={ "nose_ridge_width_neg" 6 "nose_ridge_width_pos" 33 }
 		gene_bs_nose_size={ "nose_size_neg" 6 "nose_size_pos" 17 }
 		gene_bs_nose_tip_angle={ "nose_tip_angle_neg" 25 "nose_tip_angle_pos" 56 }
 		gene_bs_nose_tip_forward={ "nose_tip_forward_neg" 125 "nose_tip_forward_neg" 118 }
 		gene_bs_nose_tip_width={ "nose_tip_width_neg" 134 "nose_tip_width_neg" 163 }
 		face_detail_cheek_def={ "cheek_def_01" 36 "cheek_def_02" 57 }
 		face_detail_cheek_fat={ "cheek_fat_04_pos" 60 "cheek_fat_03_pos" 120 }
 		face_detail_chin_cleft={ "chin_cleft" 6 "chin_dimple" 10 }
 		face_detail_chin_def={ "chin_def" 79 "chin_def" 90 }
 		face_detail_eye_lower_lid_def={ "eye_lower_lid_def" 214 "eye_lower_lid_def" 148 }
 		face_detail_eye_socket={ "eye_socket_01" 129 "eye_socket_01" 53 }
 		face_detail_nasolabial={ "nasolabial_03" 35 "nasolabial_04" 91 }
 		face_detail_nose_ridge_def={ "nose_ridge_def_pos" 49 "nose_ridge_def_neg" 232 }
 		face_detail_nose_tip_def={ "nose_tip_def" 46 "nose_tip_def" 37 }
 		face_detail_temple_def={ "temple_def" 140 "temple_def" 52 }
 		expression_brow_wrinkles={ "brow_wrinkles_02" 56 "brow_wrinkles_04" 31 }
 		expression_eye_wrinkles={ "eye_wrinkles_03" 171 "eye_wrinkles_01" 255 }
 		expression_forehead_wrinkles={ "forehead_wrinkles_01" 255 "forehead_wrinkles_01" 84 }
 		expression_other={ "cheek_wrinkles_both_01" 38 "nose_wrinkles_01" 70 }
 		complexion={ "complexion_1" 16 "complexion_1" 83 }
 		gene_height={ "giant_height" 0 "normal_height" 152 }
 		gene_bs_body_type={ "body_fat_head_fat_full" 138 "body_fat_head_fat_full" 119 }
 		gene_bs_body_shape={ "body_shape_rectangle_full" 255 "body_shape_rectangle_half" 255 }
 		gene_bs_bust={ "bust_clothes" 104 "bust_clothes" 85 }
 		gene_age={ "old_1" 122 "old_1" 80 }
 		gene_eyebrows_shape={ "avg_spacing_low_thickness" 191 "far_spacing_low_thickness" 196 }
 		gene_eyebrows_fullness={ "layer_2_low_thickness" 226 "layer_2_low_thickness" 190 }
 		gene_body_hair={ "body_hair_sparse_low_stubble" 102 "body_hair_sparse_low_stubble" 123 }
 		gene_hair_type={ "hair_straight_thin_beard" 92 "hair_straight_thin_beard" 121 }
 		gene_baldness={ "no_baldness" 127 "no_baldness" 127 }
 		eye_accessory={ "normal_eyes" 255 "normal_eyes" 255 }
 		teeth_accessory={ "normal_teeth" 0 "normal_teeth" 0 }
 		eyelashes_accessory={ "asian_eyelashes" 255 "asian_eyelashes" 255 }
 		face_detail_eye_upper_lid_def={ "default_upper_lid_def" 127 "default_upper_lid_def" 127 }
 		face_detail_monolid={ "monolid" 246 "monolid" 247 }
 		gene_eye_size={ "eye_size" 135 "eye_size" 124 }
 		gene_eye_shut_top={ "eye_shut_top" 130 "eye_shut_top" 150 }
 		gene_eye_shut_bottom={ "eye_shut_bottom" 122 "eye_shut_bottom" 123 }
 		gene_bs_eye_height_inside={ "eye_height_inside_pos" 9 "eye_height_inside_pos" 16 }
 		gene_bs_eye_height_outisde={ "eye_height_outisde_neg" 108 "eye_height_outisde_pos" 46 }
 		gene_bs_ear_lobe={ "ear_lobe_fused" 234 "ear_lobe_fused" 212 }
 		gene_bs_nose_central_width={ "nose_central_width_pos" 96 "nose_central_width_neg" 58 }
 		gene_bs_nose_septum_width={ "nose_septum_width_pos" 121 "nose_septum_width_pos" 115 }
 		gene_forehead_inner_brow_width={ "vanilla_inner_brow_width" 127 "vanilla_inner_brow_width" 127 }
 		gene_bs_mouth_lower_lip_profile={ "vanilla_lower_lip_profile" 127 "lower_lip_profile" 97 }
 		gene_bs_eye_outer_width={ "vanilla_eye_outer_width" 127 "vanilla_eye_outer_width" 127 }
 		gene_bs_head_asymmetry_1={ "vanilla_head_asymmetry_1" 127 "vanilla_head_asymmetry_1" 127 }
 		gene_bs_eye_fold_2={ "vanilla_eye_fold_2" 127 "vanilla_eye_fold_2" 127 }
 		gene_bs_mouth_center_curve={ "vanilla_mouth_center_curve" 127 "vanilla_mouth_center_curve" 127 }
 		gene_bs_eyebrow_straight={ "vanilla_eyebrow_straight" 127 "vanilla_eyebrow_straight" 127 }
 		gene_bs_head_round_shape={ "vanilla_head_round_shape" 127 "vanilla_head_round_shape" 127 }
 		gene_bs_nose_septum_height={ "vanilla_nose_septum_height" 127 "vanilla_nose_septum_height" 127 }
 		gene_bs_head_lower_height={ "vanilla_head_lower_height" 127 "vanilla_head_lower_height" 127 }
 		gene_bs_nose_flared_nostril={ "vanilla_nose_flared_nostril" 127 "vanilla_nose_flared_nostril" 127 }
 		gene_bs_mouth_upper_lip_forward={ "vanilla_upper_lip_forward" 127 "vanilla_upper_lip_forward" 127 }
 		gene_bs_mouth_lower_lip_forward={ "vanilla_lower_lip_forward" 127 "vanilla_lower_lip_forward" 127 }
 		gene_bs_nose_swollen={ "vanilla_nose_swollen" 127 "vanilla_nose_swollen" 127 }
 		gene_bs_ears_fantasy={ "vanilla_ears_fantasy" 127 "vanilla_ears_fantasy" 127 }
 		gene_bs_mouth_glamour_lips={ "vanilla_mouth_glamour_lips" 127 "vanilla_mouth_glamour_lips" 127 }
 		pose={ "" 255 "" 0 }
 		beards={ "japanese_beards" 162 "no_beard" 0 }
 		hairstyles={ "japanese_hairstyles_fp2" 134 "all_hairstyles" 0 }
 		legwear={ "japanese_tabi" 162 "all_legwear" 0 }
 		gene_shrink_body={ "shrink_all" 255 "" 0 }
 		gene_bs_additive_headgears={ "additive_headgears" 255 "" 0 }
 		gene_bs_cloak_offset={ "cloak_offset" 255 "" 0 }
 		gene_bs_long_beard={ "long_beard" 255 "" 0 }
 		clothes={ "japanese_high_nobility_clothes" 30 "most_clothes" 0 }
 		shogunate_special_belt_accessory={ "katana_01" 139 "katana_01" 0 }
 		headgear={ "shogunate_suieikan" 162 "no_headgear" 0 }
 		gene_balding_hair_effect={ "no_baldness" 255 "no_baldness" 0 }
 }
	entity={ 1903276529 1903276529 }
	tags={ {
			hash=1989892411
			invert=no
		}
 {
			hash=2125534279
			invert=no
		}
 {
			hash=3941715396
			invert=no
		}
 {
			hash=681013727
			invert=no
		}
 {
			hash=3672171019
			invert=no
		}
 {
			hash=4196965543
			invert=no
		}
 {
			hash=2670398845
			invert=no
		}
 {
			hash=1500963499
			invert=no
		}
 {
			hash=1563190100
			invert=no
		}
 }
}

