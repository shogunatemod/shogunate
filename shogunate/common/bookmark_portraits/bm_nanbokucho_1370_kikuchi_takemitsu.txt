# Auto generated file, do not edit manually. Created using console command dump_bookmark_portraits
# History database id:10066022
bm_nanbokucho_1370_kikuchi_takemitsu={
	type=male
	id=16024
	age=0.510000
	genes={ 		hair_color={ 9 254 9 254 }
 		skin_color={ 204 42 204 42 }
 		eye_color={ 2 206 2 206 }
 		gene_chin_forward={ "chin_forward_pos" 135 "chin_forward_pos" 135 }
 		gene_chin_height={ "chin_height_neg" 106 "chin_height_neg" 106 }
 		gene_chin_width={ "chin_width_neg" 142 "chin_width_neg" 142 }
 		gene_eye_angle={ "eye_angle_neg" 150 "eye_angle_neg" 150 }
 		gene_eye_depth={ "eye_depth_pos" 173 "eye_depth_pos" 173 }
 		gene_eye_height={ "eye_height_pos" 130 "eye_height_pos" 130 }
 		gene_eye_distance={ "eye_distance_pos" 91 "eye_distance_pos" 91 }
 		gene_eye_shut={ "eye_shut_pos" 135 "eye_shut_pos" 135 }
 		gene_forehead_angle={ "forehead_angle_neg" 133 "forehead_angle_neg" 133 }
 		gene_forehead_brow_height={ "forehead_brow_height_neg" 164 "forehead_brow_height_neg" 164 }
 		gene_forehead_roundness={ "forehead_roundness_neg" 174 "forehead_roundness_neg" 174 }
 		gene_forehead_width={ "forehead_width_neg" 113 "forehead_width_neg" 113 }
 		gene_forehead_height={ "forehead_height_neg" 124 "forehead_height_neg" 124 }
 		gene_head_height={ "head_height_pos" 131 "head_height_pos" 131 }
 		gene_head_width={ "head_width_pos" 130 "head_width_pos" 130 }
 		gene_head_profile={ "head_profile_pos" 55 "head_profile_pos" 55 }
 		gene_head_top_height={ "head_top_height_pos" 147 "head_top_height_pos" 147 }
 		gene_head_top_width={ "head_top_width_neg" 97 "head_top_width_neg" 97 }
 		gene_jaw_angle={ "jaw_angle_pos" 177 "jaw_angle_pos" 177 }
 		gene_jaw_forward={ "jaw_forward_pos" 116 "jaw_forward_pos" 116 }
 		gene_jaw_height={ "jaw_height_pos" 100 "jaw_height_pos" 100 }
 		gene_jaw_width={ "jaw_width_neg" 130 "jaw_width_neg" 130 }
 		gene_mouth_corner_depth={ "mouth_corner_depth_neg" 162 "mouth_corner_depth_neg" 162 }
 		gene_mouth_corner_height={ "mouth_corner_height_pos" 142 "mouth_corner_height_pos" 142 }
 		gene_mouth_forward={ "mouth_forward_neg" 141 "mouth_forward_neg" 141 }
 		gene_mouth_height={ "mouth_height_neg" 136 "mouth_height_neg" 136 }
 		gene_mouth_width={ "mouth_width_neg" 50 "mouth_width_neg" 50 }
 		gene_mouth_upper_lip_size={ "mouth_upper_lip_size_pos" 116 "mouth_upper_lip_size_pos" 116 }
 		gene_mouth_lower_lip_size={ "mouth_lower_lip_size_pos" 116 "mouth_lower_lip_size_pos" 116 }
 		gene_mouth_open={ "mouth_open_neg" 39 "mouth_open_neg" 39 }
 		gene_neck_length={ "neck_length_pos" 133 "neck_length_pos" 133 }
 		gene_neck_width={ "neck_width_pos" 84 "neck_width_pos" 84 }
 		gene_bs_cheek_forward={ "cheek_forward_neg" 125 "cheek_forward_neg" 125 }
 		gene_bs_cheek_height={ "cheek_height_neg" 7 "cheek_height_neg" 7 }
 		gene_bs_cheek_width={ "cheek_width_pos" 80 "cheek_width_pos" 80 }
 		gene_bs_ear_angle={ "ear_angle_neg" 51 "ear_angle_neg" 51 }
 		gene_bs_ear_inner_shape={ "ear_inner_shape_pos" 177 "ear_inner_shape_pos" 177 }
 		gene_bs_ear_bend={ "ear_both_bend_pos" 51 "ear_both_bend_pos" 51 }
 		gene_bs_ear_outward={ "ear_outward_pos" 70 "ear_outward_pos" 70 }
 		gene_bs_ear_size={ "ear_size_pos" 33 "ear_size_pos" 33 }
 		gene_bs_eye_corner_depth={ "eye_corner_depth_pos" 17 "eye_corner_depth_pos" 17 }
 		gene_bs_eye_fold_shape={ "eye_fold_shape_neg" 40 "eye_fold_shape_neg" 40 }
 		gene_bs_eye_size={ "eye_size_neg" 154 "eye_size_neg" 154 }
 		gene_bs_eye_upper_lid_size={ "eye_upper_lid_size_neg" 23 "eye_upper_lid_size_neg" 23 }
 		gene_bs_forehead_brow_curve={ "forehead_brow_curve_neg" 252 "forehead_brow_curve_neg" 252 }
 		gene_bs_forehead_brow_forward={ "forehead_brow_forward_neg" 155 "forehead_brow_forward_neg" 155 }
 		gene_bs_forehead_brow_inner_height={ "forehead_brow_inner_height_pos" 31 "forehead_brow_inner_height_pos" 31 }
 		gene_bs_forehead_brow_outer_height={ "forehead_brow_outer_height_neg" 9 "forehead_brow_outer_height_neg" 9 }
 		gene_bs_forehead_brow_width={ "forehead_brow_width_pos" 120 "forehead_brow_width_pos" 120 }
 		gene_bs_jaw_def={ "jaw_def_pos" 50 "jaw_def_pos" 50 }
 		gene_bs_mouth_lower_lip_def={ "mouth_lower_lip_def_pos" 187 "mouth_lower_lip_def_pos" 187 }
 		gene_bs_mouth_lower_lip_full={ "mouth_lower_lip_full_pos" 27 "mouth_lower_lip_full_pos" 27 }
 		gene_bs_mouth_lower_lip_pad={ "mouth_lower_lip_pad_pos" 20 "mouth_lower_lip_pad_pos" 20 }
 		gene_bs_mouth_lower_lip_width={ "mouth_lower_lip_width_neg" 28 "mouth_lower_lip_width_neg" 28 }
 		gene_bs_mouth_philtrum_def={ "mouth_philtrum_def_pos" 85 "mouth_philtrum_def_pos" 85 }
 		gene_bs_mouth_philtrum_shape={ "mouth_philtrum_shape_neg" 39 "mouth_philtrum_shape_neg" 39 }
 		gene_bs_mouth_philtrum_width={ "mouth_philtrum_width_pos" 2 "mouth_philtrum_width_pos" 2 }
 		gene_bs_mouth_upper_lip_def={ "mouth_upper_lip_def_pos" 12 "mouth_upper_lip_def_pos" 12 }
 		gene_bs_mouth_upper_lip_full={ "mouth_upper_lip_full_pos" 4 "mouth_upper_lip_full_pos" 4 }
 		gene_bs_mouth_upper_lip_profile={ "mouth_upper_lip_profile_neg" 39 "mouth_upper_lip_profile_neg" 39 }
 		gene_bs_mouth_upper_lip_width={ "mouth_upper_lip_width_pos" 36 "mouth_upper_lip_width_pos" 36 }
 		gene_bs_nose_forward={ "nose_forward_pos" 6 "nose_forward_pos" 6 }
 		gene_bs_nose_height={ "nose_height_neg" 15 "nose_height_neg" 15 }
 		gene_bs_nose_length={ "nose_length_pos" 0 "nose_length_neg" 0 }
 		gene_bs_nose_nostril_height={ "nose_nostril_height_neg" 24 "nose_nostril_height_neg" 24 }
 		gene_bs_nose_nostril_width={ "nose_nostril_width_pos" 14 "nose_nostril_width_pos" 14 }
 		gene_bs_nose_profile={ "nose_profile_neg" 136 "nose_profile_neg" 136 }
 		gene_bs_nose_ridge_angle={ "nose_ridge_angle_pos" 33 "nose_ridge_angle_pos" 33 }
 		gene_bs_nose_ridge_width={ "nose_ridge_width_neg" 134 "nose_ridge_width_neg" 134 }
 		gene_bs_nose_size={ "nose_size_pos" 139 "nose_size_pos" 139 }
 		gene_bs_nose_tip_angle={ "nose_tip_angle_neg" 53 "nose_tip_angle_neg" 53 }
 		gene_bs_nose_tip_forward={ "nose_tip_forward_neg" 134 "nose_tip_forward_neg" 134 }
 		gene_bs_nose_tip_width={ "nose_tip_width_neg" 209 "nose_tip_width_neg" 209 }
 		face_detail_cheek_def={ "cheek_def_02" 10 "cheek_def_02" 10 }
 		face_detail_cheek_fat={ "cheek_fat_01_pos" 10 "cheek_fat_01_pos" 10 }
 		face_detail_chin_cleft={ "chin_dimple" 14 "chin_dimple" 14 }
 		face_detail_chin_def={ "chin_def" 14 "chin_def" 14 }
 		face_detail_eye_lower_lid_def={ "eye_lower_lid_def" 219 "eye_lower_lid_def" 219 }
 		face_detail_eye_socket={ "eye_socket_01" 3 "eye_socket_01" 3 }
 		face_detail_nasolabial={ "nasolabial_04" 56 "nasolabial_04" 56 }
 		face_detail_nose_ridge_def={ "nose_ridge_def_neg" 100 "nose_ridge_def_neg" 100 }
 		face_detail_nose_tip_def={ "nose_tip_def" 64 "nose_tip_def" 64 }
 		face_detail_temple_def={ "temple_def" 62 "temple_def" 62 }
 		expression_brow_wrinkles={ "brow_wrinkles_04" 35 "brow_wrinkles_04" 35 }
 		expression_eye_wrinkles={ "eye_wrinkles_03" 255 "eye_wrinkles_03" 154 }
 		expression_forehead_wrinkles={ "forehead_wrinkles_03" 118 "forehead_wrinkles_03" 118 }
 		expression_other={ "cheek_wrinkles_both_01" 116 "cheek_wrinkles_both_01" 116 }
 		complexion={ "complexion_1" 77 "complexion_1" 77 }
 		gene_height={ "normal_height" 131 "normal_height" 131 }
 		gene_bs_body_type={ "body_fat_head_fat_low" 118 "body_fat_head_fat_low" 134 }
 		gene_bs_body_shape={ "body_shape_average" 0 "body_shape_hourglass_half" 0 }
 		gene_bs_bust={ "bust_clothes" 53 "bust_shape_1_half" 84 }
 		gene_age={ "old_4" 125 "old_4" 125 }
 		gene_eyebrows_shape={ "avg_spacing_low_thickness" 189 "avg_spacing_low_thickness" 189 }
 		gene_eyebrows_fullness={ "layer_2_low_thickness" 250 "layer_2_avg_thickness" 250 }
 		gene_body_hair={ "body_hair_sparse_low_stubble" 2 "body_hair_sparse_low_stubble" 2 }
 		gene_hair_type={ "hair_straight_thin_beard" 117 "hair_straight_thin_beard" 117 }
 		gene_baldness={ "no_baldness" 127 "no_baldness" 127 }
 		eye_accessory={ "normal_eyes" 255 "normal_eyes" 255 }
 		teeth_accessory={ "normal_teeth" 0 "normal_teeth" 0 }
 		eyelashes_accessory={ "asian_eyelashes" 255 "asian_eyelashes" 255 }
 		face_detail_eye_upper_lid_def={ "default_upper_lid_def" 127 "default_upper_lid_def" 127 }
 		face_detail_monolid={ "monolid" 198 "monolid" 198 }
 		gene_eye_size={ "eye_size" 137 "eye_size" 137 }
 		gene_eye_shut_top={ "eye_shut_top" 139 "eye_shut_top" 139 }
 		gene_eye_shut_bottom={ "eye_shut_bottom" 124 "eye_shut_bottom" 124 }
 		gene_bs_eye_height_inside={ "eye_height_inside_neg" 66 "eye_height_inside_neg" 66 }
 		gene_bs_eye_height_outisde={ "eye_height_outisde_neg" 70 "eye_height_outisde_neg" 70 }
 		gene_bs_ear_lobe={ "ear_lobe_fused" 230 "ear_lobe_fused" 230 }
 		gene_bs_nose_central_width={ "nose_central_width_pos" 12 "nose_central_width_pos" 12 }
 		gene_bs_nose_septum_width={ "nose_septum_width_pos" 32 "nose_septum_width_pos" 32 }
 		gene_forehead_inner_brow_width={ "vanilla_inner_brow_width" 127 "vanilla_inner_brow_width" 127 }
 		gene_bs_mouth_lower_lip_profile={ "lower_lip_profile" 61 "lower_lip_profile" 61 }
 		gene_bs_eye_outer_width={ "vanilla_eye_outer_width" 127 "vanilla_eye_outer_width" 127 }
 		gene_bs_head_asymmetry_1={ "vanilla_head_asymmetry_1" 127 "vanilla_head_asymmetry_1" 127 }
 		gene_bs_eye_fold_2={ "vanilla_eye_fold_2" 127 "vanilla_eye_fold_2" 127 }
 		gene_bs_mouth_center_curve={ "vanilla_mouth_center_curve" 127 "vanilla_mouth_center_curve" 127 }
 		gene_bs_eyebrow_straight={ "vanilla_eyebrow_straight" 127 "vanilla_eyebrow_straight" 127 }
 		gene_bs_head_round_shape={ "vanilla_head_round_shape" 127 "vanilla_head_round_shape" 127 }
 		gene_bs_nose_septum_height={ "vanilla_nose_septum_height" 127 "vanilla_nose_septum_height" 127 }
 		gene_bs_head_lower_height={ "vanilla_head_lower_height" 127 "vanilla_head_lower_height" 127 }
 		gene_bs_nose_flared_nostril={ "vanilla_nose_flared_nostril" 127 "vanilla_nose_flared_nostril" 127 }
 		gene_bs_mouth_upper_lip_forward={ "vanilla_upper_lip_forward" 127 "vanilla_upper_lip_forward" 127 }
 		gene_bs_mouth_lower_lip_forward={ "vanilla_lower_lip_forward" 127 "vanilla_lower_lip_forward" 127 }
 		gene_bs_nose_swollen={ "vanilla_nose_swollen" 127 "vanilla_nose_swollen" 127 }
 		gene_bs_ears_fantasy={ "vanilla_ears_fantasy" 127 "vanilla_ears_fantasy" 127 }
 		gene_bs_mouth_glamour_lips={ "vanilla_mouth_glamour_lips" 127 "vanilla_mouth_glamour_lips" 127 }
 		pose={ "" 255 "" 0 }
 		hairstyles={ "japanese_hairstyles_fp2" 143 "all_hairstyles" 0 }
 		legwear={ "japanese_war_legwear" 143 "all_legwear" 0 }
 		gene_shrink_body={ "shrink_all" 255 "" 0 }
 		gene_bs_additive_headgears={ "additive_headgears" 255 "" 0 }
 		gene_bs_cloak_offset={ "cloak_offset" 255 "" 0 }
 		gene_bs_long_beard={ "long_beard" 255 "" 0 }
 		clothes={ "japanese_war_nobility_clothes" 177 "most_clothes" 0 }
 		headgear={ "shogunate_suieikan" 15 "no_headgear" 0 }
 		beards={ "scripted_character_beards_shogunate_01" 153 "no_beard" 0 }
 		shogunate_special_belt_accessory={ "katana_01" 255 "katana_01" 0 }
 		gene_balding_hair_effect={ "baldness_stage_1" 255 "no_baldness" 0 }
 }
	entity={ 979141817 979141817 }
	tags={ {
			hash=1989892411
			invert=no
		}
 {
			hash=2125534279
			invert=no
		}
 {
			hash=3941715396
			invert=no
		}
 {
			hash=681013727
			invert=no
		}
 {
			hash=3672171019
			invert=no
		}
 {
			hash=3500802322
			invert=no
		}
 {
			hash=4196965543
			invert=no
		}
 {
			hash=2670398845
			invert=no
		}
 {
			hash=1500963499
			invert=no
		}
 {
			hash=1563190100
			invert=no
		}
 }
}

