# Auto generated file, do not edit manually. Created using console command dump_bookmark_portraits
# History database id:10066201
bm_motonari1523_aso_koretoyo={
	type=male
	id=22270
	age=0.300000
	genes={ 		hair_color={ 7 234 7 233 }
 		skin_color={ 228 65 203 60 }
 		eye_color={ 23 203 12 242 }
 		gene_chin_forward={ "chin_forward_pos" 131 "chin_forward_pos" 135 }
 		gene_chin_height={ "chin_height_neg" 116 "chin_height_neg" 99 }
 		gene_chin_width={ "chin_width_neg" 131 "chin_width_neg" 134 }
 		gene_eye_angle={ "eye_angle_neg" 143 "eye_angle_neg" 135 }
 		gene_eye_depth={ "eye_depth_pos" 178 "eye_depth_pos" 168 }
 		gene_eye_height={ "eye_height_pos" 144 "eye_height_pos" 138 }
 		gene_eye_distance={ "eye_distance_pos" 100 "eye_distance_pos" 93 }
 		gene_eye_shut={ "eye_shut_neg" 163 "eye_shut_pos" 146 }
 		gene_forehead_angle={ "forehead_angle_neg" 132 "forehead_angle_neg" 146 }
 		gene_forehead_brow_height={ "forehead_brow_height_neg" 154 "forehead_brow_height_neg" 177 }
 		gene_forehead_roundness={ "forehead_roundness_neg" 121 "forehead_roundness_neg" 100 }
 		gene_forehead_width={ "forehead_width_neg" 120 "forehead_width_neg" 111 }
 		gene_forehead_height={ "forehead_height_neg" 121 "forehead_height_neg" 120 }
 		gene_head_height={ "head_height_pos" 152 "head_height_pos" 134 }
 		gene_head_width={ "head_width_pos" 134 "head_width_pos" 127 }
 		gene_head_profile={ "head_profile_pos" 53 "head_profile_pos" 51 }
 		gene_head_top_height={ "head_top_height_pos" 123 "head_top_height_pos" 149 }
 		gene_head_top_width={ "head_top_width_neg" 164 "head_top_width_neg" 96 }
 		gene_jaw_angle={ "jaw_angle_pos" 178 "jaw_angle_pos" 157 }
 		gene_jaw_forward={ "jaw_forward_pos" 102 "jaw_forward_pos" 134 }
 		gene_jaw_height={ "jaw_height_pos" 106 "jaw_height_pos" 92 }
 		gene_jaw_width={ "jaw_width_neg" 134 "jaw_width_neg" 131 }
 		gene_mouth_corner_depth={ "mouth_corner_depth_neg" 169 "mouth_corner_depth_neg" 160 }
 		gene_mouth_corner_height={ "mouth_corner_height_pos" 140 "mouth_corner_height_pos" 137 }
 		gene_mouth_forward={ "mouth_forward_neg" 152 "mouth_forward_neg" 131 }
 		gene_mouth_height={ "mouth_height_neg" 136 "mouth_height_neg" 160 }
 		gene_mouth_width={ "mouth_width_neg" 119 "mouth_width_neg" 121 }
 		gene_mouth_upper_lip_size={ "mouth_upper_lip_size_pos" 118 "mouth_upper_lip_size_neg" 72 }
 		gene_mouth_lower_lip_size={ "mouth_lower_lip_size_pos" 123 "mouth_lower_lip_size_pos" 100 }
 		gene_mouth_open={ "mouth_open_neg" 47 "mouth_open_neg" 35 }
 		gene_neck_length={ "neck_length_pos" 137 "neck_length_pos" 137 }
 		gene_neck_width={ "neck_width_pos" 109 "neck_width_pos" 82 }
 		gene_bs_cheek_forward={ "cheek_forward_neg" 106 "cheek_forward_neg" 121 }
 		gene_bs_cheek_height={ "cheek_height_pos" 71 "cheek_height_neg" 5 }
 		gene_bs_cheek_width={ "cheek_width_pos" 174 "cheek_width_pos" 70 }
 		gene_bs_ear_angle={ "ear_angle_neg" 49 "ear_angle_neg" 66 }
 		gene_bs_ear_inner_shape={ "ear_inner_shape_pos" 179 "ear_inner_shape_pos" 22 }
 		gene_bs_ear_bend={ "ear_both_bend_pos" 51 "ear_both_bend_pos" 5 }
 		gene_bs_ear_outward={ "ear_outward_pos" 55 "ear_outward_pos" 71 }
 		gene_bs_ear_size={ "ear_size_pos" 52 "ear_size_pos" 39 }
 		gene_bs_eye_corner_depth={ "eye_corner_depth_pos" 21 "eye_corner_depth_pos" 16 }
 		gene_bs_eye_fold_shape={ "eye_fold_shape_neg" 0 "eye_fold_shape_neg" 58 }
 		gene_bs_eye_size={ "eye_size_neg" 71 "eye_size_neg" 140 }
 		gene_bs_eye_upper_lid_size={ "eye_upper_lid_size_neg" 19 "eye_upper_lid_size_neg" 17 }
 		gene_bs_forehead_brow_curve={ "forehead_brow_curve_neg" 254 "forehead_brow_curve_neg" 23 }
 		gene_bs_forehead_brow_forward={ "forehead_brow_forward_neg" 147 "forehead_brow_forward_neg" 151 }
 		gene_bs_forehead_brow_inner_height={ "forehead_brow_inner_height_pos" 21 "forehead_brow_inner_height_neg" 12 }
 		gene_bs_forehead_brow_outer_height={ "forehead_brow_outer_height_neg" 9 "forehead_brow_outer_height_pos" 27 }
 		gene_bs_forehead_brow_width={ "forehead_brow_width_pos" 118 "forehead_brow_width_pos" 107 }
 		gene_bs_jaw_def={ "jaw_def_pos" 33 "jaw_def_neg" 54 }
 		gene_bs_mouth_lower_lip_def={ "mouth_lower_lip_def_pos" 237 "mouth_lower_lip_def_pos" 234 }
 		gene_bs_mouth_lower_lip_full={ "mouth_lower_lip_full_pos" 27 "mouth_lower_lip_full_pos" 19 }
 		gene_bs_mouth_lower_lip_pad={ "mouth_lower_lip_pad_pos" 1 "mouth_lower_lip_pad_pos" 5 }
 		gene_bs_mouth_lower_lip_width={ "mouth_lower_lip_width_neg" 6 "mouth_lower_lip_width_neg" 18 }
 		gene_bs_mouth_philtrum_def={ "mouth_philtrum_def_pos" 85 "mouth_philtrum_def_pos" 71 }
 		gene_bs_mouth_philtrum_shape={ "mouth_philtrum_shape_neg" 56 "mouth_philtrum_shape_neg" 37 }
 		gene_bs_mouth_philtrum_width={ "mouth_philtrum_width_pos" 40 "mouth_philtrum_width_pos" 9 }
 		gene_bs_mouth_upper_lip_def={ "mouth_upper_lip_def_pos" 1 "mouth_upper_lip_def_pos" 131 }
 		gene_bs_mouth_upper_lip_full={ "mouth_upper_lip_full_pos" 10 "mouth_upper_lip_full_pos" 7 }
 		gene_bs_mouth_upper_lip_profile={ "mouth_upper_lip_profile_neg" 33 "mouth_upper_lip_profile_neg" 19 }
 		gene_bs_mouth_upper_lip_width={ "mouth_upper_lip_width_pos" 48 "mouth_upper_lip_width_pos" 56 }
 		gene_bs_nose_forward={ "nose_forward_neg" 42 "nose_forward_pos" 9 }
 		gene_bs_nose_height={ "nose_height_neg" 51 "nose_height_neg" 8 }
 		gene_bs_nose_length={ "nose_length_neg" 101 "nose_length_pos" 8 }
 		gene_bs_nose_nostril_height={ "nose_nostril_height_neg" 32 "nose_nostril_height_neg" 77 }
 		gene_bs_nose_nostril_width={ "nose_nostril_width_pos" 3 "nose_nostril_width_pos" 54 }
 		gene_bs_nose_profile={ "nose_profile_neg" 11 "nose_profile_neg" 11 }
 		gene_bs_nose_ridge_angle={ "nose_ridge_angle_neg" 11 "nose_ridge_angle_pos" 64 }
 		gene_bs_nose_ridge_width={ "nose_ridge_width_neg" 131 "nose_ridge_width_neg" 131 }
 		gene_bs_nose_size={ "nose_size_neg" 241 "nose_size_pos" 121 }
 		gene_bs_nose_tip_angle={ "nose_tip_angle_pos" 25 "nose_tip_angle_neg" 61 }
 		gene_bs_nose_tip_forward={ "nose_tip_forward_neg" 165 "nose_tip_forward_neg" 171 }
 		gene_bs_nose_tip_width={ "nose_tip_width_pos" 31 "nose_tip_width_neg" 228 }
 		face_detail_cheek_def={ "cheek_def_01" 3 "cheek_def_01" 28 }
 		face_detail_cheek_fat={ "cheek_fat_03_pos" 67 "cheek_fat_01_pos" 27 }
 		face_detail_chin_cleft={ "chin_cleft" 23 "chin_cleft" 7 }
 		face_detail_chin_def={ "chin_def" 78 "chin_def" 12 }
 		face_detail_eye_lower_lid_def={ "eye_lower_lid_def" 202 "eye_lower_lid_def" 141 }
 		face_detail_eye_socket={ "eye_socket_01" 127 "eye_socket_01" 67 }
 		face_detail_nasolabial={ "nasolabial_03" 44 "nasolabial_02" 46 }
 		face_detail_nose_ridge_def={ "nose_ridge_def_pos" 42 "nose_ridge_def_neg" 128 }
 		face_detail_nose_tip_def={ "nose_tip_def" 42 "nose_tip_def" 15 }
 		face_detail_temple_def={ "temple_def" 16 "temple_def" 106 }
 		expression_brow_wrinkles={ "brow_wrinkles_04" 123 "brow_wrinkles_02" 28 }
 		expression_eye_wrinkles={ "eye_wrinkles_03" 205 "eye_wrinkles_01" 130 }
 		expression_forehead_wrinkles={ "forehead_wrinkles_01" 175 "forehead_wrinkles_01" 73 }
 		expression_other={ "nose_wrinkles_01" 73 "cheek_wrinkles_both_01" 9 }
 		complexion={ "complexion_1" 232 "complexion_1" 191 }
 		gene_height={ "normal_height" 112 "normal_height" 133 }
 		gene_bs_body_type={ "body_fat_head_fat_full" 137 "body_fat_head_fat_medium" 128 }
 		gene_bs_body_shape={ "body_shape_triangle_full" 183 "body_shape_rectangle_full" 0 }
 		gene_bs_bust={ "bust_clothes" 18 "bust_shape_2_full" 201 }
 		gene_age={ "old_1" 190 "old_4" 135 }
 		gene_eyebrows_shape={ "avg_spacing_low_thickness" 220 "avg_spacing_avg_thickness" 150 }
 		gene_eyebrows_fullness={ "layer_2_avg_thickness" 207 "layer_2_avg_thickness" 216 }
 		gene_body_hair={ "body_hair_sparse_low_stubble" 15 "body_hair_sparse_low_stubble" 140 }
 		gene_hair_type={ "hair_straight_thin_beard" 134 "hair_straight_thin_beard" 104 }
 		gene_baldness={ "male_pattern_baldness" 168 "male_pattern_baldness" 102 }
 		eye_accessory={ "normal_eyes" 255 "normal_eyes" 255 }
 		teeth_accessory={ "normal_teeth" 0 "normal_teeth" 0 }
 		eyelashes_accessory={ "asian_eyelashes" 255 "asian_eyelashes" 255 }
 		face_detail_eye_upper_lid_def={ "default_upper_lid_def" 127 "default_upper_lid_def" 127 }
 		face_detail_monolid={ "monolid" 210 "monolid" 178 }
 		gene_eye_size={ "eye_size" 122 "eye_size" 129 }
 		gene_eye_shut_top={ "eye_shut_top" 136 "eye_shut_top" 130 }
 		gene_eye_shut_bottom={ "eye_shut_bottom" 108 "eye_shut_bottom" 124 }
 		gene_bs_eye_height_inside={ "eye_height_inside_pos" 42 "eye_height_inside_pos" 50 }
 		gene_bs_eye_height_outisde={ "eye_height_outisde_neg" 105 "eye_height_outisde_pos" 75 }
 		gene_bs_ear_lobe={ "ear_lobe_fused" 199 "ear_lobe_fused" 210 }
 		gene_bs_nose_central_width={ "nose_central_width_neg" 53 "nose_central_width_neg" 13 }
 		gene_bs_nose_septum_width={ "nose_septum_width_pos" 93 "nose_septum_width_pos" 97 }
 		gene_forehead_inner_brow_width={ "vanilla_inner_brow_width" 127 "vanilla_inner_brow_width" 127 }
 		gene_bs_mouth_lower_lip_profile={ "lower_lip_profile" 36 "vanilla_lower_lip_profile" 127 }
 		gene_bs_eye_outer_width={ "vanilla_eye_outer_width" 127 "vanilla_eye_outer_width" 127 }
 		gene_bs_head_asymmetry_1={ "vanilla_head_asymmetry_1" 127 "vanilla_head_asymmetry_1" 127 }
 		gene_bs_eye_fold_2={ "vanilla_eye_fold_2" 127 "vanilla_eye_fold_2" 127 }
 		gene_bs_mouth_center_curve={ "vanilla_mouth_center_curve" 127 "vanilla_mouth_center_curve" 127 }
 		gene_bs_eyebrow_straight={ "vanilla_eyebrow_straight" 127 "vanilla_eyebrow_straight" 127 }
 		gene_bs_head_round_shape={ "vanilla_head_round_shape" 127 "vanilla_head_round_shape" 127 }
 		gene_bs_nose_septum_height={ "vanilla_nose_septum_height" 127 "vanilla_nose_septum_height" 127 }
 		gene_bs_head_lower_height={ "vanilla_head_lower_height" 127 "vanilla_head_lower_height" 127 }
 		gene_bs_nose_flared_nostril={ "vanilla_nose_flared_nostril" 127 "vanilla_nose_flared_nostril" 127 }
 		gene_bs_mouth_upper_lip_forward={ "vanilla_upper_lip_forward" 127 "vanilla_upper_lip_forward" 127 }
 		gene_bs_mouth_lower_lip_forward={ "vanilla_lower_lip_forward" 127 "vanilla_lower_lip_forward" 127 }
 		gene_bs_nose_swollen={ "vanilla_nose_swollen" 127 "vanilla_nose_swollen" 127 }
 		gene_bs_ears_fantasy={ "vanilla_ears_fantasy" 127 "vanilla_ears_fantasy" 127 }
 		gene_bs_mouth_glamour_lips={ "vanilla_mouth_glamour_lips" 127 "vanilla_mouth_glamour_lips" 127 }
 		pose={ "" 255 "" 0 }
 		beards={ "japanese_beards" 63 "no_beard" 0 }
 		hairstyles={ "japanese_hairstyles_fp2" 198 "all_hairstyles" 0 }
 		legwear={ "japanese_tabi" 63 "all_legwear" 0 }
 		gene_shrink_body={ "shrink_all" 255 "" 0 }
 		gene_bs_additive_headgears={ "additive_headgears" 255 "" 0 }
 		gene_bs_cloak_offset={ "cloak_offset" 255 "" 0 }
 		gene_bs_long_beard={ "long_beard" 255 "" 0 }
 		clothes={ "japanese_low_nobility_clothes" 62 "most_clothes" 0 }
 		shogunate_special_belt_accessory={ "katana_01" 219 "katana_01" 0 }
 		headgear={ "no_headgear" 147 "no_headgear" 0 }
 		gene_balding_hair_effect={ "baldness_stage_2" 255 "no_baldness" 0 }
 }
	entity={ 979141817 979141817 }
	tags={ {
			hash=4196965543
			invert=no
		}
 {
			hash=2125534279
			invert=no
		}
 {
			hash=2670398845
			invert=no
		}
 {
			hash=2951898311
			invert=no
		}
 {
			hash=681013727
			invert=no
		}
 {
			hash=1563190100
			invert=no
		}
 }
}

