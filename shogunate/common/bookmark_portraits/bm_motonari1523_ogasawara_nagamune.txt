# Auto generated file, do not edit manually. Created using console command dump_bookmark_portraits
# History database id:10021400
bm_motonari1523_ogasawara_nagamune={
	type=male
	id=22262
	age=0.310000
	genes={ 		hair_color={ 12 245 14 245 }
 		skin_color={ 158 81 155 83 }
 		eye_color={ 7 253 14 247 }
 		gene_chin_forward={ "chin_forward_pos" 137 "chin_forward_pos" 132 }
 		gene_chin_height={ "chin_height_neg" 116 "chin_height_neg" 106 }
 		gene_chin_width={ "chin_width_neg" 130 "chin_width_neg" 142 }
 		gene_eye_angle={ "eye_angle_neg" 118 "eye_angle_neg" 138 }
 		gene_eye_depth={ "eye_depth_pos" 165 "eye_depth_pos" 173 }
 		gene_eye_height={ "eye_height_pos" 151 "eye_height_pos" 129 }
 		gene_eye_distance={ "eye_distance_pos" 90 "eye_distance_pos" 136 }
 		gene_eye_shut={ "eye_shut_neg" 165 "eye_shut_pos" 138 }
 		gene_forehead_angle={ "forehead_angle_neg" 130 "forehead_angle_neg" 131 }
 		gene_forehead_brow_height={ "forehead_brow_height_neg" 169 "forehead_brow_height_neg" 148 }
 		gene_forehead_roundness={ "forehead_roundness_neg" 182 "forehead_roundness_neg" 224 }
 		gene_forehead_width={ "forehead_width_neg" 112 "forehead_width_neg" 128 }
 		gene_forehead_height={ "forehead_height_neg" 122 "forehead_height_neg" 140 }
 		gene_head_height={ "head_height_pos" 161 "head_height_pos" 120 }
 		gene_head_width={ "head_width_pos" 138 "head_width_pos" 133 }
 		gene_head_profile={ "head_profile_pos" 80 "head_profile_pos" 55 }
 		gene_head_top_height={ "head_top_height_pos" 160 "head_top_height_pos" 149 }
 		gene_head_top_width={ "head_top_width_neg" 108 "head_top_width_neg" 151 }
 		gene_jaw_angle={ "jaw_angle_pos" 144 "jaw_angle_pos" 168 }
 		gene_jaw_forward={ "jaw_forward_pos" 111 "jaw_forward_pos" 107 }
 		gene_jaw_height={ "jaw_height_pos" 97 "jaw_height_pos" 125 }
 		gene_jaw_width={ "jaw_width_neg" 125 "jaw_width_neg" 132 }
 		gene_mouth_corner_depth={ "mouth_corner_depth_neg" 161 "mouth_corner_depth_neg" 165 }
 		gene_mouth_corner_height={ "mouth_corner_height_pos" 98 "mouth_corner_height_pos" 113 }
 		gene_mouth_forward={ "mouth_forward_neg" 146 "mouth_forward_neg" 131 }
 		gene_mouth_height={ "mouth_height_neg" 150 "mouth_height_neg" 145 }
 		gene_mouth_width={ "mouth_width_neg" 110 "mouth_width_neg" 82 }
 		gene_mouth_upper_lip_size={ "mouth_upper_lip_size_neg" 110 "mouth_upper_lip_size_neg" 61 }
 		gene_mouth_lower_lip_size={ "mouth_lower_lip_size_pos" 111 "mouth_lower_lip_size_pos" 117 }
 		gene_mouth_open={ "mouth_open_neg" 35 "mouth_open_neg" 43 }
 		gene_neck_length={ "neck_length_pos" 133 "neck_length_pos" 139 }
 		gene_neck_width={ "neck_width_pos" 106 "neck_width_pos" 86 }
 		gene_bs_cheek_forward={ "cheek_forward_neg" 105 "cheek_forward_neg" 123 }
 		gene_bs_cheek_height={ "cheek_height_neg" 9 "cheek_height_pos" 109 }
 		gene_bs_cheek_width={ "cheek_width_pos" 112 "cheek_width_pos" 167 }
 		gene_bs_ear_angle={ "ear_angle_pos" 60 "ear_angle_pos" 49 }
 		gene_bs_ear_inner_shape={ "ear_inner_shape_pos" 13 "ear_inner_shape_pos" 20 }
 		gene_bs_ear_bend={ "ear_both_bend_pos" 17 "ear_both_bend_pos" 52 }
 		gene_bs_ear_outward={ "ear_outward_pos" 10 "ear_outward_pos" 35 }
 		gene_bs_ear_size={ "ear_size_pos" 56 "ear_size_pos" 136 }
 		gene_bs_eye_corner_depth={ "eye_corner_depth_pos" 21 "eye_corner_depth_pos" 6 }
 		gene_bs_eye_fold_shape={ "eye_fold_shape_neg" 0 "eye_fold_shape_neg" 0 }
 		gene_bs_eye_size={ "eye_size_neg" 23 "eye_size_neg" 154 }
 		gene_bs_eye_upper_lid_size={ "eye_upper_lid_size_neg" 5 "eye_upper_lid_size_neg" 27 }
 		gene_bs_forehead_brow_curve={ "forehead_brow_curve_neg" 68 "forehead_brow_curve_neg" 252 }
 		gene_bs_forehead_brow_forward={ "forehead_brow_forward_neg" 32 "forehead_brow_forward_pos" 3 }
 		gene_bs_forehead_brow_inner_height={ "forehead_brow_inner_height_neg" 30 "forehead_brow_inner_height_pos" 33 }
 		gene_bs_forehead_brow_outer_height={ "forehead_brow_outer_height_neg" 60 "forehead_brow_outer_height_neg" 12 }
 		gene_bs_forehead_brow_width={ "forehead_brow_width_pos" 118 "forehead_brow_width_neg" 99 }
 		gene_bs_jaw_def={ "jaw_def_pos" 44 "jaw_def_neg" 28 }
 		gene_bs_mouth_lower_lip_def={ "mouth_lower_lip_def_pos" 37 "mouth_lower_lip_def_pos" 35 }
 		gene_bs_mouth_lower_lip_full={ "mouth_lower_lip_full_pos" 73 "mouth_lower_lip_full_pos" 10 }
 		gene_bs_mouth_lower_lip_pad={ "mouth_lower_lip_pad_pos" 25 "mouth_lower_lip_pad_pos" 7 }
 		gene_bs_mouth_lower_lip_width={ "mouth_lower_lip_width_neg" 16 "mouth_lower_lip_width_neg" 14 }
 		gene_bs_mouth_philtrum_def={ "mouth_philtrum_def_pos" 86 "mouth_philtrum_def_pos" 71 }
 		gene_bs_mouth_philtrum_shape={ "mouth_philtrum_shape_neg" 38 "mouth_philtrum_shape_neg" 32 }
 		gene_bs_mouth_philtrum_width={ "mouth_philtrum_width_pos" 50 "mouth_philtrum_width_pos" 11 }
 		gene_bs_mouth_upper_lip_def={ "mouth_upper_lip_def_pos" 8 "mouth_upper_lip_def_pos" 123 }
 		gene_bs_mouth_upper_lip_full={ "mouth_upper_lip_full_pos" 82 "mouth_upper_lip_full_pos" 13 }
 		gene_bs_mouth_upper_lip_profile={ "mouth_upper_lip_profile_neg" 32 "mouth_upper_lip_profile_neg" 43 }
 		gene_bs_mouth_upper_lip_width={ "mouth_upper_lip_width_pos" 40 "mouth_upper_lip_width_pos" 51 }
 		gene_bs_nose_forward={ "nose_forward_pos" 54 "nose_forward_pos" 10 }
 		gene_bs_nose_height={ "nose_height_neg" 8 "nose_height_neg" 2 }
 		gene_bs_nose_length={ "nose_length_pos" 14 "nose_length_neg" 83 }
 		gene_bs_nose_nostril_height={ "nose_nostril_height_neg" 85 "nose_nostril_height_neg" 57 }
 		gene_bs_nose_nostril_width={ "nose_nostril_width_pos" 25 "nose_nostril_width_pos" 8 }
 		gene_bs_nose_profile={ "nose_profile_neg" 105 "nose_profile_pos" 68 }
 		gene_bs_nose_ridge_angle={ "nose_ridge_angle_pos" 74 "nose_ridge_angle_neg" 17 }
 		gene_bs_nose_ridge_width={ "nose_ridge_width_neg" 4 "nose_ridge_width_neg" 137 }
 		gene_bs_nose_size={ "nose_size_neg" 63 "nose_size_neg" 29 }
 		gene_bs_nose_tip_angle={ "nose_tip_angle_neg" 29 "nose_tip_angle_neg" 57 }
 		gene_bs_nose_tip_forward={ "nose_tip_forward_neg" 123 "nose_tip_forward_neg" 120 }
 		gene_bs_nose_tip_width={ "nose_tip_width_neg" 206 "nose_tip_width_neg" 240 }
 		face_detail_cheek_def={ "cheek_def_01" 41 "cheek_def_02" 66 }
 		face_detail_cheek_fat={ "cheek_fat_02_pos" 92 "cheek_fat_03_pos" 26 }
 		face_detail_chin_cleft={ "chin_dimple" 20 "chin_cleft" 12 }
 		face_detail_chin_def={ "chin_def" 38 "chin_def" 97 }
 		face_detail_eye_lower_lid_def={ "eye_lower_lid_def" 208 "eye_lower_lid_def" 225 }
 		face_detail_eye_socket={ "eye_socket_01" 13 "eye_socket_01" 38 }
 		face_detail_nasolabial={ "nasolabial_01" 37 "nasolabial_04" 100 }
 		face_detail_nose_ridge_def={ "nose_ridge_def_pos" 28 "nose_ridge_def_neg" 176 }
 		face_detail_nose_tip_def={ "nose_tip_def" 90 "nose_tip_def" 20 }
 		face_detail_temple_def={ "temple_def" 79 "temple_def" 146 }
 		expression_brow_wrinkles={ "brow_wrinkles_01" 141 "brow_wrinkles_01" 138 }
 		expression_eye_wrinkles={ "eye_wrinkles_02" 135 "eye_wrinkles_01" 19 }
 		expression_forehead_wrinkles={ "forehead_wrinkles_03" 124 "forehead_wrinkles_01" 93 }
 		expression_other={ "cheek_wrinkles_both_01" 12 "cheek_wrinkles_both_01" 26 }
 		complexion={ "complexion_1" 137 "complexion_1" 199 }
 		gene_height={ "normal_height" 121 "normal_height" 127 }
 		gene_bs_body_type={ "body_fat_head_fat_low" 108 "body_fat_head_fat_low" 126 }
 		gene_bs_body_shape={ "body_shape_triangle_half" 193 "body_shape_triangle_half" 0 }
 		gene_bs_bust={ "bust_clothes" 100 "bust_shape_2_full" 8 }
 		gene_age={ "old_3" 141 "old_1" 3 }
 		gene_eyebrows_shape={ "far_spacing_low_thickness" 173 "avg_spacing_low_thickness" 165 }
 		gene_eyebrows_fullness={ "layer_2_low_thickness" 251 "layer_2_avg_thickness" 192 }
 		gene_body_hair={ "body_hair_sparse_low_stubble" 96 "body_hair_sparse_low_stubble" 16 }
 		gene_hair_type={ "hair_straight_thin_beard" 107 "hair_straight_thin_beard" 122 }
 		gene_baldness={ "male_pattern_baldness" 173 "male_pattern_baldness" 159 }
 		eye_accessory={ "normal_eyes" 255 "normal_eyes" 255 }
 		teeth_accessory={ "normal_teeth" 0 "normal_teeth" 0 }
 		eyelashes_accessory={ "asian_eyelashes" 255 "asian_eyelashes" 255 }
 		face_detail_eye_upper_lid_def={ "default_upper_lid_def" 127 "default_upper_lid_def" 127 }
 		face_detail_monolid={ "monolid" 255 "monolid" 235 }
 		gene_eye_size={ "eye_size" 124 "eye_size" 140 }
 		gene_eye_shut_top={ "eye_shut_top" 144 "eye_shut_top" 139 }
 		gene_eye_shut_bottom={ "eye_shut_bottom" 117 "eye_shut_bottom" 123 }
 		gene_bs_eye_height_inside={ "eye_height_inside_pos" 58 "eye_height_inside_neg" 59 }
 		gene_bs_eye_height_outisde={ "eye_height_outisde_pos" 13 "eye_height_outisde_neg" 21 }
 		gene_bs_ear_lobe={ "ear_lobe_fused" 197 "ear_lobe_fused" 234 }
 		gene_bs_nose_central_width={ "nose_central_width_neg" 62 "nose_central_width_pos" 41 }
 		gene_bs_nose_septum_width={ "nose_septum_width_pos" 13 "nose_septum_width_pos" 104 }
 		gene_forehead_inner_brow_width={ "vanilla_inner_brow_width" 127 "vanilla_inner_brow_width" 127 }
 		gene_bs_mouth_lower_lip_profile={ "vanilla_lower_lip_profile" 127 "lower_lip_profile" 51 }
 		gene_bs_eye_outer_width={ "vanilla_eye_outer_width" 127 "vanilla_eye_outer_width" 127 }
 		gene_bs_head_asymmetry_1={ "vanilla_head_asymmetry_1" 127 "vanilla_head_asymmetry_1" 127 }
 		gene_bs_eye_fold_2={ "vanilla_eye_fold_2" 127 "vanilla_eye_fold_2" 127 }
 		gene_bs_mouth_center_curve={ "vanilla_mouth_center_curve" 127 "vanilla_mouth_center_curve" 127 }
 		gene_bs_eyebrow_straight={ "vanilla_eyebrow_straight" 127 "vanilla_eyebrow_straight" 127 }
 		gene_bs_head_round_shape={ "vanilla_head_round_shape" 127 "vanilla_head_round_shape" 127 }
 		gene_bs_nose_septum_height={ "vanilla_nose_septum_height" 127 "vanilla_nose_septum_height" 127 }
 		gene_bs_head_lower_height={ "vanilla_head_lower_height" 127 "vanilla_head_lower_height" 127 }
 		gene_bs_nose_flared_nostril={ "vanilla_nose_flared_nostril" 127 "vanilla_nose_flared_nostril" 127 }
 		gene_bs_mouth_upper_lip_forward={ "vanilla_upper_lip_forward" 127 "vanilla_upper_lip_forward" 127 }
 		gene_bs_mouth_lower_lip_forward={ "vanilla_lower_lip_forward" 127 "vanilla_lower_lip_forward" 127 }
 		gene_bs_nose_swollen={ "vanilla_nose_swollen" 127 "vanilla_nose_swollen" 127 }
 		gene_bs_ears_fantasy={ "vanilla_ears_fantasy" 127 "vanilla_ears_fantasy" 127 }
 		gene_bs_mouth_glamour_lips={ "vanilla_mouth_glamour_lips" 127 "vanilla_mouth_glamour_lips" 127 }
 		pose={ "" 255 "" 0 }
 		beards={ "japanese_beards" 85 "no_beard" 0 }
 		hairstyles={ "japanese_hairstyles_fp2" 3 "all_hairstyles" 0 }
 		legwear={ "japanese_tabi" 85 "all_legwear" 0 }
 		gene_shrink_body={ "shrink_all" 255 "" 0 }
 		gene_bs_additive_headgears={ "additive_headgears" 255 "" 0 }
 		gene_bs_cloak_offset={ "cloak_offset" 255 "" 0 }
 		gene_bs_long_beard={ "long_beard" 255 "" 0 }
 		clothes={ "japanese_high_nobility_clothes" 206 "most_clothes" 0 }
 		shogunate_special_belt_accessory={ "katana_01" 163 "katana_01" 0 }
 		headgear={ "no_headgear" 144 "no_headgear" 0 }
 		gene_balding_hair_effect={ "baldness_stage_2" 255 "no_baldness" 0 }
 }
	entity={ 3942081117 3942081117 }
	tags={ {
			hash=4196965543
			invert=no
		}
 {
			hash=2125534279
			invert=no
		}
 {
			hash=2670398845
			invert=no
		}
 {
			hash=2951898311
			invert=no
		}
 {
			hash=681013727
			invert=no
		}
 {
			hash=1563190100
			invert=no
		}
 }
}

