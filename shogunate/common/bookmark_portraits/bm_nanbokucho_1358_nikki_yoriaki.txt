# Auto generated file, do not edit manually. Created using console command dump_bookmark_portraits
# History database id:12396020
bm_nanbokucho_1358_nikki_yoriaki={
	type=male
	id=14976
	age=0.590000
	genes={ 		hair_color={ 10 241 14 243 }
 		skin_color={ 182 80 190 78 }
 		eye_color={ 19 249 45 252 }
 		gene_chin_forward={ "chin_forward_pos" 130 "chin_forward_pos" 130 }
 		gene_chin_height={ "chin_height_neg" 126 "chin_height_neg" 117 }
 		gene_chin_width={ "chin_width_neg" 144 "chin_width_neg" 132 }
 		gene_eye_angle={ "eye_angle_neg" 128 "eye_angle_neg" 121 }
 		gene_eye_depth={ "eye_depth_pos" 172 "eye_depth_pos" 167 }
 		gene_eye_height={ "eye_height_pos" 145 "eye_height_pos" 142 }
 		gene_eye_distance={ "eye_distance_pos" 123 "eye_distance_pos" 141 }
 		gene_eye_shut={ "eye_shut_pos" 142 "eye_shut_pos" 127 }
 		gene_forehead_angle={ "forehead_angle_neg" 127 "forehead_angle_neg" 138 }
 		gene_forehead_brow_height={ "forehead_brow_height_neg" 142 "forehead_brow_height_neg" 162 }
 		gene_forehead_roundness={ "forehead_roundness_neg" 135 "forehead_roundness_neg" 102 }
 		gene_forehead_width={ "forehead_width_neg" 147 "forehead_width_neg" 132 }
 		gene_forehead_height={ "forehead_height_neg" 141 "forehead_height_neg" 144 }
 		gene_head_height={ "head_height_pos" 158 "head_height_pos" 138 }
 		gene_head_width={ "head_width_pos" 89 "head_width_pos" 142 }
 		gene_head_profile={ "head_profile_pos" 89 "head_profile_pos" 57 }
 		gene_head_top_height={ "head_top_height_pos" 130 "head_top_height_pos" 174 }
 		gene_head_top_width={ "head_top_width_neg" 160 "head_top_width_neg" 114 }
 		gene_jaw_angle={ "jaw_angle_pos" 92 "jaw_angle_pos" 137 }
 		gene_jaw_forward={ "jaw_forward_neg" 103 "jaw_forward_pos" 126 }
 		gene_jaw_height={ "jaw_height_pos" 122 "jaw_height_pos" 112 }
 		gene_jaw_width={ "jaw_width_neg" 143 "jaw_width_neg" 134 }
 		gene_mouth_corner_depth={ "mouth_corner_depth_neg" 180 "mouth_corner_depth_neg" 168 }
 		gene_mouth_corner_height={ "mouth_corner_height_pos" 91 "mouth_corner_height_pos" 103 }
 		gene_mouth_forward={ "mouth_forward_neg" 148 "mouth_forward_neg" 131 }
 		gene_mouth_height={ "mouth_height_neg" 150 "mouth_height_neg" 157 }
 		gene_mouth_width={ "mouth_width_neg" 93 "mouth_width_neg" 120 }
 		gene_mouth_upper_lip_size={ "mouth_upper_lip_size_neg" 65 "mouth_upper_lip_size_neg" 74 }
 		gene_mouth_lower_lip_size={ "mouth_lower_lip_size_pos" 102 "mouth_lower_lip_size_pos" 124 }
 		gene_mouth_open={ "mouth_open_neg" 35 "mouth_open_neg" 35 }
 		gene_neck_length={ "neck_length_pos" 134 "neck_length_pos" 137 }
 		gene_neck_width={ "neck_width_pos" 79 "neck_width_pos" 86 }
 		gene_bs_cheek_forward={ "cheek_forward_neg" 103 "cheek_forward_neg" 107 }
 		gene_bs_cheek_height={ "cheek_height_pos" 180 "cheek_height_pos" 132 }
 		gene_bs_cheek_width={ "cheek_width_pos" 125 "cheek_width_pos" 53 }
 		gene_bs_ear_angle={ "ear_angle_pos" 30 "ear_angle_pos" 14 }
 		gene_bs_ear_inner_shape={ "ear_inner_shape_pos" 33 "ear_inner_shape_pos" 28 }
 		gene_bs_ear_bend={ "ear_both_bend_pos" 53 "ear_both_bend_pos" 225 }
 		gene_bs_ear_outward={ "ear_outward_pos" 53 "ear_outward_pos" 40 }
 		gene_bs_ear_size={ "ear_size_pos" 37 "ear_size_pos" 15 }
 		gene_bs_eye_corner_depth={ "eye_corner_depth_pos" 2 "eye_corner_depth_neg" 115 }
 		gene_bs_eye_fold_shape={ "eye_fold_shape_neg" 0 "eye_fold_shape_neg" 0 }
 		gene_bs_eye_size={ "eye_size_neg" 26 "eye_size_neg" 11 }
 		gene_bs_eye_upper_lid_size={ "eye_upper_lid_size_neg" 50 "eye_upper_lid_size_neg" 32 }
 		gene_bs_forehead_brow_curve={ "forehead_brow_curve_neg" 35 "forehead_brow_curve_neg" 115 }
 		gene_bs_forehead_brow_forward={ "forehead_brow_forward_neg" 75 "forehead_brow_forward_pos" 10 }
 		gene_bs_forehead_brow_inner_height={ "forehead_brow_inner_height_neg" 27 "forehead_brow_inner_height_pos" 27 }
 		gene_bs_forehead_brow_outer_height={ "forehead_brow_outer_height_neg" 12 "forehead_brow_outer_height_pos" 19 }
 		gene_bs_forehead_brow_width={ "forehead_brow_width_pos" 107 "forehead_brow_width_pos" 123 }
 		gene_bs_jaw_def={ "jaw_def_neg" 46 "jaw_def_neg" 127 }
 		gene_bs_mouth_lower_lip_def={ "mouth_lower_lip_def_pos" 37 "mouth_lower_lip_def_pos" 36 }
 		gene_bs_mouth_lower_lip_full={ "mouth_lower_lip_full_pos" 74 "mouth_lower_lip_full_pos" 75 }
 		gene_bs_mouth_lower_lip_pad={ "mouth_lower_lip_pad_pos" 10 "mouth_lower_lip_pad_pos" 22 }
 		gene_bs_mouth_lower_lip_width={ "mouth_lower_lip_width_neg" 6 "mouth_lower_lip_width_neg" 2 }
 		gene_bs_mouth_philtrum_def={ "mouth_philtrum_def_pos" 93 "mouth_philtrum_def_pos" 101 }
 		gene_bs_mouth_philtrum_shape={ "mouth_philtrum_shape_neg" 37 "mouth_philtrum_shape_neg" 15 }
 		gene_bs_mouth_philtrum_width={ "mouth_philtrum_width_pos" 38 "mouth_philtrum_width_pos" 43 }
 		gene_bs_mouth_upper_lip_def={ "mouth_upper_lip_def_pos" 124 "mouth_upper_lip_def_pos" 124 }
 		gene_bs_mouth_upper_lip_full={ "mouth_upper_lip_full_pos" 55 "mouth_upper_lip_full_pos" 66 }
 		gene_bs_mouth_upper_lip_profile={ "mouth_upper_lip_profile_neg" 32 "mouth_upper_lip_profile_neg" 33 }
 		gene_bs_mouth_upper_lip_width={ "mouth_upper_lip_width_pos" 51 "mouth_upper_lip_width_pos" 50 }
 		gene_bs_nose_forward={ "nose_forward_pos" 53 "nose_forward_pos" 69 }
 		gene_bs_nose_height={ "nose_height_pos" 4 "nose_height_neg" 2 }
 		gene_bs_nose_length={ "nose_length_neg" 101 "nose_length_neg" 64 }
 		gene_bs_nose_nostril_height={ "nose_nostril_height_neg" 73 "nose_nostril_height_neg" 100 }
 		gene_bs_nose_nostril_width={ "nose_nostril_width_pos" 13 "nose_nostril_width_pos" 21 }
 		gene_bs_nose_profile={ "nose_profile_neg" 15 "nose_profile_neg" 17 }
 		gene_bs_nose_ridge_angle={ "nose_ridge_angle_neg" 16 "nose_ridge_angle_neg" 7 }
 		gene_bs_nose_ridge_width={ "nose_ridge_width_pos" 23 "nose_ridge_width_pos" 4 }
 		gene_bs_nose_size={ "nose_size_neg" 21 "nose_size_neg" 18 }
 		gene_bs_nose_tip_angle={ "nose_tip_angle_neg" 24 "nose_tip_angle_neg" 74 }
 		gene_bs_nose_tip_forward={ "nose_tip_forward_neg" 123 "nose_tip_forward_neg" 121 }
 		gene_bs_nose_tip_width={ "nose_tip_width_neg" 182 "nose_tip_width_neg" 157 }
 		face_detail_cheek_def={ "cheek_def_01" 97 "cheek_def_01" 112 }
 		face_detail_cheek_fat={ "cheek_fat_02_pos" 162 "cheek_fat_03_pos" 93 }
 		face_detail_chin_cleft={ "chin_cleft" 23 "chin_dimple" 15 }
 		face_detail_chin_def={ "chin_def" 18 "chin_def" 13 }
 		face_detail_eye_lower_lid_def={ "eye_lower_lid_def" 200 "eye_lower_lid_def" 171 }
 		face_detail_eye_socket={ "eye_socket_01" 40 "eye_socket_01" 169 }
 		face_detail_nasolabial={ "nasolabial_03" 96 "nasolabial_04" 62 }
 		face_detail_nose_ridge_def={ "nose_ridge_def_neg" 252 "nose_ridge_def_neg" 241 }
 		face_detail_nose_tip_def={ "nose_tip_def" 28 "nose_tip_def" 46 }
 		face_detail_temple_def={ "temple_def" 173 "temple_def" 125 }
 		expression_brow_wrinkles={ "brow_wrinkles_02" 61 "brow_wrinkles_04" 138 }
 		expression_eye_wrinkles={ "eye_wrinkles_01" 255 "eye_wrinkles_03" 92 }
 		expression_forehead_wrinkles={ "forehead_wrinkles_03" 135 "forehead_wrinkles_02" 48 }
 		expression_other={ "cheek_wrinkles_both_01" 74 "cheek_wrinkles_both_01" 58 }
 		complexion={ "complexion_1" 58 "complexion_1" 245 }
 		gene_height={ "normal_height" 126 "normal_height" 117 }
 		gene_bs_body_type={ "body_fat_head_fat_medium" 112 "body_fat_head_fat_full" 135 }
 		gene_bs_body_shape={ "body_shape_average" 132 "body_shape_rectangle_half" 0 }
 		gene_bs_bust={ "bust_clothes" 100 "bust_shape_2_half" 50 }
 		gene_age={ "old_1" 234 "old_1" 194 }
 		gene_eyebrows_shape={ "avg_spacing_low_thickness" 145 "far_spacing_avg_thickness" 170 }
 		gene_eyebrows_fullness={ "layer_2_avg_thickness" 253 "layer_2_avg_thickness" 207 }
 		gene_body_hair={ "body_hair_sparse_low_stubble" 127 "body_hair_sparse_low_stubble" 134 }
 		gene_hair_type={ "hair_straight_thin_beard" 181 "hair_straight_thin_beard" 152 }
 		gene_baldness={ "male_pattern_baldness" 65 "male_pattern_baldness" 134 }
 		eye_accessory={ "normal_eyes" 255 "normal_eyes" 255 }
 		teeth_accessory={ "normal_teeth" 0 "normal_teeth" 0 }
 		eyelashes_accessory={ "asian_eyelashes" 255 "asian_eyelashes" 255 }
 		face_detail_eye_upper_lid_def={ "default_upper_lid_def" 127 "default_upper_lid_def" 127 }
 		face_detail_monolid={ "monolid" 231 "monolid" 240 }
 		gene_eye_size={ "eye_size" 131 "eye_size" 130 }
 		gene_eye_shut_top={ "eye_shut_top" 127 "eye_shut_top" 148 }
 		gene_eye_shut_bottom={ "eye_shut_bottom" 114 "eye_shut_bottom" 117 }
 		gene_bs_eye_height_inside={ "eye_height_inside_neg" 48 "eye_height_inside_pos" 15 }
 		gene_bs_eye_height_outisde={ "eye_height_outisde_pos" 70 "eye_height_outisde_pos" 64 }
 		gene_bs_ear_lobe={ "ear_lobe_fused" 249 "ear_lobe_fused" 230 }
 		gene_bs_nose_central_width={ "nose_central_width_neg" 63 "nose_central_width_pos" 2 }
 		gene_bs_nose_septum_width={ "nose_septum_width_pos" 35 "nose_septum_width_pos" 70 }
 		gene_forehead_inner_brow_width={ "vanilla_inner_brow_width" 127 "vanilla_inner_brow_width" 127 }
 		gene_bs_mouth_lower_lip_profile={ "vanilla_lower_lip_profile" 127 "vanilla_lower_lip_profile" 127 }
 		gene_bs_eye_outer_width={ "vanilla_eye_outer_width" 127 "vanilla_eye_outer_width" 127 }
 		gene_bs_head_asymmetry_1={ "vanilla_head_asymmetry_1" 127 "vanilla_head_asymmetry_1" 127 }
 		gene_bs_eye_fold_2={ "vanilla_eye_fold_2" 127 "vanilla_eye_fold_2" 127 }
 		gene_bs_mouth_center_curve={ "vanilla_mouth_center_curve" 127 "vanilla_mouth_center_curve" 127 }
 		gene_bs_eyebrow_straight={ "vanilla_eyebrow_straight" 127 "vanilla_eyebrow_straight" 127 }
 		gene_bs_head_round_shape={ "vanilla_head_round_shape" 127 "vanilla_head_round_shape" 127 }
 		gene_bs_nose_septum_height={ "vanilla_nose_septum_height" 127 "vanilla_nose_septum_height" 127 }
 		gene_bs_head_lower_height={ "vanilla_head_lower_height" 127 "vanilla_head_lower_height" 127 }
 		gene_bs_nose_flared_nostril={ "vanilla_nose_flared_nostril" 127 "vanilla_nose_flared_nostril" 127 }
 		gene_bs_mouth_upper_lip_forward={ "vanilla_upper_lip_forward" 127 "vanilla_upper_lip_forward" 127 }
 		gene_bs_mouth_lower_lip_forward={ "vanilla_lower_lip_forward" 127 "vanilla_lower_lip_forward" 127 }
 		gene_bs_nose_swollen={ "vanilla_nose_swollen" 127 "vanilla_nose_swollen" 127 }
 		gene_bs_ears_fantasy={ "vanilla_ears_fantasy" 127 "vanilla_ears_fantasy" 127 }
 		gene_bs_mouth_glamour_lips={ "vanilla_mouth_glamour_lips" 127 "vanilla_mouth_glamour_lips" 127 }
 		pose={ "" 255 "" 0 }
 		beards={ "japanese_beards" 117 "no_beard" 0 }
 		hairstyles={ "japanese_hairstyles_fp2" 99 "all_hairstyles" 0 }
 		legwear={ "japanese_tabi" 117 "all_legwear" 0 }
 		gene_shrink_body={ "shrink_all" 255 "" 0 }
 		gene_bs_additive_headgears={ "additive_headgears" 255 "" 0 }
 		gene_bs_cloak_offset={ "cloak_offset" 255 "" 0 }
 		gene_bs_long_beard={ "long_beard" 255 "" 0 }
 		clothes={ "japanese_high_nobility_clothes" 116 "most_clothes" 0 }
 		shogunate_special_belt_accessory={ "katana_01" 7 "katana_01" 0 }
 		headgear={ "shogunate_suieikan" 117 "no_headgear" 0 }
 		gene_balding_hair_effect={ "baldness_stage_2" 255 "no_baldness" 0 }
 }
	entity={ 979141817 979141817 }
	tags={ {
			hash=1989892411
			invert=no
		}
 {
			hash=2125534279
			invert=no
		}
 {
			hash=3941715396
			invert=no
		}
 {
			hash=681013727
			invert=no
		}
 {
			hash=3672171019
			invert=no
		}
 {
			hash=2670398845
			invert=no
		}
 {
			hash=1500963499
			invert=no
		}
 {
			hash=2951898311
			invert=no
		}
 {
			hash=1563190100
			invert=no
		}
 }
}

