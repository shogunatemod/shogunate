# Auto generated file, do not edit manually. Created using console command dump_bookmark_portraits
# History database id:12071035
bm_threemountain_1416_date_mochimune={
	type=male
	id=18610
	age=0.230000
	genes={ 		hair_color={ 13 242 9 232 }
 		skin_color={ 156 61 154 54 }
 		eye_color={ 36 248 42 246 }
 		gene_chin_forward={ "chin_forward_pos" 101 "chin_forward_pos" 134 }
 		gene_chin_height={ "chin_height_neg" 109 "chin_height_neg" 115 }
 		gene_chin_width={ "chin_width_neg" 150 "chin_width_neg" 142 }
 		gene_eye_angle={ "eye_angle_neg" 123 "eye_angle_neg" 143 }
 		gene_eye_depth={ "eye_depth_pos" 179 "eye_depth_pos" 164 }
 		gene_eye_height={ "eye_height_pos" 155 "eye_height_pos" 140 }
 		gene_eye_distance={ "eye_distance_pos" 77 "eye_distance_pos" 76 }
 		gene_eye_shut={ "eye_shut_neg" 166 "eye_shut_pos" 162 }
 		gene_forehead_angle={ "forehead_angle_neg" 140 "forehead_angle_neg" 178 }
 		gene_forehead_brow_height={ "forehead_brow_height_neg" 159 "forehead_brow_height_neg" 148 }
 		gene_forehead_roundness={ "forehead_roundness_neg" 128 "forehead_roundness_neg" 99 }
 		gene_forehead_width={ "forehead_width_neg" 108 "forehead_width_neg" 163 }
 		gene_forehead_height={ "forehead_height_neg" 112 "forehead_height_neg" 147 }
 		gene_head_height={ "head_height_pos" 142 "head_height_pos" 129 }
 		gene_head_width={ "head_width_pos" 131 "head_width_pos" 132 }
 		gene_head_profile={ "head_profile_pos" 55 "head_profile_pos" 46 }
 		gene_head_top_height={ "head_top_height_pos" 136 "head_top_height_pos" 138 }
 		gene_head_top_width={ "head_top_width_neg" 147 "head_top_width_neg" 108 }
 		gene_jaw_angle={ "jaw_angle_pos" 157 "jaw_angle_pos" 144 }
 		gene_jaw_forward={ "jaw_forward_pos" 115 "jaw_forward_neg" 113 }
 		gene_jaw_height={ "jaw_height_pos" 117 "jaw_height_pos" 89 }
 		gene_jaw_width={ "jaw_width_neg" 129 "jaw_width_neg" 140 }
 		gene_mouth_corner_depth={ "mouth_corner_depth_neg" 170 "mouth_corner_depth_neg" 178 }
 		gene_mouth_corner_height={ "mouth_corner_height_pos" 118 "mouth_corner_height_pos" 140 }
 		gene_mouth_forward={ "mouth_forward_neg" 141 "mouth_forward_neg" 139 }
 		gene_mouth_height={ "mouth_height_neg" 130 "mouth_height_neg" 166 }
 		gene_mouth_width={ "mouth_width_neg" 86 "mouth_width_neg" 127 }
 		gene_mouth_upper_lip_size={ "mouth_upper_lip_size_pos" 120 "mouth_upper_lip_size_neg" 94 }
 		gene_mouth_lower_lip_size={ "mouth_lower_lip_size_pos" 121 "mouth_lower_lip_size_pos" 118 }
 		gene_mouth_open={ "mouth_open_neg" 37 "mouth_open_neg" 35 }
 		gene_neck_length={ "neck_length_pos" 130 "neck_length_pos" 140 }
 		gene_neck_width={ "neck_width_pos" 95 "neck_width_pos" 80 }
 		gene_bs_cheek_forward={ "cheek_forward_neg" 112 "cheek_forward_neg" 120 }
 		gene_bs_cheek_height={ "cheek_height_neg" 11 "cheek_height_neg" 5 }
 		gene_bs_cheek_width={ "cheek_width_pos" 74 "cheek_width_pos" 122 }
 		gene_bs_ear_angle={ "ear_angle_pos" 11 "ear_angle_neg" 45 }
 		gene_bs_ear_inner_shape={ "ear_inner_shape_pos" 20 "ear_inner_shape_pos" 180 }
 		gene_bs_ear_bend={ "ear_both_bend_pos" 49 "ear_both_bend_pos" 240 }
 		gene_bs_ear_outward={ "ear_outward_pos" 46 "ear_outward_pos" 55 }
 		gene_bs_ear_size={ "ear_size_pos" 35 "ear_size_pos" 112 }
 		gene_bs_eye_corner_depth={ "eye_corner_depth_pos" 11 "eye_corner_depth_neg" 57 }
 		gene_bs_eye_fold_shape={ "eye_fold_shape_neg" 4 "eye_fold_shape_neg" 48 }
 		gene_bs_eye_size={ "eye_size_neg" 137 "eye_size_neg" 91 }
 		gene_bs_eye_upper_lid_size={ "eye_upper_lid_size_neg" 3 "eye_upper_lid_size_neg" 54 }
 		gene_bs_forehead_brow_curve={ "forehead_brow_curve_neg" 252 "forehead_brow_curve_neg" 255 }
 		gene_bs_forehead_brow_forward={ "forehead_brow_forward_neg" 155 "forehead_brow_forward_neg" 97 }
 		gene_bs_forehead_brow_inner_height={ "forehead_brow_inner_height_pos" 102 "forehead_brow_inner_height_pos" 28 }
 		gene_bs_forehead_brow_outer_height={ "forehead_brow_outer_height_neg" 1 "forehead_brow_outer_height_pos" 42 }
 		gene_bs_forehead_brow_width={ "forehead_brow_width_pos" 111 "forehead_brow_width_pos" 120 }
 		gene_bs_jaw_def={ "jaw_def_neg" 24 "jaw_def_pos" 33 }
 		gene_bs_mouth_lower_lip_def={ "mouth_lower_lip_def_pos" 31 "mouth_lower_lip_def_pos" 235 }
 		gene_bs_mouth_lower_lip_full={ "mouth_lower_lip_full_pos" 20 "mouth_lower_lip_full_pos" 76 }
 		gene_bs_mouth_lower_lip_pad={ "mouth_lower_lip_pad_pos" 5 "mouth_lower_lip_pad_pos" 4 }
 		gene_bs_mouth_lower_lip_width={ "mouth_lower_lip_width_neg" 8 "mouth_lower_lip_width_pos" 63 }
 		gene_bs_mouth_philtrum_def={ "mouth_philtrum_def_pos" 112 "mouth_philtrum_def_pos" 92 }
 		gene_bs_mouth_philtrum_shape={ "mouth_philtrum_shape_neg" 32 "mouth_philtrum_shape_neg" 54 }
 		gene_bs_mouth_philtrum_width={ "mouth_philtrum_width_pos" 8 "mouth_philtrum_width_pos" 5 }
 		gene_bs_mouth_upper_lip_def={ "mouth_upper_lip_def_pos" 169 "mouth_upper_lip_def_pos" 8 }
 		gene_bs_mouth_upper_lip_full={ "mouth_upper_lip_full_pos" 95 "mouth_upper_lip_full_pos" 7 }
 		gene_bs_mouth_upper_lip_profile={ "mouth_upper_lip_profile_neg" 24 "mouth_upper_lip_profile_neg" 7 }
 		gene_bs_mouth_upper_lip_width={ "mouth_upper_lip_width_pos" 54 "mouth_upper_lip_width_pos" 36 }
 		gene_bs_nose_forward={ "nose_forward_pos" 43 "nose_forward_pos" 50 }
 		gene_bs_nose_height={ "nose_height_neg" 51 "nose_height_neg" 43 }
 		gene_bs_nose_length={ "nose_length_neg" 74 "nose_length_pos" 21 }
 		gene_bs_nose_nostril_height={ "nose_nostril_height_neg" 95 "nose_nostril_height_neg" 65 }
 		gene_bs_nose_nostril_width={ "nose_nostril_width_pos" 73 "nose_nostril_width_pos" 11 }
 		gene_bs_nose_profile={ "nose_profile_neg" 88 "nose_profile_neg" 17 }
 		gene_bs_nose_ridge_angle={ "nose_ridge_angle_pos" 35 "nose_ridge_angle_neg" 20 }
 		gene_bs_nose_ridge_width={ "nose_ridge_width_neg" 131 "nose_ridge_width_neg" 8 }
 		gene_bs_nose_size={ "nose_size_pos" 84 "nose_size_neg" 18 }
 		gene_bs_nose_tip_angle={ "nose_tip_angle_neg" 65 "nose_tip_angle_neg" 126 }
 		gene_bs_nose_tip_forward={ "nose_tip_forward_neg" 120 "nose_tip_forward_neg" 183 }
 		gene_bs_nose_tip_width={ "nose_tip_width_neg" 208 "nose_tip_width_neg" 52 }
 		face_detail_cheek_def={ "cheek_def_02" 53 "cheek_def_02" 24 }
 		face_detail_cheek_fat={ "cheek_fat_04_pos" 63 "cheek_fat_01_pos" 141 }
 		face_detail_chin_cleft={ "chin_dimple" 6 "chin_cleft" 14 }
 		face_detail_chin_def={ "chin_def" 125 "chin_def" 77 }
 		face_detail_eye_lower_lid_def={ "eye_lower_lid_def" 233 "eye_lower_lid_def" 199 }
 		face_detail_eye_socket={ "eye_socket_03" 125 "eye_socket_03" 70 }
 		face_detail_nasolabial={ "nasolabial_03" 53 "nasolabial_02" 0 }
 		face_detail_nose_ridge_def={ "nose_ridge_def_neg" 237 "nose_ridge_def_neg" 121 }
 		face_detail_nose_tip_def={ "nose_tip_def" 100 "nose_tip_def" 106 }
 		face_detail_temple_def={ "temple_def" 59 "temple_def" 125 }
 		expression_brow_wrinkles={ "brow_wrinkles_03" 132 "brow_wrinkles_03" 45 }
 		expression_eye_wrinkles={ "eye_wrinkles_02" 130 "eye_wrinkles_01" 115 }
 		expression_forehead_wrinkles={ "forehead_wrinkles_02" 92 "forehead_wrinkles_03" 148 }
 		expression_other={ "cheek_wrinkles_both_01" 103 "cheek_wrinkles_both_01" 136 }
 		complexion={ "complexion_1" 191 "complexion_1" 92 }
 		gene_height={ "normal_height" 102 "normal_height" 127 }
 		gene_bs_body_type={ "body_fat_head_fat_low" 149 "body_fat_head_fat_low" 144 }
 		gene_bs_body_shape={ "body_shape_average" 112 "body_shape_rectangle_half" 0 }
 		gene_bs_bust={ "bust_clothes" 108 "bust_shape_2_half" 5 }
 		gene_age={ "old_4" 111 "old_1" 98 }
 		gene_eyebrows_shape={ "avg_spacing_avg_thickness" 133 "avg_spacing_avg_thickness" 242 }
 		gene_eyebrows_fullness={ "layer_2_low_thickness" 202 "layer_2_avg_thickness" 223 }
 		gene_body_hair={ "body_hair_sparse_low_stubble" 129 "body_hair_sparse_low_stubble" 133 }
 		gene_hair_type={ "hair_straight_thin_beard" 182 "hair_straight_thin_beard" 190 }
 		gene_baldness={ "male_pattern_baldness" 130 "male_pattern_baldness" 165 }
 		eye_accessory={ "normal_eyes" 255 "normal_eyes" 255 }
 		teeth_accessory={ "normal_teeth" 0 "normal_teeth" 0 }
 		eyelashes_accessory={ "asian_eyelashes" 255 "asian_eyelashes" 255 }
 		face_detail_eye_upper_lid_def={ "default_upper_lid_def" 127 "default_upper_lid_def" 127 }
 		face_detail_monolid={ "monolid" 217 "monolid" 231 }
 		gene_eye_size={ "eye_size" 126 "eye_size" 127 }
 		gene_eye_shut_top={ "eye_shut_top" 148 "eye_shut_top" 140 }
 		gene_eye_shut_bottom={ "eye_shut_bottom" 111 "eye_shut_bottom" 127 }
 		gene_bs_eye_height_inside={ "eye_height_inside_neg" 15 "eye_height_inside_pos" 18 }
 		gene_bs_eye_height_outisde={ "eye_height_outisde_neg" 97 "eye_height_outisde_neg" 75 }
 		gene_bs_ear_lobe={ "ear_lobe_fused" 255 "ear_lobe_fused" 214 }
 		gene_bs_nose_central_width={ "nose_central_width_pos" 87 "nose_central_width_neg" 12 }
 		gene_bs_nose_septum_width={ "nose_septum_width_pos" 30 "nose_septum_width_pos" 73 }
 		gene_forehead_inner_brow_width={ "vanilla_inner_brow_width" 127 "vanilla_inner_brow_width" 127 }
 		gene_bs_mouth_lower_lip_profile={ "lower_lip_profile" 28 "vanilla_lower_lip_profile" 127 }
 		gene_bs_eye_outer_width={ "vanilla_eye_outer_width" 127 "vanilla_eye_outer_width" 127 }
 		gene_bs_head_asymmetry_1={ "vanilla_head_asymmetry_1" 127 "vanilla_head_asymmetry_1" 127 }
 		gene_bs_eye_fold_2={ "vanilla_eye_fold_2" 127 "vanilla_eye_fold_2" 127 }
 		gene_bs_mouth_center_curve={ "vanilla_mouth_center_curve" 127 "vanilla_mouth_center_curve" 127 }
 		gene_bs_eyebrow_straight={ "vanilla_eyebrow_straight" 127 "vanilla_eyebrow_straight" 127 }
 		gene_bs_head_round_shape={ "vanilla_head_round_shape" 127 "vanilla_head_round_shape" 127 }
 		gene_bs_nose_septum_height={ "vanilla_nose_septum_height" 127 "vanilla_nose_septum_height" 127 }
 		gene_bs_head_lower_height={ "vanilla_head_lower_height" 127 "vanilla_head_lower_height" 127 }
 		gene_bs_nose_flared_nostril={ "vanilla_nose_flared_nostril" 127 "vanilla_nose_flared_nostril" 127 }
 		gene_bs_mouth_upper_lip_forward={ "vanilla_upper_lip_forward" 127 "vanilla_upper_lip_forward" 127 }
 		gene_bs_mouth_lower_lip_forward={ "vanilla_lower_lip_forward" 127 "vanilla_lower_lip_forward" 127 }
 		gene_bs_nose_swollen={ "vanilla_nose_swollen" 127 "vanilla_nose_swollen" 127 }
 		gene_bs_ears_fantasy={ "vanilla_ears_fantasy" 127 "vanilla_ears_fantasy" 127 }
 		gene_bs_mouth_glamour_lips={ "vanilla_mouth_glamour_lips" 127 "vanilla_mouth_glamour_lips" 127 }
 		pose={ "" 255 "" 0 }
 		beards={ "japanese_beards" 46 "no_beard" 0 }
 		hairstyles={ "japanese_hairstyles_fp2" 194 "all_hairstyles" 0 }
 		legwear={ "japanese_tabi" 46 "all_legwear" 0 }
 		gene_shrink_body={ "shrink_all" 255 "" 0 }
 		gene_bs_additive_headgears={ "additive_headgears" 255 "" 0 }
 		gene_bs_cloak_offset={ "cloak_offset" 255 "" 0 }
 		gene_bs_long_beard={ "long_beard" 255 "" 0 }
 		clothes={ "japanese_high_nobility_clothes" 108 "most_clothes" 0 }
 		shogunate_special_belt_accessory={ "katana_01" 182 "katana_01" 0 }
 		headgear={ "shogunate_suieikan" 46 "no_headgear" 0 }
 		gene_balding_hair_effect={ "no_baldness" 255 "no_baldness" 0 }
 }
	entity={ 2268070609 2268070609 }
	tags={ {
			hash=1989892411
			invert=no
		}
 {
			hash=2125534279
			invert=no
		}
 {
			hash=3941715396
			invert=no
		}
 {
			hash=681013727
			invert=no
		}
 {
			hash=3672171019
			invert=no
		}
 {
			hash=4196965543
			invert=no
		}
 {
			hash=2670398845
			invert=no
		}
 {
			hash=1500963499
			invert=no
		}
 {
			hash=1563190100
			invert=no
		}
 }
}

