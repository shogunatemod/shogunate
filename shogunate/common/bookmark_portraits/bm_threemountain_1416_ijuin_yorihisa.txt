# Auto generated file, do not edit manually. Created using console command dump_bookmark_portraits
# History database id:10060809
bm_threemountain_1416_ijuin_yorihisa={
	type=male
	id=17953
	age=0.460000
	genes={ 		hair_color={ 15 244 16 247 }
 		skin_color={ 108 59 117 60 }
 		eye_color={ 15 243 18 251 }
 		gene_chin_forward={ "chin_forward_pos" 139 "chin_forward_pos" 101 }
 		gene_chin_height={ "chin_height_neg" 132 "chin_height_neg" 124 }
 		gene_chin_width={ "chin_width_neg" 127 "chin_width_neg" 139 }
 		gene_eye_angle={ "eye_angle_neg" 140 "eye_angle_neg" 143 }
 		gene_eye_depth={ "eye_depth_pos" 88 "eye_depth_pos" 187 }
 		gene_eye_height={ "eye_height_pos" 146 "eye_height_pos" 143 }
 		gene_eye_distance={ "eye_distance_pos" 63 "eye_distance_pos" 91 }
 		gene_eye_shut={ "eye_shut_pos" 176 "eye_shut_pos" 140 }
 		gene_forehead_angle={ "forehead_angle_neg" 135 "forehead_angle_neg" 144 }
 		gene_forehead_brow_height={ "forehead_brow_height_neg" 140 "forehead_brow_height_neg" 157 }
 		gene_forehead_roundness={ "forehead_roundness_neg" 167 "forehead_roundness_neg" 155 }
 		gene_forehead_width={ "forehead_width_neg" 149 "forehead_width_neg" 144 }
 		gene_forehead_height={ "forehead_height_neg" 146 "forehead_height_neg" 125 }
 		gene_head_height={ "head_height_pos" 129 "head_height_pos" 131 }
 		gene_head_width={ "head_width_pos" 111 "head_width_pos" 127 }
 		gene_head_profile={ "head_profile_pos" 39 "head_profile_pos" 54 }
 		gene_head_top_height={ "head_top_height_pos" 117 "head_top_height_pos" 147 }
 		gene_head_top_width={ "head_top_width_neg" 138 "head_top_width_neg" 138 }
 		gene_jaw_angle={ "jaw_angle_pos" 140 "jaw_angle_pos" 144 }
 		gene_jaw_forward={ "jaw_forward_pos" 148 "jaw_forward_pos" 116 }
 		gene_jaw_height={ "jaw_height_pos" 107 "jaw_height_pos" 111 }
 		gene_jaw_width={ "jaw_width_neg" 132 "jaw_width_neg" 122 }
 		gene_mouth_corner_depth={ "mouth_corner_depth_neg" 176 "mouth_corner_depth_neg" 173 }
 		gene_mouth_corner_height={ "mouth_corner_height_pos" 104 "mouth_corner_height_pos" 109 }
 		gene_mouth_forward={ "mouth_forward_neg" 132 "mouth_forward_neg" 139 }
 		gene_mouth_height={ "mouth_height_neg" 149 "mouth_height_neg" 152 }
 		gene_mouth_width={ "mouth_width_neg" 66 "mouth_width_neg" 68 }
 		gene_mouth_upper_lip_size={ "mouth_upper_lip_size_neg" 92 "mouth_upper_lip_size_neg" 54 }
 		gene_mouth_lower_lip_size={ "mouth_lower_lip_size_pos" 60 "mouth_lower_lip_size_pos" 123 }
 		gene_mouth_open={ "mouth_open_neg" 23 "mouth_open_neg" 86 }
 		gene_neck_length={ "neck_length_pos" 131 "neck_length_pos" 133 }
 		gene_neck_width={ "neck_width_pos" 77 "neck_width_pos" 89 }
 		gene_bs_cheek_forward={ "cheek_forward_neg" 125 "cheek_forward_neg" 123 }
 		gene_bs_cheek_height={ "cheek_height_neg" 149 "cheek_height_pos" 15 }
 		gene_bs_cheek_width={ "cheek_width_pos" 231 "cheek_width_pos" 200 }
 		gene_bs_ear_angle={ "ear_angle_pos" 10 "ear_angle_pos" 19 }
 		gene_bs_ear_inner_shape={ "ear_inner_shape_pos" 34 "ear_inner_shape_pos" 29 }
 		gene_bs_ear_bend={ "ear_both_bend_pos" 72 "ear_both_bend_pos" 72 }
 		gene_bs_ear_outward={ "ear_outward_pos" 29 "ear_outward_pos" 47 }
 		gene_bs_ear_size={ "ear_size_pos" 52 "ear_size_pos" 65 }
 		gene_bs_eye_corner_depth={ "eye_corner_depth_pos" 21 "eye_corner_depth_pos" 23 }
 		gene_bs_eye_fold_shape={ "eye_fold_shape_neg" 0 "eye_fold_shape_neg" 0 }
 		gene_bs_eye_size={ "eye_size_neg" 109 "eye_size_neg" 113 }
 		gene_bs_eye_upper_lid_size={ "eye_upper_lid_size_neg" 29 "eye_upper_lid_size_neg" 53 }
 		gene_bs_forehead_brow_curve={ "forehead_brow_curve_neg" 246 "forehead_brow_curve_neg" 32 }
 		gene_bs_forehead_brow_forward={ "forehead_brow_forward_neg" 24 "forehead_brow_forward_neg" 20 }
 		gene_bs_forehead_brow_inner_height={ "forehead_brow_inner_height_pos" 5 "forehead_brow_inner_height_pos" 34 }
 		gene_bs_forehead_brow_outer_height={ "forehead_brow_outer_height_neg" 18 "forehead_brow_outer_height_pos" 31 }
 		gene_bs_forehead_brow_width={ "forehead_brow_width_pos" 115 "forehead_brow_width_neg" 67 }
 		gene_bs_jaw_def={ "jaw_def_neg" 106 "jaw_def_pos" 2 }
 		gene_bs_mouth_lower_lip_def={ "mouth_lower_lip_def_pos" 37 "mouth_lower_lip_def_pos" 38 }
 		gene_bs_mouth_lower_lip_full={ "mouth_lower_lip_full_pos" 73 "mouth_lower_lip_full_pos" 74 }
 		gene_bs_mouth_lower_lip_pad={ "mouth_lower_lip_pad_pos" 46 "mouth_lower_lip_pad_pos" 24 }
 		gene_bs_mouth_lower_lip_width={ "mouth_lower_lip_width_pos" 77 "mouth_lower_lip_width_pos" 52 }
 		gene_bs_mouth_philtrum_def={ "mouth_philtrum_def_pos" 85 "mouth_philtrum_def_pos" 96 }
 		gene_bs_mouth_philtrum_shape={ "mouth_philtrum_shape_neg" 48 "mouth_philtrum_shape_neg" 3 }
 		gene_bs_mouth_philtrum_width={ "mouth_philtrum_width_pos" 2 "mouth_philtrum_width_pos" 96 }
 		gene_bs_mouth_upper_lip_def={ "mouth_upper_lip_def_pos" 160 "mouth_upper_lip_def_pos" 129 }
 		gene_bs_mouth_upper_lip_full={ "mouth_upper_lip_full_pos" 114 "mouth_upper_lip_full_pos" 117 }
 		gene_bs_mouth_upper_lip_profile={ "mouth_upper_lip_profile_pos" 17 "mouth_upper_lip_profile_neg" 26 }
 		gene_bs_mouth_upper_lip_width={ "mouth_upper_lip_width_pos" 57 "mouth_upper_lip_width_pos" 42 }
 		gene_bs_nose_forward={ "nose_forward_pos" 55 "nose_forward_pos" 72 }
 		gene_bs_nose_height={ "nose_height_neg" 63 "nose_height_neg" 56 }
 		gene_bs_nose_length={ "nose_length_pos" 33 "nose_length_pos" 11 }
 		gene_bs_nose_nostril_height={ "nose_nostril_height_neg" 102 "nose_nostril_height_pos" 41 }
 		gene_bs_nose_nostril_width={ "nose_nostril_width_pos" 32 "nose_nostril_width_pos" 61 }
 		gene_bs_nose_profile={ "nose_profile_neg" 23 "nose_profile_hawk" 12 }
 		gene_bs_nose_ridge_angle={ "nose_ridge_angle_pos" 72 "nose_ridge_angle_pos" 90 }
 		gene_bs_nose_ridge_width={ "nose_ridge_width_pos" 2 "nose_ridge_width_neg" 10 }
 		gene_bs_nose_size={ "nose_size_neg" 46 "nose_size_neg" 205 }
 		gene_bs_nose_tip_angle={ "nose_tip_angle_neg" 28 "nose_tip_angle_pos" 39 }
 		gene_bs_nose_tip_forward={ "nose_tip_forward_neg" 117 "nose_tip_forward_neg" 116 }
 		gene_bs_nose_tip_width={ "nose_tip_width_neg" 211 "nose_tip_width_neg" 68 }
 		face_detail_cheek_def={ "cheek_def_02" 2 "cheek_def_02" 32 }
 		face_detail_cheek_fat={ "cheek_fat_01_pos" 118 "cheek_fat_04_pos" 94 }
 		face_detail_chin_cleft={ "chin_dimple" 24 "chin_cleft" 4 }
 		face_detail_chin_def={ "chin_def" 44 "chin_def" 67 }
 		face_detail_eye_lower_lid_def={ "eye_lower_lid_def" 212 "eye_lower_lid_def" 158 }
 		face_detail_eye_socket={ "eye_socket_01" 87 "eye_socket_01" 100 }
 		face_detail_nasolabial={ "nasolabial_04" 24 "nasolabial_03" 10 }
 		face_detail_nose_ridge_def={ "nose_ridge_def_neg" 136 "nose_ridge_def_neg" 219 }
 		face_detail_nose_tip_def={ "nose_tip_def" 188 "nose_tip_def" 127 }
 		face_detail_temple_def={ "temple_def" 105 "temple_def" 69 }
 		expression_brow_wrinkles={ "brow_wrinkles_03" 129 "brow_wrinkles_04" 70 }
 		expression_eye_wrinkles={ "eye_wrinkles_03" 12 "eye_wrinkles_02" 130 }
 		expression_forehead_wrinkles={ "forehead_wrinkles_02" 119 "forehead_wrinkles_03" 144 }
 		expression_other={ "cheek_wrinkles_both_01" 165 "cheek_wrinkles_both_01" 106 }
 		complexion={ "complexion_1" 120 "complexion_1" 18 }
 		gene_height={ "normal_height" 121 "normal_height" 126 }
 		gene_bs_body_type={ "body_fat_head_fat_full" 151 "body_fat_head_fat_medium" 106 }
 		gene_bs_body_shape={ "body_shape_pear_half" 132 "body_shape_hourglass_full" 0 }
 		gene_bs_bust={ "bust_clothes" 93 "bust_default" 105 }
 		gene_age={ "old_3" 117 "old_4" 130 }
 		gene_eyebrows_shape={ "avg_spacing_avg_thickness" 248 "far_spacing_low_thickness" 175 }
 		gene_eyebrows_fullness={ "layer_2_avg_thickness" 197 "layer_2_low_thickness" 199 }
 		gene_body_hair={ "body_hair_sparse_low_stubble" 43 "body_hair_sparse_low_stubble" 23 }
 		gene_hair_type={ "hair_straight_thin_beard" 115 "hair_straight_thin_beard" 157 }
 		gene_baldness={ "male_pattern_baldness" 84 "male_pattern_baldness" 188 }
 		eye_accessory={ "normal_eyes" 255 "normal_eyes" 255 }
 		teeth_accessory={ "normal_teeth" 0 "normal_teeth" 0 }
 		eyelashes_accessory={ "asian_eyelashes" 255 "asian_eyelashes" 255 }
 		face_detail_eye_upper_lid_def={ "default_upper_lid_def" 127 "default_upper_lid_def" 127 }
 		face_detail_monolid={ "monolid" 166 "monolid" 242 }
 		gene_eye_size={ "eye_size" 131 "eye_size" 135 }
 		gene_eye_shut_top={ "eye_shut_top" 135 "eye_shut_top" 129 }
 		gene_eye_shut_bottom={ "eye_shut_bottom" 105 "eye_shut_bottom" 104 }
 		gene_bs_eye_height_inside={ "eye_height_inside_neg" 40 "eye_height_inside_neg" 15 }
 		gene_bs_eye_height_outisde={ "eye_height_outisde_neg" 120 "eye_height_outisde_pos" 18 }
 		gene_bs_ear_lobe={ "ear_lobe_fused" 250 "ear_lobe_fused" 244 }
 		gene_bs_nose_central_width={ "nose_central_width_pos" 109 "nose_central_width_pos" 104 }
 		gene_bs_nose_septum_width={ "nose_septum_width_pos" 81 "nose_septum_width_pos" 4 }
 		gene_forehead_inner_brow_width={ "vanilla_inner_brow_width" 127 "vanilla_inner_brow_width" 127 }
 		gene_bs_mouth_lower_lip_profile={ "vanilla_lower_lip_profile" 127 "vanilla_lower_lip_profile" 127 }
 		gene_bs_eye_outer_width={ "vanilla_eye_outer_width" 127 "vanilla_eye_outer_width" 127 }
 		gene_bs_head_asymmetry_1={ "vanilla_head_asymmetry_1" 127 "vanilla_head_asymmetry_1" 127 }
 		gene_bs_eye_fold_2={ "vanilla_eye_fold_2" 127 "vanilla_eye_fold_2" 127 }
 		gene_bs_mouth_center_curve={ "vanilla_mouth_center_curve" 127 "vanilla_mouth_center_curve" 127 }
 		gene_bs_eyebrow_straight={ "vanilla_eyebrow_straight" 127 "vanilla_eyebrow_straight" 127 }
 		gene_bs_head_round_shape={ "vanilla_head_round_shape" 127 "vanilla_head_round_shape" 127 }
 		gene_bs_nose_septum_height={ "vanilla_nose_septum_height" 127 "vanilla_nose_septum_height" 127 }
 		gene_bs_head_lower_height={ "vanilla_head_lower_height" 127 "vanilla_head_lower_height" 127 }
 		gene_bs_nose_flared_nostril={ "vanilla_nose_flared_nostril" 127 "vanilla_nose_flared_nostril" 127 }
 		gene_bs_mouth_upper_lip_forward={ "vanilla_upper_lip_forward" 127 "vanilla_upper_lip_forward" 127 }
 		gene_bs_mouth_lower_lip_forward={ "vanilla_lower_lip_forward" 127 "vanilla_lower_lip_forward" 127 }
 		gene_bs_nose_swollen={ "vanilla_nose_swollen" 127 "vanilla_nose_swollen" 127 }
 		gene_bs_ears_fantasy={ "vanilla_ears_fantasy" 127 "vanilla_ears_fantasy" 127 }
 		gene_bs_mouth_glamour_lips={ "vanilla_mouth_glamour_lips" 127 "vanilla_mouth_glamour_lips" 127 }
 		pose={ "" 255 "" 0 }
 		beards={ "japanese_beards" 115 "no_beard" 0 }
 		hairstyles={ "japanese_hairstyles_fp2" 169 "all_hairstyles" 0 }
 		legwear={ "japanese_tabi" 115 "all_legwear" 0 }
 		gene_shrink_body={ "shrink_all" 255 "" 0 }
 		gene_bs_additive_headgears={ "additive_headgears" 255 "" 0 }
 		gene_bs_cloak_offset={ "cloak_offset" 255 "" 0 }
 		gene_bs_long_beard={ "long_beard" 255 "" 0 }
 		clothes={ "japanese_low_nobility_clothes" 216 "most_clothes" 0 }
 		shogunate_special_belt_accessory={ "katana_01" 146 "katana_01" 0 }
 		headgear={ "shogunate_suieikan" 115 "no_headgear" 0 }
 		gene_balding_hair_effect={ "baldness_stage_2" 255 "no_baldness" 0 }
 }
	entity={ 3293488942 3293488942 }
	tags={ {
			hash=1989892411
			invert=no
		}
 {
			hash=2125534279
			invert=no
		}
 {
			hash=3941715396
			invert=no
		}
 {
			hash=681013727
			invert=no
		}
 {
			hash=3672171019
			invert=no
		}
 {
			hash=4196965543
			invert=no
		}
 {
			hash=2670398845
			invert=no
		}
 {
			hash=1500963499
			invert=no
		}
 {
			hash=2951898311
			invert=no
		}
 {
			hash=1563190100
			invert=no
		}
 }
}

