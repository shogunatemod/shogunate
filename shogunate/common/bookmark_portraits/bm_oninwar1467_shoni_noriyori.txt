# Auto generated file, do not edit manually. Created using console command dump_bookmark_portraits
# History database id:10068004
bm_oninwar1467_shoni_noriyori={
	type=male
	id=19492
	age=0.470000
	genes={ 		hair_color={ 11 244 11 244 }
 		skin_color={ 198 74 197 72 }
 		eye_color={ 20 248 2 196 }
 		gene_chin_forward={ "chin_forward_pos" 106 "chin_forward_pos" 138 }
 		gene_chin_height={ "chin_height_neg" 103 "chin_height_neg" 119 }
 		gene_chin_width={ "chin_width_neg" 131 "chin_width_neg" 129 }
 		gene_eye_angle={ "eye_angle_neg" 141 "eye_angle_neg" 126 }
 		gene_eye_depth={ "eye_depth_pos" 173 "eye_depth_pos" 169 }
 		gene_eye_height={ "eye_height_pos" 150 "eye_height_pos" 130 }
 		gene_eye_distance={ "eye_distance_pos" 96 "eye_distance_pos" 78 }
 		gene_eye_shut={ "eye_shut_pos" 169 "eye_shut_pos" 141 }
 		gene_forehead_angle={ "forehead_angle_neg" 147 "forehead_angle_neg" 158 }
 		gene_forehead_brow_height={ "forehead_brow_height_neg" 172 "forehead_brow_height_neg" 163 }
 		gene_forehead_roundness={ "forehead_roundness_neg" 117 "forehead_roundness_neg" 142 }
 		gene_forehead_width={ "forehead_width_neg" 148 "forehead_width_neg" 163 }
 		gene_forehead_height={ "forehead_height_neg" 158 "forehead_height_neg" 135 }
 		gene_head_height={ "head_height_pos" 145 "head_height_pos" 138 }
 		gene_head_width={ "head_width_pos" 98 "head_width_pos" 94 }
 		gene_head_profile={ "head_profile_pos" 48 "head_profile_pos" 79 }
 		gene_head_top_height={ "head_top_height_pos" 125 "head_top_height_pos" 149 }
 		gene_head_top_width={ "head_top_width_neg" 135 "head_top_width_neg" 162 }
 		gene_jaw_angle={ "jaw_angle_pos" 74 "jaw_angle_pos" 135 }
 		gene_jaw_forward={ "jaw_forward_pos" 140 "jaw_forward_neg" 104 }
 		gene_jaw_height={ "jaw_height_pos" 114 "jaw_height_pos" 108 }
 		gene_jaw_width={ "jaw_width_neg" 134 "jaw_width_neg" 173 }
 		gene_mouth_corner_depth={ "mouth_corner_depth_neg" 160 "mouth_corner_depth_neg" 180 }
 		gene_mouth_corner_height={ "mouth_corner_height_pos" 95 "mouth_corner_height_pos" 112 }
 		gene_mouth_forward={ "mouth_forward_neg" 142 "mouth_forward_neg" 129 }
 		gene_mouth_height={ "mouth_height_neg" 152 "mouth_height_neg" 151 }
 		gene_mouth_width={ "mouth_width_neg" 14 "mouth_width_neg" 114 }
 		gene_mouth_upper_lip_size={ "mouth_upper_lip_size_neg" 112 "mouth_upper_lip_size_neg" 84 }
 		gene_mouth_lower_lip_size={ "mouth_lower_lip_size_pos" 102 "mouth_lower_lip_size_pos" 109 }
 		gene_mouth_open={ "mouth_open_neg" 46 "mouth_open_neg" 35 }
 		gene_neck_length={ "neck_length_pos" 139 "neck_length_pos" 130 }
 		gene_neck_width={ "neck_width_pos" 84 "neck_width_pos" 89 }
 		gene_bs_cheek_forward={ "cheek_forward_neg" 107 "cheek_forward_neg" 104 }
 		gene_bs_cheek_height={ "cheek_height_pos" 148 "cheek_height_neg" 6 }
 		gene_bs_cheek_width={ "cheek_width_pos" 140 "cheek_width_pos" 123 }
 		gene_bs_ear_angle={ "ear_angle_pos" 6 "ear_angle_pos" 31 }
 		gene_bs_ear_inner_shape={ "ear_inner_shape_pos" 0 "ear_inner_shape_pos" 36 }
 		gene_bs_ear_bend={ "ear_both_bend_pos" 37 "ear_both_bend_pos" 50 }
 		gene_bs_ear_outward={ "ear_outward_pos" 10 "ear_outward_pos" 28 }
 		gene_bs_ear_size={ "ear_size_pos" 18 "ear_size_pos" 15 }
 		gene_bs_eye_corner_depth={ "eye_corner_depth_neg" 6 "eye_corner_depth_pos" 13 }
 		gene_bs_eye_fold_shape={ "eye_fold_shape_neg" 0 "eye_fold_shape_neg" 0 }
 		gene_bs_eye_size={ "eye_size_neg" 73 "eye_size_neg" 21 }
 		gene_bs_eye_upper_lid_size={ "eye_upper_lid_size_neg" 0 "eye_upper_lid_size_neg" 60 }
 		gene_bs_forehead_brow_curve={ "forehead_brow_curve_neg" 178 "forehead_brow_curve_neg" 30 }
 		gene_bs_forehead_brow_forward={ "forehead_brow_forward_neg" 37 "forehead_brow_forward_pos" 53 }
 		gene_bs_forehead_brow_inner_height={ "forehead_brow_inner_height_pos" 25 "forehead_brow_inner_height_neg" 21 }
 		gene_bs_forehead_brow_outer_height={ "forehead_brow_outer_height_pos" 31 "forehead_brow_outer_height_pos" 51 }
 		gene_bs_forehead_brow_width={ "forehead_brow_width_pos" 78 "forehead_brow_width_pos" 119 }
 		gene_bs_jaw_def={ "jaw_def_neg" 39 "jaw_def_pos" 130 }
 		gene_bs_mouth_lower_lip_def={ "mouth_lower_lip_def_pos" 52 "mouth_lower_lip_def_pos" 37 }
 		gene_bs_mouth_lower_lip_full={ "mouth_lower_lip_full_pos" 99 "mouth_lower_lip_full_pos" 76 }
 		gene_bs_mouth_lower_lip_pad={ "mouth_lower_lip_pad_pos" 33 "mouth_lower_lip_pad_pos" 19 }
 		gene_bs_mouth_lower_lip_width={ "mouth_lower_lip_width_pos" 104 "mouth_lower_lip_width_neg" 8 }
 		gene_bs_mouth_philtrum_def={ "mouth_philtrum_def_pos" 61 "mouth_philtrum_def_pos" 100 }
 		gene_bs_mouth_philtrum_shape={ "mouth_philtrum_shape_neg" 42 "mouth_philtrum_shape_neg" 44 }
 		gene_bs_mouth_philtrum_width={ "mouth_philtrum_width_pos" 3 "mouth_philtrum_width_pos" 39 }
 		gene_bs_mouth_upper_lip_def={ "mouth_upper_lip_def_pos" 123 "mouth_upper_lip_def_pos" 124 }
 		gene_bs_mouth_upper_lip_full={ "mouth_upper_lip_full_pos" 86 "mouth_upper_lip_full_pos" 117 }
 		gene_bs_mouth_upper_lip_profile={ "mouth_upper_lip_profile_neg" 30 "mouth_upper_lip_profile_neg" 39 }
 		gene_bs_mouth_upper_lip_width={ "mouth_upper_lip_width_pos" 48 "mouth_upper_lip_width_pos" 50 }
 		gene_bs_nose_forward={ "nose_forward_pos" 35 "nose_forward_pos" 9 }
 		gene_bs_nose_height={ "nose_height_neg" 0 "nose_height_neg" 44 }
 		gene_bs_nose_length={ "nose_length_pos" 18 "nose_length_neg" 106 }
 		gene_bs_nose_nostril_height={ "nose_nostril_height_neg" 101 "nose_nostril_height_neg" 53 }
 		gene_bs_nose_nostril_width={ "nose_nostril_width_pos" 17 "nose_nostril_width_pos" 59 }
 		gene_bs_nose_profile={ "nose_profile_neg" 6 "nose_profile_neg" 17 }
 		gene_bs_nose_ridge_angle={ "nose_ridge_angle_pos" 86 "nose_ridge_angle_neg" 4 }
 		gene_bs_nose_ridge_width={ "nose_ridge_width_pos" 8 "nose_ridge_width_pos" 10 }
 		gene_bs_nose_size={ "nose_size_neg" 153 "nose_size_neg" 39 }
 		gene_bs_nose_tip_angle={ "nose_tip_angle_neg" 30 "nose_tip_angle_pos" 63 }
 		gene_bs_nose_tip_forward={ "nose_tip_forward_neg" 119 "nose_tip_forward_neg" 125 }
 		gene_bs_nose_tip_width={ "nose_tip_width_neg" 212 "nose_tip_width_pos" 103 }
 		face_detail_cheek_def={ "cheek_def_02" 53 "cheek_def_01" 28 }
 		face_detail_cheek_fat={ "cheek_fat_02_pos" 68 "cheek_fat_04_pos" 55 }
 		face_detail_chin_cleft={ "chin_cleft" 25 "chin_dimple" 8 }
 		face_detail_chin_def={ "chin_def" 59 "chin_def" 94 }
 		face_detail_eye_lower_lid_def={ "eye_lower_lid_def" 189 "eye_lower_lid_def" 152 }
 		face_detail_eye_socket={ "eye_socket_01" 150 "eye_socket_01" 48 }
 		face_detail_nasolabial={ "nasolabial_01" 115 "nasolabial_02" 104 }
 		face_detail_nose_ridge_def={ "nose_ridge_def_neg" 186 "nose_ridge_def_pos" 49 }
 		face_detail_nose_tip_def={ "nose_tip_def" 45 "nose_tip_def" 12 }
 		face_detail_temple_def={ "temple_def" 42 "temple_def" 133 }
 		expression_brow_wrinkles={ "brow_wrinkles_04" 0 "brow_wrinkles_03" 176 }
 		expression_eye_wrinkles={ "eye_wrinkles_01" 4 "eye_wrinkles_02" 136 }
 		expression_forehead_wrinkles={ "forehead_wrinkles_01" 135 "forehead_wrinkles_02" 41 }
 		expression_other={ "cheek_wrinkles_both_01" 125 "cheek_wrinkles_both_01" 165 }
 		complexion={ "complexion_1" 167 "complexion_1" 49 }
 		gene_height={ "normal_height" 131 "normal_height" 148 }
 		gene_bs_body_type={ "body_fat_head_fat_full" 115 "body_fat_head_fat_low" 147 }
 		gene_bs_body_shape={ "body_shape_hourglass_half" 91 "body_shape_rectangle_full" 0 }
 		gene_bs_bust={ "bust_clothes" 173 "bust_shape_1_full" 19 }
 		gene_age={ "old_4" 215 "old_4" 143 }
 		gene_eyebrows_shape={ "far_spacing_low_thickness" 181 "far_spacing_low_thickness" 243 }
 		gene_eyebrows_fullness={ "layer_2_avg_thickness" 245 "layer_2_avg_thickness" 205 }
 		gene_body_hair={ "body_hair_sparse_low_stubble" 147 "body_hair_sparse_low_stubble" 125 }
 		gene_hair_type={ "hair_straight_thin_beard" 118 "hair_straight_thin_beard" 190 }
 		gene_baldness={ "male_pattern_baldness" 193 "male_pattern_baldness" 129 }
 		eye_accessory={ "normal_eyes" 255 "normal_eyes" 255 }
 		teeth_accessory={ "normal_teeth" 0 "normal_teeth" 0 }
 		eyelashes_accessory={ "asian_eyelashes" 255 "asian_eyelashes" 255 }
 		face_detail_eye_upper_lid_def={ "default_upper_lid_def" 127 "default_upper_lid_def" 127 }
 		face_detail_monolid={ "monolid" 249 "monolid" 141 }
 		gene_eye_size={ "eye_size" 119 "eye_size" 133 }
 		gene_eye_shut_top={ "eye_shut_top" 151 "eye_shut_top" 152 }
 		gene_eye_shut_bottom={ "eye_shut_bottom" 127 "eye_shut_bottom" 117 }
 		gene_bs_eye_height_inside={ "eye_height_inside_pos" 54 "eye_height_inside_pos" 29 }
 		gene_bs_eye_height_outisde={ "eye_height_outisde_neg" 31 "eye_height_outisde_neg" 68 }
 		gene_bs_ear_lobe={ "ear_lobe_fused" 202 "ear_lobe_fused" 227 }
 		gene_bs_nose_central_width={ "nose_central_width_pos" 115 "nose_central_width_neg" 33 }
 		gene_bs_nose_septum_width={ "nose_septum_width_pos" 98 "nose_septum_width_pos" 87 }
 		gene_forehead_inner_brow_width={ "vanilla_inner_brow_width" 127 "vanilla_inner_brow_width" 127 }
 		gene_bs_mouth_lower_lip_profile={ "vanilla_lower_lip_profile" 127 "vanilla_lower_lip_profile" 127 }
 		gene_bs_eye_outer_width={ "vanilla_eye_outer_width" 127 "vanilla_eye_outer_width" 127 }
 		gene_bs_head_asymmetry_1={ "vanilla_head_asymmetry_1" 127 "vanilla_head_asymmetry_1" 127 }
 		gene_bs_eye_fold_2={ "vanilla_eye_fold_2" 127 "vanilla_eye_fold_2" 127 }
 		gene_bs_mouth_center_curve={ "vanilla_mouth_center_curve" 127 "vanilla_mouth_center_curve" 127 }
 		gene_bs_eyebrow_straight={ "vanilla_eyebrow_straight" 127 "vanilla_eyebrow_straight" 127 }
 		gene_bs_head_round_shape={ "vanilla_head_round_shape" 127 "vanilla_head_round_shape" 127 }
 		gene_bs_nose_septum_height={ "vanilla_nose_septum_height" 127 "vanilla_nose_septum_height" 127 }
 		gene_bs_head_lower_height={ "vanilla_head_lower_height" 127 "vanilla_head_lower_height" 127 }
 		gene_bs_nose_flared_nostril={ "vanilla_nose_flared_nostril" 127 "vanilla_nose_flared_nostril" 127 }
 		gene_bs_mouth_upper_lip_forward={ "vanilla_upper_lip_forward" 127 "vanilla_upper_lip_forward" 127 }
 		gene_bs_mouth_lower_lip_forward={ "vanilla_lower_lip_forward" 127 "vanilla_lower_lip_forward" 127 }
 		gene_bs_nose_swollen={ "vanilla_nose_swollen" 127 "vanilla_nose_swollen" 127 }
 		gene_bs_ears_fantasy={ "vanilla_ears_fantasy" 127 "vanilla_ears_fantasy" 127 }
 		gene_bs_mouth_glamour_lips={ "vanilla_mouth_glamour_lips" 127 "vanilla_mouth_glamour_lips" 127 }
 		pose={ "" 255 "" 0 }
 		beards={ "japanese_beards" 107 "no_beard" 0 }
 		hairstyles={ "japanese_hairstyles_fp2" 251 "all_hairstyles" 0 }
 		legwear={ "japanese_tabi" 107 "all_legwear" 0 }
 		gene_shrink_body={ "shrink_all" 255 "" 0 }
 		gene_bs_additive_headgears={ "additive_headgears" 255 "" 0 }
 		gene_bs_cloak_offset={ "cloak_offset" 255 "" 0 }
 		gene_bs_long_beard={ "long_beard" 255 "" 0 }
 		clothes={ "japanese_high_nobility_clothes" 50 "most_clothes" 0 }
 		shogunate_special_belt_accessory={ "katana_01" 163 "katana_01" 0 }
 		headgear={ "no_headgear" 215 "no_headgear" 0 }
 		gene_balding_hair_effect={ "baldness_stage_2" 255 "no_baldness" 0 }
 }
	entity={ 807438772 807438772 }
	tags={ {
			hash=4196965543
			invert=no
		}
 {
			hash=2125534279
			invert=no
		}
 {
			hash=2670398845
			invert=no
		}
 {
			hash=2951898311
			invert=no
		}
 {
			hash=681013727
			invert=no
		}
 {
			hash=1563190100
			invert=no
		}
 }
}

