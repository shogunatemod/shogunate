# Auto generated file, do not edit manually. Created using console command dump_bookmark_portraits
# History database id:10043000
bm_funaokayama1511_azai_sukemasa={
	type=male
	id=22216
	age=0.200000
	genes={ 		hair_color={ 16 246 15 246 }
 		skin_color={ 105 66 124 63 }
 		eye_color={ 40 245 2 245 }
 		gene_chin_forward={ "chin_forward_pos" 138 "chin_forward_pos" 115 }
 		gene_chin_height={ "chin_height_neg" 120 "chin_height_neg" 117 }
 		gene_chin_width={ "chin_width_neg" 138 "chin_width_neg" 144 }
 		gene_eye_angle={ "eye_angle_neg" 130 "eye_angle_neg" 121 }
 		gene_eye_depth={ "eye_depth_pos" 168 "eye_depth_pos" 173 }
 		gene_eye_height={ "eye_height_pos" 140 "eye_height_pos" 153 }
 		gene_eye_distance={ "eye_distance_pos" 99 "eye_distance_pos" 86 }
 		gene_eye_shut={ "eye_shut_pos" 164 "eye_shut_pos" 137 }
 		gene_forehead_angle={ "forehead_angle_neg" 179 "forehead_angle_neg" 140 }
 		gene_forehead_brow_height={ "forehead_brow_height_neg" 161 "forehead_brow_height_neg" 157 }
 		gene_forehead_roundness={ "forehead_roundness_neg" 94 "forehead_roundness_neg" 223 }
 		gene_forehead_width={ "forehead_width_neg" 154 "forehead_width_neg" 141 }
 		gene_forehead_height={ "forehead_height_neg" 121 "forehead_height_neg" 117 }
 		gene_head_height={ "head_height_pos" 149 "head_height_pos" 140 }
 		gene_head_width={ "head_width_pos" 144 "head_width_pos" 146 }
 		gene_head_profile={ "head_profile_pos" 40 "head_profile_pos" 31 }
 		gene_head_top_height={ "head_top_height_pos" 116 "head_top_height_pos" 145 }
 		gene_head_top_width={ "head_top_width_neg" 142 "head_top_width_neg" 140 }
 		gene_jaw_angle={ "jaw_angle_pos" 89 "jaw_angle_pos" 155 }
 		gene_jaw_forward={ "jaw_forward_pos" 135 "jaw_forward_neg" 93 }
 		gene_jaw_height={ "jaw_height_pos" 84 "jaw_height_pos" 110 }
 		gene_jaw_width={ "jaw_width_neg" 138 "jaw_width_neg" 128 }
 		gene_mouth_corner_depth={ "mouth_corner_depth_neg" 168 "mouth_corner_depth_neg" 174 }
 		gene_mouth_corner_height={ "mouth_corner_height_pos" 84 "mouth_corner_height_pos" 116 }
 		gene_mouth_forward={ "mouth_forward_neg" 133 "mouth_forward_neg" 135 }
 		gene_mouth_height={ "mouth_height_neg" 147 "mouth_height_neg" 153 }
 		gene_mouth_width={ "mouth_width_neg" 98 "mouth_width_neg" 55 }
 		gene_mouth_upper_lip_size={ "mouth_upper_lip_size_neg" 52 "mouth_upper_lip_size_neg" 107 }
 		gene_mouth_lower_lip_size={ "mouth_lower_lip_size_pos" 117 "mouth_lower_lip_size_pos" 105 }
 		gene_mouth_open={ "mouth_open_neg" 35 "mouth_open_neg" 28 }
 		gene_neck_length={ "neck_length_pos" 143 "neck_length_pos" 137 }
 		gene_neck_width={ "neck_width_pos" 80 "neck_width_pos" 90 }
 		gene_bs_cheek_forward={ "cheek_forward_neg" 102 "cheek_forward_neg" 122 }
 		gene_bs_cheek_height={ "cheek_height_neg" 10 "cheek_height_pos" 181 }
 		gene_bs_cheek_width={ "cheek_width_pos" 80 "cheek_width_pos" 166 }
 		gene_bs_ear_angle={ "ear_angle_pos" 48 "ear_angle_pos" 62 }
 		gene_bs_ear_inner_shape={ "ear_inner_shape_pos" 20 "ear_inner_shape_pos" 38 }
 		gene_bs_ear_bend={ "ear_both_bend_pos" 42 "ear_both_bend_pos" 35 }
 		gene_bs_ear_outward={ "ear_outward_pos" 73 "ear_outward_pos" 10 }
 		gene_bs_ear_size={ "ear_size_pos" 40 "ear_size_pos" 31 }
 		gene_bs_eye_corner_depth={ "eye_corner_depth_pos" 11 "eye_corner_depth_neg" 6 }
 		gene_bs_eye_fold_shape={ "eye_fold_shape_neg" 0 "eye_fold_shape_neg" 1 }
 		gene_bs_eye_size={ "eye_size_neg" 87 "eye_size_neg" 28 }
 		gene_bs_eye_upper_lid_size={ "eye_upper_lid_size_neg" 19 "eye_upper_lid_size_neg" 27 }
 		gene_bs_forehead_brow_curve={ "forehead_brow_curve_neg" 47 "forehead_brow_curve_neg" 243 }
 		gene_bs_forehead_brow_forward={ "forehead_brow_forward_neg" 61 "forehead_brow_forward_pos" 15 }
 		gene_bs_forehead_brow_inner_height={ "forehead_brow_inner_height_pos" 31 "forehead_brow_inner_height_neg" 8 }
 		gene_bs_forehead_brow_outer_height={ "forehead_brow_outer_height_neg" 4 "forehead_brow_outer_height_pos" 38 }
 		gene_bs_forehead_brow_width={ "forehead_brow_width_pos" 121 "forehead_brow_width_neg" 40 }
 		gene_bs_jaw_def={ "jaw_def_pos" 130 "jaw_def_neg" 23 }
 		gene_bs_mouth_lower_lip_def={ "mouth_lower_lip_def_pos" 59 "mouth_lower_lip_def_pos" 35 }
 		gene_bs_mouth_lower_lip_full={ "mouth_lower_lip_full_pos" 83 "mouth_lower_lip_full_pos" 74 }
 		gene_bs_mouth_lower_lip_pad={ "mouth_lower_lip_pad_pos" 11 "mouth_lower_lip_pad_pos" 5 }
 		gene_bs_mouth_lower_lip_width={ "mouth_lower_lip_width_neg" 0 "mouth_lower_lip_width_neg" 12 }
 		gene_bs_mouth_philtrum_def={ "mouth_philtrum_def_pos" 91 "mouth_philtrum_def_pos" 88 }
 		gene_bs_mouth_philtrum_shape={ "mouth_philtrum_shape_neg" 53 "mouth_philtrum_shape_neg" 3 }
 		gene_bs_mouth_philtrum_width={ "mouth_philtrum_width_pos" 46 "mouth_philtrum_width_pos" 2 }
 		gene_bs_mouth_upper_lip_def={ "mouth_upper_lip_def_pos" 122 "mouth_upper_lip_def_pos" 125 }
 		gene_bs_mouth_upper_lip_full={ "mouth_upper_lip_full_pos" 59 "mouth_upper_lip_full_pos" 6 }
 		gene_bs_mouth_upper_lip_profile={ "mouth_upper_lip_profile_neg" 31 "mouth_upper_lip_profile_neg" 22 }
 		gene_bs_mouth_upper_lip_width={ "mouth_upper_lip_width_pos" 42 "mouth_upper_lip_width_pos" 50 }
 		gene_bs_nose_forward={ "nose_forward_pos" 9 "nose_forward_pos" 61 }
 		gene_bs_nose_height={ "nose_height_pos" 1 "nose_height_neg" 2 }
 		gene_bs_nose_length={ "nose_length_neg" 101 "nose_length_neg" 83 }
 		gene_bs_nose_nostril_height={ "nose_nostril_height_neg" 86 "nose_nostril_height_neg" 61 }
 		gene_bs_nose_nostril_width={ "nose_nostril_width_pos" 19 "nose_nostril_width_pos" 21 }
 		gene_bs_nose_profile={ "nose_profile_pos" 38 "nose_profile_neg" 17 }
 		gene_bs_nose_ridge_angle={ "nose_ridge_angle_neg" 17 "nose_ridge_angle_neg" 5 }
 		gene_bs_nose_ridge_width={ "nose_ridge_width_pos" 9 "nose_ridge_width_neg" 30 }
 		gene_bs_nose_size={ "nose_size_pos" 49 "nose_size_neg" 60 }
 		gene_bs_nose_tip_angle={ "nose_tip_angle_pos" 59 "nose_tip_angle_neg" 53 }
 		gene_bs_nose_tip_forward={ "nose_tip_forward_neg" 122 "nose_tip_forward_neg" 125 }
 		gene_bs_nose_tip_width={ "nose_tip_width_neg" 44 "nose_tip_width_neg" 156 }
 		face_detail_cheek_def={ "cheek_def_02" 17 "cheek_def_02" 86 }
 		face_detail_cheek_fat={ "cheek_fat_01_pos" 57 "cheek_fat_01_pos" 145 }
 		face_detail_chin_cleft={ "chin_cleft" 13 "chin_dimple" 6 }
 		face_detail_chin_def={ "chin_def" 52 "chin_def" 19 }
 		face_detail_eye_lower_lid_def={ "eye_lower_lid_def" 247 "eye_lower_lid_def" 219 }
 		face_detail_eye_socket={ "eye_socket_02" 173 "eye_socket_01" 146 }
 		face_detail_nasolabial={ "nasolabial_02" 51 "nasolabial_04" 1 }
 		face_detail_nose_ridge_def={ "nose_ridge_def_neg" 134 "nose_ridge_def_neg" 185 }
 		face_detail_nose_tip_def={ "nose_tip_def" 158 "nose_tip_def" 48 }
 		face_detail_temple_def={ "temple_def" 60 "temple_def" 70 }
 		expression_brow_wrinkles={ "brow_wrinkles_03" 170 "brow_wrinkles_03" 171 }
 		expression_eye_wrinkles={ "eye_wrinkles_02" 166 "eye_wrinkles_02" 152 }
 		expression_forehead_wrinkles={ "forehead_wrinkles_03" 137 "forehead_wrinkles_02" 147 }
 		expression_other={ "cheek_wrinkles_both_01" 70 "cheek_wrinkles_both_01" 164 }
 		complexion={ "complexion_1" 199 "complexion_1" 150 }
 		gene_height={ "normal_height" 105 "normal_height" 116 }
 		gene_bs_body_type={ "body_fat_head_fat_full" 118 "body_fat_head_fat_low" 152 }
 		gene_bs_body_shape={ "body_shape_average" 0 "body_shape_rectangle_half" 0 }
 		gene_bs_bust={ "bust_clothes" 12 "bust_shape_1_full" 106 }
 		gene_age={ "old_3" 102 "old_4" 172 }
 		gene_eyebrows_shape={ "avg_spacing_avg_thickness" 199 "avg_spacing_low_thickness" 182 }
 		gene_eyebrows_fullness={ "layer_2_high_thickness" 208 "layer_2_avg_thickness" 213 }
 		gene_body_hair={ "body_hair_sparse_low_stubble" 145 "body_hair_sparse_low_stubble" 86 }
 		gene_hair_type={ "hair_straight_thin_beard" 108 "hair_straight_thin_beard" 151 }
 		gene_baldness={ "male_pattern_baldness" 141 "male_pattern_baldness" 223 }
 		eye_accessory={ "normal_eyes" 255 "normal_eyes" 255 }
 		teeth_accessory={ "normal_teeth" 0 "normal_teeth" 0 }
 		eyelashes_accessory={ "asian_eyelashes" 255 "asian_eyelashes" 255 }
 		face_detail_eye_upper_lid_def={ "default_upper_lid_def" 127 "default_upper_lid_def" 127 }
 		face_detail_monolid={ "monolid" 243 "monolid" 240 }
 		gene_eye_size={ "eye_size" 130 "eye_size" 133 }
 		gene_eye_shut_top={ "eye_shut_top" 105 "eye_shut_top" 144 }
 		gene_eye_shut_bottom={ "eye_shut_bottom" 114 "eye_shut_bottom" 121 }
 		gene_bs_eye_height_inside={ "eye_height_inside_neg" 76 "eye_height_inside_pos" 3 }
 		gene_bs_eye_height_outisde={ "eye_height_outisde_pos" 74 "eye_height_outisde_neg" 80 }
 		gene_bs_ear_lobe={ "ear_lobe_fused" 223 "ear_lobe_fused" 250 }
 		gene_bs_nose_central_width={ "nose_central_width_pos" 122 "nose_central_width_pos" 32 }
 		gene_bs_nose_septum_width={ "nose_septum_width_pos" 55 "nose_septum_width_pos" 67 }
 		gene_forehead_inner_brow_width={ "vanilla_inner_brow_width" 127 "vanilla_inner_brow_width" 127 }
 		gene_bs_mouth_lower_lip_profile={ "vanilla_lower_lip_profile" 127 "lower_lip_profile" 26 }
 		gene_bs_eye_outer_width={ "vanilla_eye_outer_width" 127 "vanilla_eye_outer_width" 127 }
 		gene_bs_head_asymmetry_1={ "vanilla_head_asymmetry_1" 127 "vanilla_head_asymmetry_1" 127 }
 		gene_bs_eye_fold_2={ "vanilla_eye_fold_2" 127 "vanilla_eye_fold_2" 127 }
 		gene_bs_mouth_center_curve={ "vanilla_mouth_center_curve" 127 "vanilla_mouth_center_curve" 127 }
 		gene_bs_eyebrow_straight={ "vanilla_eyebrow_straight" 127 "vanilla_eyebrow_straight" 127 }
 		gene_bs_head_round_shape={ "vanilla_head_round_shape" 127 "vanilla_head_round_shape" 127 }
 		gene_bs_nose_septum_height={ "vanilla_nose_septum_height" 127 "vanilla_nose_septum_height" 127 }
 		gene_bs_head_lower_height={ "vanilla_head_lower_height" 127 "vanilla_head_lower_height" 127 }
 		gene_bs_nose_flared_nostril={ "vanilla_nose_flared_nostril" 127 "vanilla_nose_flared_nostril" 127 }
 		gene_bs_mouth_upper_lip_forward={ "vanilla_upper_lip_forward" 127 "vanilla_upper_lip_forward" 127 }
 		gene_bs_mouth_lower_lip_forward={ "vanilla_lower_lip_forward" 127 "vanilla_lower_lip_forward" 127 }
 		gene_bs_nose_swollen={ "vanilla_nose_swollen" 127 "vanilla_nose_swollen" 127 }
 		gene_bs_ears_fantasy={ "vanilla_ears_fantasy" 127 "vanilla_ears_fantasy" 127 }
 		gene_bs_mouth_glamour_lips={ "vanilla_mouth_glamour_lips" 127 "vanilla_mouth_glamour_lips" 127 }
 		pose={ "" 255 "" 0 }
 		beards={ "japanese_beards" 177 "no_beard" 0 }
 		hairstyles={ "japanese_hairstyles_fp2" 5 "all_hairstyles" 0 }
 		legwear={ "japanese_war_legwear" 0 "all_legwear" 0 }
 		gene_shrink_body={ "shrink_all" 255 "" 0 }
 		gene_bs_additive_headgears={ "additive_headgears" 255 "" 0 }
 		gene_bs_cloak_offset={ "cloak_offset" 255 "" 0 }
 		gene_bs_long_beard={ "long_beard" 255 "" 0 }
 		clothes={ "japanese_war_nobility_clothes" 237 "most_clothes" 0 }
 		headgear={ "shogunate_war_helmet" 5 "no_headgear" 0 }
 		shogunate_special_belt_accessory={ "katana_01" 255 "katana_01" 0 }
 		gene_balding_hair_effect={ "no_baldness" 255 "no_baldness" 0 }
 }
	entity={ 1863162561 1863162561 }
	tags={ {
			hash=1989892411
			invert=no
		}
 {
			hash=2125534279
			invert=no
		}
 {
			hash=3941715396
			invert=no
		}
 {
			hash=681013727
			invert=no
		}
 {
			hash=3672171019
			invert=no
		}
 {
			hash=2495432340
			invert=no
		}
 {
			hash=4196965543
			invert=no
		}
 {
			hash=2670398845
			invert=no
		}
 {
			hash=1500963499
			invert=no
		}
 {
			hash=1563190100
			invert=no
		}
 }
}

