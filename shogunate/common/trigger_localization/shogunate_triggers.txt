﻿shogunate_establish_shogunate_decision_kyoto_held = {
	global = establish_shogunate_decision_kyoto_held
}

shogunate_sengoku_ending_decision_powerful_enemy_trigger = {
	global = shogunate_sengoku_ending_decision_powerful_enemy_trigger_desc
}

shogunate_sengoku_ending_decision_imperial_rank_trigger = {
	global = shogunate_sengoku_ending_decision_imperial_rank_trigger_desc
}

shogunate_sengoku_ending_decision_realm_size_trigger = {
	global = shogunate_sengoku_ending_decision_realm_size_trigger_desc
}

shogunate_unify_two_courts_by_northern_court_decision_realm_size_trigger = {
	global = shogunate_unify_two_courts_by_northern_court_decision_realm_size_trigger_desc
}

shogunate_unify_two_courts_by_southern_court_decision_realm_size_trigger = {
	global = shogunate_unify_two_courts_by_southern_court_decision_realm_size_trigger_desc
}

shogunate_unify_two_courts_decision_kyoto_trigger = {
	global = shogunate_unify_two_courts_decision_kyoto_trigger_desc
}

shogunate_unify_two_courts_decision_kamakura_trigger = {
	global = shogunate_unify_two_courts_decision_kamakura_trigger_desc
}

shogunate_unify_two_courts_decision_dazaifu_trigger = {
	global = shogunate_unify_two_courts_decision_dazaifu_trigger_desc
}

shogunate_unify_two_courts_decision_taga_trigger = {
	global = shogunate_unify_two_courts_decision_taga_trigger_desc
}


shogunate_not_in_diplomatic_range_trigger = {
	global_not = NOT_IN_DIPLOMATIC_RANGE
}

shogunate_adoption_age_is_too_close_trigger = {
	global_not = SHOGUNATE_ADOPTION_AGE_IS_TOO_CLOSE
}

shogunate_adoption_father_is_not_male_trigger = {
	global_not = SHOGUNATE_ADOPTION_FATHER_IS_NOT_MALE
}

shogunate_adoption_child_is_married_or_betrothed_trigger = {
	global_not = SHOGUNATE_ADOPTION_CHILD_IS_MARRIED_OR_BETROTHED
}

shogunate_adoption_actor_is_not_dynast_trigger = {
	global_not = SHOGUNATE_ADOPTION_ACTOR_IS_NOT_DYNAST
}


shogunate_gain_court_post_decision_unique_trigger = {
	global_not = shogunate_gain_court_post_decision_unique_trigger_desc
}

shogunate_gain_court_post_decision_limited_trigger = {
	global_not = shogunate_gain_court_post_decision_limited_trigger_desc
}

shogunate_gain_court_post_decision_center_trigger = {
	global_not = shogunate_gain_court_post_decision_center_trigger_desc
}

shogunate_gain_court_post_decision_local_trigger = {
	global_not = shogunate_gain_court_post_decision_local_trigger_desc
}
