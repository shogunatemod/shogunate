﻿# Exchange Piety Modifiers

shogunate_seize_manor_modifier = {
	icon = economy_positive
	tax_mult = 0.2
}

shogunate_build_temple_modifier = {
	icon = prestige_positive
	monthly_prestige_gain_mult = 0.2
}

shogunate_mobilize_follower_modifier = {
	icon = martial_positive
	levy_reinforcement_rate = 0.2
}

shogunate_pray_modifier = {
	icon = health_positive
	health = 0.2
}

shogunate_curse_modifier = {
	icon = intrigue_positive
	hostile_scheme_phase_duration_add = medium_scheme_phase_duration_bonus_value
}

shogunate_enlightenment_modifier = {
	icon = learning_positive
	monthly_lifestyle_xp_gain_mult = 0.2
}

# Nanboku-cho Period Modifiers

shogunate_betrayer_modifier = {
	icon = diplomacy_negative
	independent_ruler_opinion = -50
}
