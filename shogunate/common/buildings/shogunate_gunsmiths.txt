﻿##########
# Kunitomo Gunsmiths
##########

kunitomo_gunsmiths_01 = {

	construction_time = slow_construction_time

	type_icon = "icon_building_blacksmiths.dds"

	can_construct_potential = {
		building_requirement_tribal = no
	}
	can_construct = {
		scope:holder.culture = {
			has_innovation = innovation_advanced_bowmaking	# Arquebus
		}
	}
	
	cost_gold = expensive_building_tier_3_cost
	
	county_modifier = {
		tax_mult = 0.05
		development_growth_factor = 0.05
	}
	
	next_building = kunitomo_gunsmiths_02
	
	ai_value = {
		base = 100
	}
	
	type = special

	flag = travel_point_of_interest_martial
}

kunitomo_gunsmiths_02 = {

	construction_time = slow_construction_time

	type_icon = "icon_building_blacksmiths.dds"

	can_construct_potential = {
		building_requirement_tribal = no
	}
	can_construct = {
		scope:holder.culture = {
			has_innovation = innovation_advanced_bowmaking	# Arquebus
		}
	}
	
	cost_gold = expensive_building_tier_4_cost
	
	county_modifier = {
		tax_mult = 0.1
		development_growth_factor = 0.1
	}
	
	next_building = kunitomo_gunsmiths_03
	
	ai_value = {
		base = 100
	}
	
	type = special

	flag = travel_point_of_interest_martial
}

kunitomo_gunsmiths_03 = {

	construction_time = slow_construction_time

	type_icon = "icon_building_blacksmiths.dds"

	can_construct_potential = {
		building_requirement_tribal = no
	}
	can_construct = {
		scope:holder.culture = {
			has_innovation = innovation_advanced_bowmaking	# Arquebus
		}
	}
	
	cost_gold = expensive_building_tier_5_cost
	
	county_modifier = {
		tax_mult = 0.15
		development_growth_factor = 0.15
	}
	
	next_building = kunitomo_gunsmiths_04
	
	ai_value = {
		base = 100
	}
	
	type = special

	flag = travel_point_of_interest_martial
}

kunitomo_gunsmiths_04 = {

	construction_time = slow_construction_time

	type_icon = "icon_building_blacksmiths.dds"

	can_construct_potential = {
		building_requirement_tribal = no
	}
	can_construct = {
		scope:holder.culture = {
			has_innovation = innovation_bombard	# Cannons
		}
	}
	
	cost_gold = expensive_building_tier_6_cost
	
	county_modifier = {
		tax_mult = 0.2
		development_growth_factor = 0.2
	}
	
	ai_value = {
		base = 100
	}
	
	type = special

	flag = travel_point_of_interest_martial
}

##########
# Negoro Gunsmiths
##########

negoro_gunsmiths_01 = {

	construction_time = slow_construction_time

	type_icon = "icon_building_blacksmiths.dds"

	can_construct_potential = {
		building_requirement_tribal = no
	}
	can_construct = {
		scope:holder.culture = {
			has_innovation = innovation_advanced_bowmaking	# Arquebus
		}
	}
	
	cost_gold = expensive_building_tier_3_cost
	
	county_modifier = {
		tax_mult = 0.05
		development_growth_factor = 0.05
	}
	
	next_building = negoro_gunsmiths_02
	
	ai_value = {
		base = 100
	}
	
	type = special

	flag = travel_point_of_interest_martial
}

negoro_gunsmiths_02 = {

	construction_time = slow_construction_time

	type_icon = "icon_building_blacksmiths.dds"

	can_construct_potential = {
		building_requirement_tribal = no
	}
	can_construct = {
		scope:holder.culture = {
			has_innovation = innovation_advanced_bowmaking	# Arquebus
		}
	}
	
	cost_gold = expensive_building_tier_4_cost
	
	county_modifier = {
		tax_mult = 0.1
		development_growth_factor = 0.1
	}
	
	next_building = negoro_gunsmiths_03
	
	ai_value = {
		base = 100
	}
	
	type = special

	flag = travel_point_of_interest_martial
}

negoro_gunsmiths_03 = {

	construction_time = slow_construction_time

	type_icon = "icon_building_blacksmiths.dds"

	can_construct_potential = {
		building_requirement_tribal = no
	}
	can_construct = {
		scope:holder.culture = {
			has_innovation = innovation_advanced_bowmaking	# Arquebus
		}
	}
	
	cost_gold = expensive_building_tier_5_cost
	
	county_modifier = {
		tax_mult = 0.15
		development_growth_factor = 0.15
	}
	
	next_building = negoro_gunsmiths_04
	
	ai_value = {
		base = 100
	}
	
	type = special

	flag = travel_point_of_interest_martial
}

negoro_gunsmiths_04 = {

	construction_time = slow_construction_time

	type_icon = "icon_building_blacksmiths.dds"

	can_construct_potential = {
		building_requirement_tribal = no
	}
	can_construct = {
		scope:holder.culture = {
			has_innovation = innovation_bombard	# Cannons
		}
	}
	
	cost_gold = expensive_building_tier_6_cost
	
	county_modifier = {
		tax_mult = 0.2
		development_growth_factor = 0.2
	}
	
	ai_value = {
		base = 100
	}
	
	type = special

	flag = travel_point_of_interest_martial
}

##########
# Saika Gunsmiths
##########

saika_gunsmiths_01 = {

	construction_time = slow_construction_time

	type_icon = "icon_building_blacksmiths.dds"

	can_construct_potential = {
		building_requirement_tribal = no
	}
	can_construct = {
		scope:holder.culture = {
			has_innovation = innovation_advanced_bowmaking	# Arquebus
		}
	}
	
	cost_gold = expensive_building_tier_3_cost
	
	county_modifier = {
		tax_mult = 0.05
		development_growth_factor = 0.05
	}
	
	next_building = saika_gunsmiths_02
	
	ai_value = {
		base = 100
	}
	
	type = special

	flag = travel_point_of_interest_martial
}

saika_gunsmiths_02 = {

	construction_time = slow_construction_time

	type_icon = "icon_building_blacksmiths.dds"

	can_construct_potential = {
		building_requirement_tribal = no
	}
	can_construct = {
		scope:holder.culture = {
			has_innovation = innovation_advanced_bowmaking	# Arquebus
		}
	}
	
	cost_gold = expensive_building_tier_4_cost
	
	county_modifier = {
		tax_mult = 0.1
		development_growth_factor = 0.1
	}
	
	next_building = saika_gunsmiths_03
	
	ai_value = {
		base = 100
	}
	
	type = special

	flag = travel_point_of_interest_martial
}

saika_gunsmiths_03 = {

	construction_time = slow_construction_time

	type_icon = "icon_building_blacksmiths.dds"

	can_construct_potential = {
		building_requirement_tribal = no
	}
	can_construct = {
		scope:holder.culture = {
			has_innovation = innovation_advanced_bowmaking	# Arquebus
		}
	}
	
	cost_gold = expensive_building_tier_5_cost
	
	county_modifier = {
		tax_mult = 0.15
		development_growth_factor = 0.15
	}
	
	next_building = saika_gunsmiths_04
	
	ai_value = {
		base = 100
	}
	
	type = special

	flag = travel_point_of_interest_martial
}

saika_gunsmiths_04 = {

	construction_time = slow_construction_time

	type_icon = "icon_building_blacksmiths.dds"

	can_construct_potential = {
		building_requirement_tribal = no
	}
	can_construct = {
		scope:holder.culture = {
			has_innovation = innovation_bombard	# Cannons
		}
	}
	
	cost_gold = expensive_building_tier_6_cost
	
	county_modifier = {
		tax_mult = 0.2
		development_growth_factor = 0.2
	}
	
	ai_value = {
		base = 100
	}
	
	type = special

	flag = travel_point_of_interest_martial
}
