﻿##########
# Kusatsu Onsen
##########

kusatsu_onsen_hot_springs_01 = {

	construction_time = very_slow_construction_time
	
	type_icon = "icon_structure_maharloo_lake.dds" 
	
	can_construct_potential = {
	}
	
	cost_gold = 300
	
	character_modifier = {
		stress_loss_mult = 0.1
		negate_health_penalty_add = 0.25
		epidemic_resistance = 10
	}

	county_modifier = {
		county_opinion_add = 5
		development_growth = 0.1
#		development_growth_factor = 0.6
	}
	
	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	}
	
	type = special

	flag = travel_point_of_interest_natural_feature
}

##########
# Arima Onsen
##########

arima_onsen_hot_springs_01 = {

	construction_time = very_slow_construction_time
	
	type_icon = "icon_structure_maharloo_lake.dds" 
	
	can_construct_potential = {
	}
	
	cost_gold = 300
	
	character_modifier = {
		stress_loss_mult = 0.1
		negate_health_penalty_add = 0.25
		epidemic_resistance = 10
	}

	county_modifier = {
		county_opinion_add = 5
		development_growth = 0.1
#		development_growth_factor = 0.6
	}
	
	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	}
	
	type = special

	flag = travel_point_of_interest_natural_feature
}

##########
# Dogo Onsen
##########

dogo_onsen_hot_springs_01 = {

	construction_time = very_slow_construction_time
	
	type_icon = "icon_structure_maharloo_lake.dds" 
	
	can_construct_potential = {
	}
	
	cost_gold = 300
	
	character_modifier = {
		stress_loss_mult = 0.1
		negate_health_penalty_add = 0.25
		epidemic_resistance = 10
	}

	county_modifier = {
		county_opinion_add = 5
		development_growth = 0.1
#		development_growth_factor = 0.6
	}
	
	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	}
	
	type = special

	flag = travel_point_of_interest_natural_feature
}

##########
# Gero Onsen
##########

gero_onsen_hot_springs_01 = {

	construction_time = very_slow_construction_time
	
	type_icon = "icon_structure_maharloo_lake.dds" 
	
	can_construct_potential = {
	}
	
	cost_gold = 300
	
	character_modifier = {
		stress_loss_mult = 0.1
		negate_health_penalty_add = 0.25
		epidemic_resistance = 10
	}

	county_modifier = {
		county_opinion_add = 5
		development_growth = 0.1
#		development_growth_factor = 0.6
	}
	
	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	}
	
	type = special

	flag = travel_point_of_interest_natural_feature
}
