﻿challenge_character_imagawa_yoshimoto = {
	start_date = 1560.6.11
	achievements = { celebrity_achievement ep1_02_achievement }
	character = {
		name = "challenge_character_imagawa_yoshimoto"
		dynasty = 10000230
		dynasty_splendor_level = 3
		type = male
		birth = 1519.1.1
		title = d_nmih_suruga
		government = feudal_government
		culture = aristocrats
		religion = shinto
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_HARD"
		history_id = 10023010

		animation = personality_cynical
	}
}

challenge_character_oda_nobunaga = {
	start_date = 1560.6.11
	achievements = { dreadful_ruler_achievement ep1_20_achievement }
	character = {
		name = "challenge_character_oda_nobunaga"
		dynasty = 10000280
		dynasty_splendor_level = 2
		type = male
		birth = 1534.6.23
		title = d_nmih_owari
		government = feudal_government
		culture = western_chubu
		religion = shinto
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_MEDIUM"
		history_id = 10029120

		animation = personality_rational
	}
}
