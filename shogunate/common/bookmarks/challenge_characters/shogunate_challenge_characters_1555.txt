﻿challenge_character_mori_motonari = {
	start_date = 1555.10.15
	achievements = { trapped_in_the_web_achievement followed_by_shadows_achievement  }
	character = {
		name = "challenge_character_mori_motonari"
		dynasty = 10000560
		dynasty_splendor_level = 1
		type = male
		birth = 1497.4.16
		title = d_nmih_aki
		government = feudal_government
		culture = western_chugoku
		religion = shinto
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_EASY"
		history_id = 10056004

		animation = personality_cynical
	}
}

challenge_character_nagao_kagetora = {
	start_date = 1555.10.15
	achievements = { saint_achievement moving_up_in_the_world_achievement }
	character = {
		name = "challenge_character_nagao_kagetora"
		dynasty = 10000320
		dynasty_splendor_level = 3
		type = male
		birth = 1530.2.18
		title = d_nmih_echigo
		government = feudal_government
		culture = eastern_hokuriku
		religion = shinto
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_MEDIUM"
		history_id = 10032011

		animation = personality_honorable
	}
}
