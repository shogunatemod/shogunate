﻿heritage_tohoku = {
	type = heritage
	audio_parameter = european
}

heritage_kanto = {
	type = heritage
	audio_parameter = european
}

heritage_hokuriku = {
	type = heritage
	audio_parameter = european
}

heritage_chubu = {
	type = heritage
	audio_parameter = european
}

heritage_kinai = {
	type = heritage
	audio_parameter = european
}

heritage_saigoku = {
	type = heritage
	audio_parameter = european
}

heritage_kyushu = {
	type = heritage
	audio_parameter = european
}

heritage_imperial_court = {
	type = heritage
	audio_parameter = european
}

heritage_ryukyu = {
	type = heritage
	audio_parameter = european
}

heritage_hokkaido = {
	type = heritage
	audio_parameter = european
}

heritage_ainu = {
	type = heritage
	audio_parameter = european
}

heritage_okhotsk = {
	type = heritage
	audio_parameter = european
}

heritage_buddhist = {
	type = heritage
	audio_parameter = european
}
