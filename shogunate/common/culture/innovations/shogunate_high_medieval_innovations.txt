﻿#### Local Types ####
@administration_1 = "gfx/interface/icons/culture_innovations/innovation_administration_01.dds"
@administration_2 = "gfx/interface/icons/culture_innovations/innovation_administration_02.dds"
@administration_3 = "gfx/interface/icons/culture_innovations/innovation_administration_03.dds"
@civil_construction_1 = "gfx/interface/icons/culture_innovations/innovation_civil_construction_01.dds"
@civil_construction_2 = "gfx/interface/icons/culture_innovations/innovation_civil_construction_02.dds"
@leadership_1 = "gfx/interface/icons/culture_innovations/innovation_leadership_01.dds"
@leadership_2 = "gfx/interface/icons/culture_innovations/innovation_leadership_02.dds"
@raised_banner = "gfx/interface/icons/culture_innovations/innovation_raised_banner.dds"
@fortifications = "gfx/interface/icons/culture_innovations/innovation_fortifications.dds"
@siege_weapons = "gfx/interface/icons/culture_innovations/innovation_siege_weapons.dds"
@levy_building = "gfx/interface/icons/culture_innovations/innovation_levy_building.dds"
@maa_01 = "gfx/interface/icons/culture_innovations/innovation_maa_01.dds"
@maa_02 = "gfx/interface/icons/culture_innovations/innovation_maa_02.dds"
@weapons_and_armor_01 = "gfx/interface/icons/culture_innovations/innovation_weapons_and_armor_01.dds"
@weapons_and_armor_02 = "gfx/interface/icons/culture_innovations/innovation_weapons_and_armor_02.dds"
@knight = "gfx/interface/icons/culture_innovations/innovation_knight.dds"
@hird = "gfx/interface/icons/culture_innovations/innovation_hird.dds"
@longboats = "gfx/interface/icons/culture_innovations/innovation_longboats.dds"
@majesty_01 = "gfx/interface/icons/culture_innovations/innovation_majesty_01.dds"
@majesty_02 = "gfx/interface/icons/culture_innovations/innovation_majesty_02.dds"
@majesty_03 = "gfx/interface/icons/culture_innovations/innovation_majesty_03.dds"
@nobility_01 = "gfx/interface/icons/culture_innovations/innovation_nobility_01.dds"
@nobility_02 = "gfx/interface/icons/culture_innovations/innovation_nobility_02.dds"
@nobility_03 = "gfx/interface/icons/culture_innovations/innovation_nobility_03.dds"
@nobility_04 = "gfx/interface/icons/culture_innovations/innovation_nobility_04.dds"
@misc_inventions = "gfx/interface/icons/culture_innovations/innovation_misc_inventions.dds"

@camel = "gfx/interface/icons/culture_innovations/innovation_camel.dds"
@elephant = "gfx/interface/icons/culture_innovations/innovation_elephant.dds"
@special_maa_01 = "gfx/interface/icons/culture_innovations/innovation_special_maa_01.dds"
@special_maa_02 = "gfx/interface/icons/culture_innovations/innovation_special_maa_02.dds"


###################
# Mid Sengoku Era #
###################

### Military

# Tamon-yagura
innovation_hoardings = {
	group = culture_group_military
	culture_era = culture_era_high_medieval
	icon = @fortifications

	custom = unlock_high_medieval_fortification_buildings

	flag = global_regular
	flag = high_medieval_era_regular
}

# Taketaba
innovation_trebuchet = {
	group = culture_group_military
	culture_era = culture_era_high_medieval
	icon = @siege_weapons

	unlock_maa = trebuchet

	flag = global_regular
	flag = high_medieval_era_regular
}

# Fudai
innovation_men_at_arms = {
	group = culture_group_military
	culture_era = culture_era_high_medieval
	icon = @leadership_1

	character_modifier = {
		men_at_arms_cap = 1
		men_at_arms_limit = 4
		men_at_arms_title_limit = 1
	}

	flag = global_regular
	flag = high_medieval_era_regular
}

# Kosho
innovation_knighthood = {
	group = culture_group_military
	culture_era = culture_era_high_medieval
	icon = @knight

	character_modifier = {
		active_accolades = 1
		glory_hound_opinion = 5
		knight_effectiveness_mult = 0.2
		accolade_glory_gain_mult = 0.1
	}

	custom = students_of_knighthood
	custom = more_glory_hounds

	flag = global_regular
	flag = high_medieval_era_regular
}

# Gunyakushu
innovation_castle_baileys = {
	group = culture_group_military
	culture_era = culture_era_high_medieval
	icon = @levy_building

	custom = unlock_high_medieval_military_buildings

	character_modifier = {
		levy_reinforcement_rate = 0.15
	}

	flag = global_regular
	flag = high_medieval_era_regular
}

# Checkpoint Abolition
innovation_shogunate_sekisho = {
	group = culture_group_military
	culture_era = culture_era_high_medieval
	icon = @maa_02

	character_modifier = {
		movement_speed = 0.1
	}

	flag = global_regular
	flag = high_medieval_era_regular
}

### Civic

# Jikiso
innovation_divine_right = {
	group = culture_group_civic
	culture_era = culture_era_high_medieval
	icon = @majesty_03

	unlock_casus_belli = de_jure_cb

	custom = multiple_claim_wars_others
	custom = unlock_palatinate_contract
	custom = more_zealot

	character_modifier = {
		short_reign_duration_mult = -0.1
		monthly_piety_gain_mult = 0.1
	}

	flag = global_regular
	flag = high_medieval_era_regular
}

# Shuinjo
innovation_land_grants = {
	group = culture_group_civic
	culture_era = culture_era_high_medieval
	icon = @administration_1

	custom = fabricate_claim_speed
	custom = cb_discount_prestige_10
	custom = more_parochial

	flag = global_regular
	flag = high_medieval_era_regular
}

# Bunkokuho
innovation_windmills = {
	group = culture_group_civic
	culture_era = culture_era_high_medieval
	icon = @administration_3

	unlock_building = windmills_01
	unlock_building = watermills_01

	flag = global_regular
	flag = high_medieval_era_regular
}

# Rakuichi-rakuza
innovation_guilds = {
	group = culture_group_civic
	culture_era = culture_era_high_medieval
	icon = @nobility_01

	custom = unlock_high_medieval_economic_buildings

	unlock_building = caravanserai_01

	county_modifier = {
		building_slot_add = 1
	}

	flag = global_regular
	flag = high_medieval_era_regular
}

# Nanban Trade
innovation_shogunate_nanban_trade = {
	group = culture_group_civic
	culture_era = culture_era_high_medieval
	icon = @longboats

	character_modifier = {
		tax_mult = 0.1
	}

	flag = global_regular
	flag = high_medieval_era_regular
}

# Cupellation
innovation_currency_03 = {
	group = culture_group_civic
	culture_era = culture_era_high_medieval
	icon = @civil_construction_1

	character_modifier = {
		development_growth_factor = 0.1
	}

	flag = global_regular
	flag = high_medieval_era_regular
}

# Self-governing City
innovation_development_03 = {
	group = culture_group_civic
	culture_era = culture_era_high_medieval
	icon = @raised_banner

	custom = reduce_develop_county_penalty_03

	flag = global_regular
	flag = high_medieval_era_regular
}

# Yasen
innovation_scutage = {
	group = culture_group_civic
	culture_era = culture_era_high_medieval
	icon = @majesty_02

	custom = unlock_scutage_contract
	
	character_modifier = {
		republic_government_tax_contribution_mult = 0.05
	}

	flag = global_regular
	flag = high_medieval_era_regular
}
